//
//  UsersViewController.h
//  Spika
//
//  Created by HelixTech-Admin  on 04/07/2017.
//  Copyright © 2017 Clover Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelectViewController.h"
#import "CSUserModel.h"
#import "SelectUserViewController.h"
#import "Reachability.h"
#import "MBProgressHUD.h"

@interface UsersViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,NSURLConnectionDataDelegate,UIAlertViewDelegate>{

    NSMutableArray *arrUserlist;
    NSMutableArray *arrUserimages;
    NSMutableData *dataServer;
    NSMutableArray *idarray;
    MBProgressHUD *hud1;
    MBProgressHUD *hud;

}
@property (nonatomic,strong) IBOutlet UITableView *tblUsers;

-(void)hideHud;
-(void)pushViewController:(UIViewController *)controller;

@end
