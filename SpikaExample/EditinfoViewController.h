//
//  EditinfoViewController.h
//  Spika
//
//  Created by HelixTech-Admin  on 28/07/2017.
//  Copyright © 2017 Clover Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSGroupModel.h"
#import "SQLite.h"
#import "CSUserModel.h"
#import "SelectViewController.h"
#import "MBProgressHUD.h"
#import "Reachability.h"



@protocol SecondViewControllerDelegate <NSObject>

-(void)updateLabelWithString:(NSString*)string;

@end

@interface EditinfoViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate,UITextFieldDelegate>
{

    NSMutableArray *users;
    NSMutableArray *arrusers;
    NSMutableArray *totalusers;
    NSMutableArray *totalusersArray;

    NSMutableArray *arrDisplayUsers;
    NSMutableArray *arrAddUsers;
    NSMutableArray *arruserids;
    NSMutableArray *arrShownusers;
    NSString *roomid;
    IBOutlet UIBarButtonItem *btnBack;
    
    NSString *flag;
    id<SecondViewControllerDelegate> delegate;
    NSString *currentuserid;
    
    NSString * indxPath;
    
}
@property(nonatomic,strong) IBOutlet UITableView *tblgrpusers;
@property(nonatomic,strong) IBOutlet UITableView *tbladdusers;
@property(nonatomic,strong) NSMutableArray *arrusers;
@property(nonatomic,strong) NSString *roomid;
@property(nonatomic, strong) NSString *editroomid;
@property(nonatomic,strong) CSGroupModel *grprecv;
@property(nonatomic,strong) CSUserModel *loggedinuserid;
@property(nonatomic,strong) NSString *strgrpflag;
@property(nonatomic,strong) IBOutlet UINavigationItem *navitem;
@property (nonatomic,strong) id <SecondViewControllerDelegate> delegate;
@property (nonatomic,strong) UIBarButtonItem *btnAddUser;





@end
