//
//  Selectusercell.h
//  Spika
//
//  Created by HelixTech-Admin  on 08/08/2017.
//  Copyright © 2017 Clover Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Selectusercell : UITableViewCell
@property(nonatomic,strong) IBOutlet UILabel *lblname;
@property(nonatomic,strong) IBOutlet UILabel *lblsubname;
@property(nonatomic,strong) IBOutlet UILabel *lbltime;
@property(nonatomic,strong) IBOutlet UIImageView *imguser;
@property(nonatomic,strong) IBOutlet UIImageView *imgtick;

@end
