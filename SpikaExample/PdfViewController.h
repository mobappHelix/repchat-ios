//
//  PdfViewController.h
//  Spika
//
//  Created by HelixTech-Admin  on 03/10/2017.
//  Copyright © 2017 Clover Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PdfViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (nonatomic, retain) NSString * pathString;
@end
