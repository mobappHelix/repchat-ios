//
//  LoginViewController.h
//  Spika
//
//  Created by HelixTech-Admin  on 26/09/2017.
//  Copyright © 2017 Clover Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSUserModel.h"
#import "SelectUserViewController.h"
#import "Reachability.h"
#import "MBProgressHUD.h"



@interface LoginViewController : UIViewController<UITextFieldDelegate>
{
    NSMutableData *dataServer;
    NSMutableArray *idarray;
    NSMutableArray *arrUserlist;
    NSMutableArray *arrUserimages;
    MBProgressHUD *hud1;
    MBProgressHUD *hud;
    
    NSString * count;
}
@property (strong, nonatomic) IBOutlet UITextField *textFieldUserName;
@property (strong, nonatomic) IBOutlet UITextField *textFieldPassword;
@property (strong, nonatomic) IBOutlet UIView *splashView;

- (IBAction)buttonTappedLogin:(id)sender;
-(void)hideHud;
-(void)pushViewController:(UIViewController *)controller;

@end
