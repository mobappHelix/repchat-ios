//
//  SelectUserTableViewCell.m
//  Spika
//
//  Created by HelixTech-Admin  on 12/10/2017.
//  Copyright © 2017 Clover Studio. All rights reserved.
//

#import "SelectUserTableViewCell.h"

@implementation SelectUserTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
