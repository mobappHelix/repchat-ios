//
//  LoginViewController.m
//  Spika
//
//  Created by HelixTech-Admin  on 26/09/2017.
//  Copyright © 2017 Clover Studio. All rights reserved.
//

#import "LoginViewController.h"
#import "SWRevealViewController.h"
#import "AppDelegate.h"


@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.loginViewController = self;
    
    arrUserlist=[[NSMutableArray alloc]init];
    
    _textFieldPassword.delegate = self;
    _textFieldUserName.delegate = self;
    
    [_textFieldUserName setReturnKeyType:UIReturnKeyNext];
    [_textFieldPassword setReturnKeyType:UIReturnKeyDone];


    
    
    UIBarButtonItem *Item1 = [[UIBarButtonItem alloc] initWithTitle:@"Exit" style:UIBarButtonItemStylePlain target:self action:@selector(doExit)];
    
    self.navigationItem.leftBarButtonItem = Item1;

}

-(void)viewWillAppear:(BOOL)animated{
    
    //    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //    hud.mode = MBProgressHUDModeAnnularDeterminate;
    ////    hud.label.text = @"Loading";
    
    NSString * splashFlag = [[NSUserDefaults standardUserDefaults] objectForKey:@"splashFlag"];
    
    if ([splashFlag isEqualToString:@"1"]) {
        _splashView.hidden = NO;
    } else {
        _splashView.hidden = YES;
    }
    
    
    NSString *fcmToken = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"fcmToken"];
    NSLog(@"FCM :%@",fcmToken);
    
    if (fcmToken == (id)[NSNull null] || fcmToken.length == 0 )
    {
        NSLog(@"NULL");
        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
    }
    
//    Reachability *reachability = [Reachability reachabilityForInternetConnection];
//    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
//    if(networkStatus==NotReachable)
//    {
//        
//        //  isoffline=YES;
//        
//        [self getOfflineusers];
//        
//        
//    }
//    else
//    {
//        // isoffline=NO;
//        //[self login:self.parameters];
//        
//        hud1 = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//        [self clearusers];
//        [self serverChecking];
//        
//    }
    
    //    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    
}

//-(void) viewWillDisappear:(BOOL)animated {
//    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
//        // back button was pressed.  We know this is true because self is no longer
//        // in the navigation stack.
//        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"splashFlag"];
//    }
//    [super viewWillDisappear:animated];
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


-(void)doExit
{
    //show confirmation message to user
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Confirmation"
                                                    message:@"Do you want to exit?"
                                                   delegate:self
                                          cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:@"OK", nil];
    [alert show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex != 0)  // 0 == the cancel button
    {
        //home button press programmatically
        UIApplication *app = [UIApplication sharedApplication];
        [app performSelector:@selector(suspend)];
        
        //wait 2 seconds while app is going background
        [NSThread sleepForTimeInterval:0.0];
        
        //exit app when app is in background
        exit(0);
    }
}


-(void)serverChecking{
    
    NSURL *urlObj=[[NSURL alloc]initWithString:@"http://148.251.133.82/spikachatws/GetLIstOfUsers"];

    NSURLRequest *requestObj=[[NSURLRequest alloc]initWithURL:urlObj];
    NSURLConnection *connectionObj=[[NSURLConnection alloc]initWithRequest:requestObj delegate:self];
    if(connectionObj)
    {
        NSLog(@"Successful connection");
        dataServer=[[NSMutableData alloc]init];
    }
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    
    
    [dataServer setLength:0];
    
    
    
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    
    [dataServer appendData:data];
    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
    //[MBProgressHUD hideHUDForView:self.view animated:YES];
    NSMutableDictionary *response=[[NSMutableDictionary alloc]initWithDictionary:[NSJSONSerialization JSONObjectWithData:dataServer options:0 error:nil]];
    
    
    idarray=[response valueForKey:@"data"];
    
    
    for (int i=0; i<=idarray.count-1; i++) {
        
        CSUserModel *chatlist=[[CSUserModel alloc]init];
        
        
        chatlist.userID=[[idarray objectAtIndex:i]valueForKey:@"user_id"];
        chatlist.name=[[idarray objectAtIndex:i]valueForKey:@"firstName"];
        chatlist.created=[[idarray objectAtIndex:i]valueForKey:@"registered_date"] ;
        
        [arrUserlist addObject:chatlist];
        
        SQLite *sqlObj=[[SQLite alloc] initWithSQLFile:@"SpikaChatDB.sqlite"];
        
        [sqlObj openDb];
        
        
        
        NSString *query = [NSString stringWithFormat:@"insert into tblusers (userID,username,created) values ('%@','%@','%@')",chatlist.userID,chatlist.name,chatlist.created];
        
        
        
        [sqlObj insertDb:query];
        
        
        [sqlObj closeDb];
        
        
        //        [MBProgressHUD hideHUDForView:self.view animated:YES];
//        [hud1 hideAnimated:YES];
        
//        [_tblUsers reloadData];
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:arrUserlist];
    [defaults setObject:data forKey:@"totalusers"];
    [defaults synchronize];
    
    
    [self loginCheck];
    
    
}


-(void)clearusers{
    
    SQLite *sqlObj=[[SQLite alloc] initWithSQLFile:@"SpikaChatDB.sqlite"];
    
    [sqlObj openDb];
    
    
    
    NSString *query = [NSString stringWithFormat:@"delete from tblusers"];
    
    
    
    [sqlObj updateDb:query];
    
    
    [sqlObj closeDb];
    
    
    
}

-(void)getOfflineusers{
    
    
    [arrUserlist removeAllObjects];
    
    SQLite *sqlObj=[[SQLite alloc] initWithSQLFile:@"SpikaChatDB.sqlite"];
    
    [sqlObj openDb];
    
    
    NSString *query = [NSString stringWithFormat:@"SELECT  userID,username,created from tblusers"];
    
    
    
    [sqlObj readDb:query];
    
    
    
    
    while ([sqlObj hasNextRow])
    {
        CSUserModel *chatlist=[[CSUserModel alloc]init];
        
        
        chatlist.userID=[sqlObj getColumn:0 type:@"text"];
        chatlist.name=[sqlObj getColumn:1 type:@"text"];
        chatlist.registereddate=[sqlObj getColumn:2 type:@"text"];
        
        [arrUserlist addObject:chatlist];
        
        
        
    }
    
    [sqlObj closeDb];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:arrUserlist];
    [defaults setObject:data forKey:@"totalusers"];
    [defaults synchronize];
    
    
}

- (IBAction)buttonTappedLogin:(id)sender {
    
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus==NotReachable)
    {
        
        //  isoffline=YES;
        
        [self getOfflineusers];
        
        
        NSString * str = _textFieldUserName.text;
        NSString * userName = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        
        
        for (int i = 0; i< arrUserlist.count; i++) {
            CSUserModel * model = [arrUserlist objectAtIndex:i];
            
            if ([model.name rangeOfString:userName options:NSCaseInsensitiveSearch].location == NSNotFound) {
                NSLog(@"string does not contain userName");
                
                //            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                //                                                            message:@"Verify Username & Password"
                //                                                           delegate:self
                //                                                  cancelButtonTitle:@"OK"
                //                                                  otherButtonTitles:nil, nil];
                //            [alert show];
                
                
                
            } else {
                NSLog(@"string contains userName!");
                
                //            CSUserModel *model=[arrUserlist objectAtIndex:i];
                
                _textFieldUserName.text = @"";
                _textFieldPassword.text = @"";
                
                
                NSMutableArray * arr = [[NSMutableArray alloc]init];
                
                    SQLite *sqlObj=[[SQLite alloc] initWithSQLFile:@"SpikaChatDB.sqlite"];
                    
                    [sqlObj openDb];
                    
                    
                    NSString * query = [NSString stringWithFormat:@"SELECT otheruserid FROM otherusers"];
                    
                    //                NSString *query = [NSString stringWithFormat:@"SELECT  userID,roomID,user_name,type,message,created,localID,status,attributes,avatarURL,pushToken,deleted,user_userID,file_id,mimetype,file_name,file_size,thumb_id,thumb_name,thumb_size,latitude,longitude,seenby FROM tblmessage where roomID = '%@'",model.userID];
                    
                    [sqlObj readDb:query];
                    
                    while ([sqlObj hasNextRow])
                    {
                        [arr addObject:[sqlObj getColumn:0 type:@"text"]];
                    }
                    
                    [sqlObj closeDb];
                
                
                
                if (![arr containsObject:model.userID]) {
                    
                    NSMutableArray * arrayCheck = [[NSMutableArray alloc]init];
                    
                    
                    SQLite *sqlObj=[[SQLite alloc] initWithSQLFile:@"SpikaChatDB.sqlite"];
                    
                    [sqlObj openDb];
                    
                    NSString *query = [NSString stringWithFormat:@"SELECT logedInUserName FROM tblmessage where userID = '%@'",model.userID];
                    
                    [sqlObj readDb:query];
                    
                    while ([sqlObj hasNextRow])
                    {
                        [arrayCheck addObject:[sqlObj getColumn:0 type:@"text"]];
                    }
                    
                    [sqlObj closeDb];
                    
                    NSString * flag ;
                    
                    
                    NSArray *originalArray = [[NSMutableArray alloc]initWithArray:arrayCheck];
                    [arrayCheck removeAllObjects];
                    //    NSMutableArray *uniqueArray = [NSMutableArray array];
                    NSMutableSet *names = [NSMutableSet set];
                    for (id obj in originalArray) {
                        //        NSString *destinationName = [obj destinationname];
                        if (![names containsObject:obj]) {
                            [arrayCheck addObject:obj];
                            [names addObject:obj];
                        }
                    }
                    
                    
                    
                    if (arrayCheck.count == 0) {
                        
                        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                                        message:@"No Data"
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil, nil];
                        [alert show];

                        
                    } else {
                        
                        for (int j = 0; j< arrayCheck.count; j++) {
                            
                            if ([[arrayCheck objectAtIndex:j] rangeOfString:userName options:NSCaseInsensitiveSearch].location == NSNotFound) {
                                NSLog(@"string does not contain userName");
                                
                                flag = @"0";
                                
                            } else {
                                
                                flag = @"1";
                                
                                break;
                                
                                
                            }
                            
                        }
                        
                        if ([flag isEqualToString:@"1"]) {
                            
                            [hud1 hideAnimated:YES];
                            
                            
                            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//                            SelectUserViewController *sfvc = [storyboard instantiateViewControllerWithIdentifier:@"selected"];
//                            sfvc.strroomid=model.userID;
//                            // [sfvc setModalPresentationStyle:UIModalPresentationFullScreen];
//                            [self.navigationController pushViewController:sfvc animated:YES];
                            
                            SWRevealViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"SWMain"];
                            
                            [self presentViewController:vc animated:YES completion:nil];
                            
                            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:model];
                            [defaults setObject:data forKey:@"Loggeduser"];
                            [defaults synchronize];
                            
                        } else if([flag isEqualToString:@"0"]) {
                            
                            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                                            message:@"No Data"
                                                                           delegate:self
                                                                  cancelButtonTitle:@"OK"
                                                                  otherButtonTitles:nil, nil];
                            [alert show];
                            
                        }
                    }
                    
          
                    
                   
                                        
                    
                } else {
                    
                                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                                                message:@"No Data"
                                                                               delegate:self
                                                                      cancelButtonTitle:@"OK"
                                                                      otherButtonTitles:nil, nil];
                                [alert show];
                    
                    
                }
       
         
                
                    

                break;
            }
        }
        
        
    }
    else
    {
        // isoffline=NO;
        //[self login:self.parameters];
        
        hud1 = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [self clearusers];
        [self serverChecking];
        
        
  
        
    }

    
    
    
    
    
    
    

}

-(void)hideHud {
    
    [hud hideAnimated:YES];
}

-(void)loginCheck {
    NSString * str = _textFieldUserName.text;
    NSString * userName = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    
    
    for (int i = 0; i< arrUserlist.count; i++) {
        CSUserModel * model = [arrUserlist objectAtIndex:i];
        
        if ([model.name rangeOfString:userName options:NSCaseInsensitiveSearch].location == NSNotFound) {
            NSLog(@"string does not contain userName");
            
            //            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Alert"
            //                                                            message:@"Verify Username & Password"
            //                                                           delegate:self
            //                                                  cancelButtonTitle:@"OK"
            //                                                  otherButtonTitles:nil, nil];
            //            [alert show];
            
            [hud1 hideAnimated:YES];

            
        } else {
            NSLog(@"string contains userName!");
            
            
            _textFieldUserName.text = @"";
            _textFieldPassword.text = @"";
            
            [hud1 hideAnimated:YES];

            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//            SelectUserViewController *sfvc = [storyboard instantiateViewControllerWithIdentifier:@"selected"];
//            sfvc.strroomid=model.userID;
//            // [sfvc setModalPresentationStyle:UIModalPresentationFullScreen];
//            [self.navigationController pushViewController:sfvc animated:YES];
            
            SWRevealViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"SWMain"];
            
            [self presentViewController:vc animated:YES completion:nil];
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:model];
            [defaults setObject:data forKey:@"Loggeduser"];
            [defaults synchronize];
            
            
            
            break;
        }
    }
}

//-(BOOL)textFieldShouldReturn:(UITextField*)textField
//{
//    NSInteger nextTag = textField.tag + 1;
//    // Try to find next responder
//    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
//    if (nextResponder) {
//        // Found next responder, so set it.
//        [nextResponder becomeFirstResponder];
//    } else {
//        // Not found, so remove keyboard.
//        [textField resignFirstResponder];
//    }
//    return NO; // We do not want UITextField to insert line-breaks.
//}





- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if(textField == _textFieldUserName) {
        [_textFieldPassword becomeFirstResponder];
    } else if (textField == _textFieldPassword) {
        [textField resignFirstResponder];

    }
    
    return NO;
}

-(void)pushViewController:(UIViewController *)controller {
    
    [self.navigationController pushViewController:controller animated:YES];
    
}

@end
