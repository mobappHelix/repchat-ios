//
//  EditinfoViewController.m
//  Spika
//
//  Created by HelixTech-Admin  on 28/07/2017.
//  Copyright © 2017 Clover Studio. All rights reserved.
//

#import "EditinfoViewController.h"

@interface EditinfoViewController (){
    
    UIButton *button;
    UIButton *button1;
    
}

@end

@implementation EditinfoViewController
@synthesize delegate;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //_arrusers=[[NSMutableArray alloc]init];
    
    flag=_strgrpflag;
    
    totalusers =[[NSMutableArray alloc]init];
    arrDisplayUsers=[[NSMutableArray alloc]init];
    arrAddUsers=[[NSMutableArray alloc]init];
    arruserids=[[NSMutableArray alloc]init];
    arrShownusers=[[NSMutableArray alloc]init];
    users=[[NSMutableArray alloc]init];
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"totalusers"];
    totalusersArray = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSArray *originalArray = [[NSMutableArray alloc]initWithArray:totalusersArray];
    //    NSMutableArray *uniqueArray = [NSMutableArray array];
    NSMutableSet *names = [NSMutableSet set];
    for (id obj in originalArray) {
        NSString *destinationName = [obj userID];
        if (![names containsObject:destinationName]) {
            [totalusers addObject:obj];
            [names addObject:destinationName];
        }
    }
    
    
    NSData *userdata = [defaults objectForKey:@"Loggeduser"];
    _loggedinuserid = [NSKeyedUnarchiver unarchiveObjectWithData:userdata];
    
    if ([flag isEqual:@"1"]) {
        
        NSData *data2 = [defaults objectForKey:@"createdgroup"];
        _grprecv=[NSKeyedUnarchiver unarchiveObjectWithData:data2];
        
        
        
    }
    else{
        
        NSData *data3 = [defaults objectForKey:@"selectedgroup"];
        _grprecv=[NSKeyedUnarchiver unarchiveObjectWithData:data3];
        if ([users containsObject:self.grprecv.creator_id]) {
            NSLog(@"Contains user");
        }
        else{
            [users addObject:self.grprecv.creator_id];
        }
    }
    [defaults synchronize];
    
    
    
    [_tbladdusers setHidden:YES];
    
    // _grprecv=[[CSGroupModel alloc]init];
    
    
    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    
    
    self.navigationItem.title=_grprecv.group_name;
    
    if (_loggedinuserid.userID==_grprecv.creator_id) {
        
        
        
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage: [UIImage imageNamed:@"ic_adduser"] forState:UIControlStateNormal];
        button.frame = CGRectMake(0, 0, 40, 40);
        [button addTarget:self action:@selector(btnAddUserClicked:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *barItem = [[UIBarButtonItem alloc] initWithCustomView:button];
        //self.navigationItem.rightBarButtonItem = barItem;
        
        UIButton *item = [UIButton buttonWithType:UIButtonTypeCustom];
        [item setImage: [UIImage imageNamed:@"ic_edit"] forState:UIControlStateNormal];
        item.frame = CGRectMake(0, 0, 40, 40);
        [item addTarget:self action:@selector(btnclick:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *Item2 = [[UIBarButtonItem alloc] initWithCustomView:item];
        
        
        self.navigationItem.rightBarButtonItems = @[barItem,Item2];
        
    }
    
    else{
        
        NSLog(@"Not Admin");
        
    }
    
    
    //self.navitem.titleView =button;
    
    [arrDisplayUsers removeAllObjects];
    [users removeAllObjects];
    [arrShownusers removeAllObjects];

    
    [self getusers];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (tableView==_tblgrpusers) {
        return arrDisplayUsers.count;
    }
    
    else
        
        return arrShownusers.count;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellId = @"cellid";
    UITableViewCell * cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    
    if (tableView==_tblgrpusers) {
        
        CSUserModel *model=[arrDisplayUsers objectAtIndex:indexPath.row];
        
        cell.textLabel.text=model.name;
        //cell.imageView.image=[UIImage imageNamed:@"user.png"];
        
        
    } else if  (tableView==_tbladdusers) {
        
        
        CSUserModel *model=[arrShownusers objectAtIndex:indexPath.row];
        
        cell.textLabel.text=model.name;
        //cell.imageView.image=[UIImage imageNamed:@"user.png"];
        
        if ([arrAddUsers containsObject:model]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            
        } else {
            cell.accessoryType = UITableViewCellAccessoryNone;
            
        }
    }
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==_tbladdusers) {
        
        UITableViewCell *cell=[_tbladdusers cellForRowAtIndexPath:indexPath];
        
        CSUserModel *seluser=[arrShownusers objectAtIndex:indexPath.row];
        
        if (cell.accessoryType==UITableViewCellAccessoryCheckmark) {
            cell.accessoryType = UITableViewCellAccessoryNone;
            [arrAddUsers removeObject:seluser];
        }
        
        else{
            
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            [arrAddUsers addObject:seluser];
        }
        
        
    }
    
    
    
}


-(void)getusers{
    
    
    NSString *jsonString;
    
    SQLite *sqlObj=[[SQLite alloc] initWithSQLFile:@"SpikaChatDB.sqlite"];
    
    [sqlObj openDb];
    
    
    NSString *query = [NSString stringWithFormat:@"SELECT  groupid,groupname,grouproomid,groupusers,groupcreatorid FROM tblgroups where grouproomid = '%@'",_grprecv.room_id];
    
    
    [sqlObj readDb:query];
    
    
    while ([sqlObj hasNextRow])
    {
        CSGroupModel *modgrp=[[CSGroupModel alloc]init];
        modgrp.group_id=[sqlObj getColumn:0 type:@"text"];
        modgrp.group_name=[sqlObj getColumn:1 type:@"text"];
        modgrp.room_id=[sqlObj getColumn:2 type:@"text"];
        modgrp.creator_id=[sqlObj getColumn:4 type:@"text"];
        
        jsonString=[sqlObj getColumn:3 type:@"text"];
        
    }
    
    NSData *data = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableArray *arrayObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    
    //modgrp.members=arrayObject;
    
    
    [users addObjectsFromArray:arrayObject];
    [sqlObj closeDb];
    
    
    NSArray *originalArray = [[NSMutableArray alloc]initWithArray:users];
    [users removeAllObjects];
    //    NSMutableArray *uniqueArray = [NSMutableArray array];
    NSMutableSet *names = [NSMutableSet set];
    for (id obj in originalArray) {
        //        NSString *destinationName = [obj destinationname];
        if (![names containsObject:obj]) {
            [users addObject:obj];
            [names addObject:obj];
        }
    }
    
    if (users.count!=0) {
        
        
        
        for (int i=0; i<=totalusers.count-1; i++) {
            CSUserModel *usr=[[CSUserModel alloc]init];
            usr=[totalusers objectAtIndex:i];
            
            for (int j=0; j<=users.count-1; j++) {
                if ([users objectAtIndex:j]==usr.userID && [users objectAtIndex:j]!=_loggedinuserid.userID) {
                    [arrDisplayUsers addObject:usr];
                    [arruserids addObject:usr.userID];
                    
                }
                
                else{
                    
                    NSLog(@"nothing");
                }
            }
        }
        
        for (int i=0; i<=totalusers.count-1; i++) {
            CSUserModel *usr=[[CSUserModel alloc]init];
            usr=[totalusers objectAtIndex:i];
            
            if ([arrDisplayUsers containsObject:usr] || usr.userID==_grprecv.creator_id) {
                NSLog(@"contains object");
            }
            else{
                
                [arrShownusers addObject:usr];
                
            }
        }
        
    }
    else{
        
        [arrShownusers addObjectsFromArray:totalusers];
        
    }
    
    [_tblgrpusers reloadData];
}

-(IBAction)btnBackClicked:(id)sender{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
-(void)btnAddUserClicked:(id)sender{
    
    

        
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus networkStatus = [reachability currentReachabilityStatus];
        if(networkStatus==NotReachable)
        {
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"No internet connection"
                                                                message:nil
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
            
            [alertView show];
            
            
            
            
        }
        else{
            
            if ([button.imageView.image isEqual:[UIImage imageNamed:@"ic_adduser"]]) {
                
                if (arrShownusers.count == 0) {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"No user to add"
                                                                        message:nil
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                    
                    [alertView show];
                } else {
                    
                [arrShownusers removeAllObjects];
                [self getusers];
                
                [button setImage:[UIImage imageNamed:@"check"] forState:UIControlStateNormal];
                
                
                [_tblgrpusers setHidden:YES];
                [_tbladdusers setHidden:NO];
                [_tbladdusers reloadData];
                }
                
            } else if ([button.imageView.image isEqual:[UIImage imageNamed:@"check"]]&& arrAddUsers.count!=0){
                

                [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                
                for (int i=0; i<=arrAddUsers.count-1; i++) {
                    
                    if (_loggedinuserid.userID==_grprecv.creator_id) {
                        
                        
                        
                        CSUserModel *user=[[CSUserModel alloc]init];
                        user=[arrAddUsers objectAtIndex:i];
                        
                        NSString *string= [NSString stringWithFormat:@"http://148.251.133.82/spikachatws/AddMemberToGroup/member_id/%@/group_id/%@",user.userID,_grprecv.group_id];
                        NSLog(@"%@",string);
                        NSString* webStringURL2 = [string stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                        NSURL *url2 = [NSURL URLWithString:webStringURL2];
                        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url2];
                        [request setHTTPMethod:@"POST"];
                        NSURLResponse *response;
                        NSError *err;
                        NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
                        NSLog(@"responseData: %@", responseData);
                        NSString *str = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
                        NSLog(@"responseData: %@", str);
                        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData
                                                                             options:kNilOptions
                                                                               error:nil];
                        
                        if ([[json valueForKey:@"response"] isEqualToNumber:[NSNumber numberWithInt:1]]) {
                            
                            [arruserids addObject:user.userID];
                            [arrDisplayUsers addObject:user];
                            [_tbladdusers reloadData];
                            
                        }
                        
                        if (user==[arrAddUsers lastObject]) {
                            if ([[json valueForKey:@"response"] isEqualToNumber:[NSNumber numberWithInt:1]]) {
                                
                                
                                
                                NSData *jsonData = [NSJSONSerialization dataWithJSONObject:arruserids options:NSJSONWritingPrettyPrinted error:nil];
                                NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                                
                                SQLite *sqlObj=[[SQLite alloc] initWithSQLFile:@"SpikaChatDB.sqlite"];
                                
                                [sqlObj openDb];
                                
                                
                                NSString *query = [NSString stringWithFormat:@"Update tblgroups set groupusers='%@' where groupid = '%@'",jsonString,_grprecv.group_id];
                                
                                
                                
                                [sqlObj updateDb:query];
                                
                                
                                [sqlObj closeDb];
                                
                                
                                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"User added Successfully"
                                                                                    message:nil
                                                                                   delegate:self
                                                                          cancelButtonTitle:@"OK"
                                                                          otherButtonTitles:nil];
                                [alertView setTag:2];
                                
                                [alertView show];
                                // [_btnAddUser setTitle:@"Add member"];
                                [button setImage:[UIImage imageNamed:@"ic_adduser"] forState:UIControlStateNormal];
                                
                                [_tblgrpusers setHidden:NO];
                                [_tbladdusers setHidden:YES];
                                
                                [_tblgrpusers reloadData];
                                
                                
                            }
                            
                        }
                        
                    }
                    
                }
                
            
            }
            else{
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Please select user to add"
                                                                    message:nil
                                                                   delegate:self
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                [alertView setTag:2];
                
                
            }
            
        }

    
 
    
}


-(void)viewWillDisappear:(BOOL)animated{
    
    [users removeAllObjects];
    
    
}

-(void)checkusers{
    
    
    
}


-(void)btnclick:(id)sender{
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Enter Name"
                                                    message:nil
                                                   delegate:self
                                          cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:@"OK", nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    alert.tag=1;
    [alert show];
    
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
    
}


- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (actionSheet.tag==1) {
        
        
        NSLog(@"string entered=%@",[actionSheet textFieldAtIndex:0].text);
        
        if (buttonIndex==1) {
            
            NSString * groupName = [actionSheet textFieldAtIndex:0].text;
            
            
            NSString *trimmedString = [groupName stringByTrimmingCharactersInSet:
                                       [NSCharacterSet whitespaceCharacterSet]];
            
            NSCharacterSet *cset = [NSCharacterSet characterSetWithCharactersInString:@"!@#$%&*()+'\";:=,/?[]"];
            NSRange range = [groupName rangeOfCharacterFromSet:cset];
            
            //            if(trimmedString){
            //
            //
            //                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Enter group name"
            //                                                                    message:nil
            //                                                                   delegate:self
            //                                                          cancelButtonTitle:@"OK"
            //                                                          otherButtonTitles:nil];
            //
            //                [alertView show];
            //
            //
            //
            //            }
            //            else
            
            
            if (range.location != NSNotFound){
                // ( or ) are present
                
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Special Characters are not allowed"
                                                                    message:nil
                                                                   delegate:self
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                
                [alertView show];
                
            } else if ([trimmedString isEqualToString:@""]) {
                NSLog(@"no room id");
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Enter group name"
                                                                    message:nil
                                                                   delegate:self
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                
                [alertView show];
                
                
            }
            else{
                
                [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                
                Reachability *reachability = [Reachability reachabilityForInternetConnection];
                NetworkStatus networkStatus = [reachability currentReachabilityStatus];
                if(networkStatus==NotReachable)
                {
                    
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"No internet connection"
                                                                        message:nil
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                    
                    [alertView show];
                    
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    
                    
                }
                
                else{
                    
                    
                    
                    NSString *string= [NSString stringWithFormat:@"http://148.251.133.82/spikachatws/EditGroupInformation/group_id/%@/group_name/%@",_grprecv.group_id,[actionSheet textFieldAtIndex:0].text];
                    
                    
                    
                    //                    NSString * str1 = [actionSheet textFieldAtIndex:0].text;
                    //
                    //                NSString *string= [NSString stringWithFormat:@"http://148.251.133.82/spikachatws/EditGroupInformation/group_id/%@/group_name/%@",_grprecv.group_id,[self URLEncodeStringFromString:str1]];
                    
                    
                    NSLog(@"%@",string);
                    NSString* webStringURL = [string stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                    NSURL *url = [NSURL URLWithString:webStringURL];
                    
                    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
                    [request setHTTPMethod:@"POST"];
                    NSURLResponse *response;
                    NSError *err;
                    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
                    NSLog(@"responseData: %@", responseData);
                    NSString *str = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
                    NSLog(@"responseData: %@", str);
                    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData
                                                                         options:kNilOptions
                                                                           error:nil];
                    
                    if ([[json valueForKey:@"response"] isEqualToNumber:[NSNumber numberWithInt:1]]) {
                        
                        [self.navigationItem  setTitle:[actionSheet textFieldAtIndex:0].text];
                        
                        [self.delegate updateLabelWithString:[actionSheet textFieldAtIndex:0].text ];
                        
                        SQLite *sqlObj=[[SQLite alloc] initWithSQLFile:@"SpikaChatDB.sqlite"];
                        
                        [sqlObj openDb];
                        
                        
                        
                        NSString *query = [NSString stringWithFormat:@"Update tblgroups set groupname='%@' where grouproomid = '%@'",[actionSheet textFieldAtIndex:0].text,_grprecv.room_id];
                        
                        
                        
                        [sqlObj updateDb:query];
                        
                        
                        [sqlObj closeDb];
                        
                        
                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Group info updated"
                                                                            message:nil
                                                                           delegate:self
                                                                  cancelButtonTitle:nil
                                                                  otherButtonTitles:@"OK",nil];
                        [alertView setTag:3];
                        
                        [alertView show];
                        
                    }
                    
                    //                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    //
                    //                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Group info updated"
                    //                                                                        message:nil
                    //                                                                       delegate:self
                    //                                                              cancelButtonTitle:nil
                    //                                                              otherButtonTitles:@"OK",nil];
                    //                    [alertView setTag:7];
                    //
                    //                    [alertView show];
                    
                    
                    
                }
            }
        }
        
        else{
            
            [self dismissViewControllerAnimated:YES completion:nil];
            
        }
    }
    
    if (actionSheet.tag==2) {
        if (buttonIndex==0){
            
//            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            //            [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
            [arrDisplayUsers removeAllObjects];
            [users removeAllObjects];
            [arrShownusers removeAllObjects];
            [arrAddUsers removeAllObjects];
            
            [self getusers];
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            [_tblgrpusers reloadData];
            [_tbladdusers reloadData];
            
            
        }
    }
    
    if (actionSheet.tag==3) {
        if (buttonIndex==0){
            
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            
            [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
            
            
            
        }
    }
    
    if (actionSheet.tag==4) {
        if (buttonIndex==0){
            
            [arrDisplayUsers removeAllObjects];
            [users removeAllObjects];
//            [arrShownusers removeAllObjects];

            //            [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
            [self getusers];
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            [_tblgrpusers reloadData];
            [_tbladdusers reloadData];
            
        }
    }
    
    if (actionSheet.tag==5) {
        
        
        
        
        if (buttonIndex==1) {
            
            Reachability *reachability = [Reachability reachabilityForInternetConnection];
            NetworkStatus networkStatus = [reachability currentReachabilityStatus];
            if(networkStatus!=NotReachable)
            {
                [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                
                //                NSIndexPath *indexPath = [self.tblgrpusers indexPathForSelectedRow];
                
                NSLog(@"%@",indxPath);
                
                CSUserModel *usermod=[[CSUserModel alloc]init];
                usermod=[arrDisplayUsers objectAtIndex:[indxPath intValue]];
                
                NSString *string= [NSString stringWithFormat:@"http://148.251.133.82/spikachatws/RemoveMemberFromGroup/member_id/%@/group_id/%@",usermod.userID,_grprecv.group_id];
                NSLog(@"%@",string);
                NSString* webStringURL = [string stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                NSURL *url = [NSURL URLWithString:webStringURL];
                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
                [request setHTTPMethod:@"POST"];
                NSURLResponse *response;
                NSError *err;
                NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
                NSLog(@"responseData: %@", responseData);
                NSString *str = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
                NSLog(@"responseData: %@", str);
                NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData
                                                                     options:kNilOptions
                                                                       error:nil];
                
                if ([[json valueForKey:@"response"] isEqualToNumber:[NSNumber numberWithInt:1]]) {
                    
                    
                    CSUserModel * userM = [arrDisplayUsers objectAtIndex:[indxPath intValue]];
                    
                    NSData *data3 = [[NSUserDefaults standardUserDefaults] objectForKey:@"selectedgroup"];
                    CSGroupModel *grp =[NSKeyedUnarchiver unarchiveObjectWithData:data3];
                    
                    NSMutableArray * array = [[NSMutableArray alloc]initWithArray:grp.members];
                    
                    [array removeObject:userM.userID];
                    
                    CSGroupModel *group = [[CSGroupModel alloc]init];
                    group.group_id = grp.group_id;
                    group.group_name = grp.group_name;
                    group.creator_id = grp.creator_id;
                    group.room_id = grp.room_id;
                    group.lastMessageTime = grp.lastMessageTime;
                    group.lastMessage = grp.lastMessage;
                    group.members =array;
                    //
                    //
                    //
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:group];
                    [defaults setObject:data forKey:@"selectedgroup"];
                    
                    
                    
                    [arrDisplayUsers removeObjectAtIndex:[indxPath intValue]];
                    //                    [_tblgrpusers reloadData];
                    
                    NSMutableArray *arruserstodelete=[[NSMutableArray alloc]init];
                    
                    if (arrDisplayUsers.count!=0) {
                        
                        
                        for (int i=0; i<=arrDisplayUsers.count-1; i++) {
                            CSUserModel *usr=[[CSUserModel alloc]init];
                            usr=[arrDisplayUsers objectAtIndex:i];
                            
                            [arruserstodelete addObject:usr.userID];
                            
                        }
                    }
                    
                    SQLite *sqlObj=[[SQLite alloc] initWithSQLFile:@"SpikaChatDB.sqlite"];
                    
                    [sqlObj openDb];
                    
                    
                    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:arruserstodelete options:NSJSONWritingPrettyPrinted error:nil];
                    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                    
                    
                    
                    NSString *query = [NSString stringWithFormat:@"Update tblgroups set groupusers='%@' where grouproomid = '%@'",jsonString,_grprecv.room_id];
                    
                    
                    
                    [sqlObj updateDb:query];
                    
                    
                    [sqlObj closeDb];
                    

                    
                    
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"User Deleted"
                                                                        message:nil
                                                                       delegate:self
                                                              cancelButtonTitle:nil
                                                              otherButtonTitles:@"OK",nil];
                    [alertView setTag:4];
                    
                    [alertView show];
                    
                    
                    
                }
            } else {
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"No internet connection"
                                                                    message:nil
                                                                   delegate:self
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                [alertView setTag:10];
                
                [alertView show];
            }
            
            
            
        }
        
        else{
            
            [self dismissViewControllerAnimated:YES completion:nil];
            
        }
    }
    
    if (actionSheet.tag==6) {
        
        
        if (buttonIndex==1) {
            
            Reachability *reachability = [Reachability reachabilityForInternetConnection];
            NetworkStatus networkStatus = [reachability currentReachabilityStatus];
            if(networkStatus!=NotReachable)
            {
                
                [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                
                //            NSIndexPath *indexPath = [self.tblgrpusers indexPathForSelectedRow];
                //
                //            CSUserModel *usermod=[[CSUserModel alloc]init];
                //            usermod=[arrDisplayUsers objectAtIndex:indexPath.row];
                
                //            http://148.251.133.82/spikachatws/RemoveMemberFromGroup/member_id/2/group_id/1
                NSString *string= [NSString stringWithFormat:@"http://148.251.133.82/spikachatws/RemoveMemberFromGroup/member_id/%@/group_id/%@",_loggedinuserid.userID,_grprecv.group_id];
                NSLog(@"%@",string);
                NSString* webStringURL = [string stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                NSURL *url = [NSURL URLWithString:webStringURL];
                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
                [request setHTTPMethod:@"POST"];
                NSURLResponse *response;
                NSError *err;
                NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
                NSLog(@"responseData: %@", responseData);
                NSString *str = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
                NSLog(@"responseData: %@", str);
                NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData
                                                                     options:kNilOptions
                                                                       error:nil];
                
                if ([[json valueForKey:@"response"] isEqualToNumber:[NSNumber numberWithInt:1]]) {
                    
                    //                [arrDisplayUsers removeObjectAtIndex:indexPath.row];
                    //                [_tblgrpusers reloadData];
                    //
                    //                NSMutableArray *arruserstodelete=[[NSMutableArray alloc]init];
                    //
                    //                if (arrDisplayUsers.count!=0) {
                    //
                    //
                    //                    for (int i=0; i<=arrDisplayUsers.count-1; i++) {
                    //                        CSUserModel *usr=[[CSUserModel alloc]init];
                    //                        usr=[arrDisplayUsers objectAtIndex:i];
                    //
                    //                        [arruserstodelete addObject:usr.userID];
                    //
                    //                    }
                    //                }
                    
                    //                SQLite *sqlObj=[[SQLite alloc] initWithSQLFile:@"SpikaChatDB.sqlite"];
                    //
                    //                [sqlObj openDb];
                    
                    
                    //                NSData *jsonData = [NSJSONSerialization dataWithJSONObject:arruserstodelete options:NSJSONWritingPrettyPrinted error:nil];
                    //                NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                    
                    
                    
                    //                NSString *query = [NSString stringWithFormat:@"Update tblgroups set groupusers='%@' where groupid = '%@'",jsonString,_grprecv.room_id];
                    
                    
                    
                    //                [sqlObj updateDb:query];
                    //
                    //
                    //                [sqlObj closeDb];
                    //
                    //                [MBProgressHUD hideHUDForView:self.view animated:YES];
                    //
                    //
                    //                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"User Deleted"
                    //                                                                    message:nil
                    //                                                                   delegate:self
                    //                                                          cancelButtonTitle:nil
                    //                                                          otherButtonTitles:@"OK",nil];
                    //                [alertView setTag:4];
                    //
                    //                [alertView show];
                    
                    
                    //                [self.navigationController dismissViewControllerAnimated:YES completion:nil];
                    
                    [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:1] animated:YES];
                    
                    //                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    //                SelectUserViewController *sfvc = [storyboard instantiateViewControllerWithIdentifier:@"selected"];
                    //
                    //
                    //                [self.navigationController popToViewController:sfvc animated:YES];
                    
                    
                }
                
                
                
            }
            else {
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"No internet connection"
                                                                    message:nil
                                                                   delegate:self
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                [alertView setTag:10];
                
                [alertView show];
                
                
            }
            
            
            
        }
        
        else{
            
            [self dismissViewControllerAnimated:YES completion:nil];
            
        }
    } if (actionSheet.tag==6) {
        
        NSLog(@"ok");
    }
    
}

//- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
//    // Return YES if you want the specified item to be editable.
//
//    if (_loggedinuserid.userID==_grprecv.creator_id ) {
//        return YES;
//    }
//
//    else
//        return NO;
//
//}

- (UITableViewCellEditingStyle)tableView:(UITableView *)aTableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Detemine if it's in editing mode
    if (aTableView==_tblgrpusers && _loggedinuserid.userID==_grprecv.creator_id) {
        return UITableViewCellEditingStyleDelete;
    }
    return UITableViewCellEditingStyleNone;
}


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    indxPath = [[NSString stringWithFormat:@"%ld",(long)indexPath.row] copy];
    
    if (editingStyle == UITableViewCellEditingStyleDelete && _loggedinuserid.userID==_grprecv.creator_id) {
        
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Confirm deletion"
                                                        message:@"Are you sure you want to delete this member?"
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel"
                                              otherButtonTitles:@"OK", nil];
        
        alert.tag=5;
        [alert show];
        
        
    }
    
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (IBAction)exitTapped:(id)sender {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Confirm deletion"
                                                    message:@"Are you sure you want to exit from this group?"
                                                   delegate:self
                                          cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:@"OK", nil];
    
    alert.tag=6;
    [alert show];
}

- (NSString *)URLEncodeStringFromString:(NSString *)string
{
    static CFStringRef charset = CFSTR("!@#$%&*()+'\";:=,/?[] ");
    CFStringRef str = (__bridge CFStringRef)string;
    CFStringEncoding encoding = kCFStringEncodingUTF8;
    return (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL, str, NULL, charset, encoding));
}

@end
