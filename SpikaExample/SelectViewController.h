//
//  SelectViewController.h
//  Spika
//
//  Created by HelixTech-Admin  on 21/06/2017.
//  Copyright © 2017 Clover Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSChatViewController.h"
#import "CSCustomConfig.h"
#import "CSUserModel.h"
#import "CSConfig.h"
#import "SelectUserViewController.h"
#import "SQLite.h"


@interface SelectViewController : UIViewController<UIAlertViewDelegate>{
     UITextField *myTextField;

}
-(IBAction)btnSelectprivate:(id)sender;
-(IBAction)btnSelectgroup:(id)sender;
@property (nonatomic,strong) CSUserModel *currentuser;


@end
