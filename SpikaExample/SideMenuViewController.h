//
//  SideMenuViewController.h
//  Spika
//
//  Created by administrator on 18/11/17.
//  Copyright © 2017 Clover Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"

@interface SideMenuViewController : UIViewController
{
    NSArray *menuItems;
}
@property (weak, nonatomic) IBOutlet UITableView *menuTableView;

@end
