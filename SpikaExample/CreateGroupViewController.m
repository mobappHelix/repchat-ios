//
//  CreateGroupViewController.m
//  Spika
//
//  Created by HelixTech-Admin  on 25/07/2017.
//  Copyright © 2017 Clover Studio. All rights reserved.
//

#import "CreateGroupViewController.h"

@interface CreateGroupViewController (){
    
    NSString *grp_id;
    NSString *groupname;
    NSMutableArray *arrusernames;
    NSMutableArray *arruserids;
}

@end

@implementation CreateGroupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    arrSelectedUser=[[NSMutableArray alloc]init];
    arrotherusers=[[NSMutableArray alloc]init];
    arrUserSelect=[[NSMutableArray alloc]init];
    idarray=[[NSMutableArray alloc]init];
    arrusernames=[[NSMutableArray alloc]init];
    arruserids=[[NSMutableArray alloc]init];
    
    _txtgroupname.delegate=self;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSData *userdata = [defaults objectForKey:@"Loggeduser"];
    _loggedinuser = [NSKeyedUnarchiver unarchiveObjectWithData:userdata];
    
    self.navigationController.interactivePopGestureRecognizer.delaysTouchesBegan = NO;
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage: [UIImage imageNamed:@"check"] forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, 40, 35);
    [button addTarget:self action:@selector(btnCreateclicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *Item1 = [[UIBarButtonItem alloc] initWithCustomView:button];
    
//    UIImage *image1 = [[UIImage imageNamed:@"check"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    UIBarButtonItem *item1 = [[UIBarButtonItem alloc] initWithImage:image1 style:UIBarButtonItemStylePlain target:self action:@selector(btnCreateclicked)];
    self.navigationItem.rightBarButtonItem=Item1;
    
    //  UIBarButtonItem *item2=[[UIBarButtonItem alloc]initWithTitle:@"Create group" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    self.navigationItem.title=@"Create group";

    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self serverChecking];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    return YES;
    
    
}

-(void)serverChecking{
    
    NSURL *urlObj=[[NSURL alloc]initWithString:[NSString stringWithFormat:@"http://148.251.133.82/spikachatws/GetLIstOfGroupsAndUsers/user_id/%@",_strid]];
    NSURLRequest *requestObj=[[NSURLRequest alloc]initWithURL:urlObj];
    NSURLConnection *connectionObj=[[NSURLConnection alloc]initWithRequest:requestObj delegate:self];
    if(connectionObj)
    {
        NSLog(@"Successful connection");
        dataServer=[[NSMutableData alloc]init];
    }
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    
    
    [dataServer setLength:0];
    
    
}

-(void)viewDidAppear:(BOOL)animated{
    
    
    
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    
    [dataServer appendData:data];
    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
    //[MBProgressHUD hideHUDForView:self.view animated:YES];
    //    NSMutableDictionary *response=[[NSMutableDictionary alloc]initWithDictionary:[NSJSONSerialization JSONObjectWithData:dataServer options:0 error:nil]];
    
    NSString *strng=[[NSString alloc]initWithData:dataServer encoding:NSUTF8StringEncoding];
    
    NSString *jsonString = strng;
    
    NSRange startRange = [jsonString rangeOfString:@"{"];
    NSRange endRange = [jsonString rangeOfString:@"}]}"];
    
    NSRange searchRange = NSMakeRange(0 , endRange.location+3);
    NSString *aString=[jsonString substringWithRange:searchRange];
    
    
    NSData *JSONdata = [[NSMutableData alloc]init];
    JSONdata=[aString dataUsingEncoding:NSUTF8StringEncoding];
    NSError * error = nil;
    
    NSMutableDictionary *response = [NSJSONSerialization JSONObjectWithData:JSONdata options:0 error:&error];
    
    
    arrotherusers=[[NSMutableArray alloc]init];
    arrSelectedUserNew = [[NSMutableArray alloc]init];
    
    
    //idarray=[response valueForKey:@"groups"];
    arrotherusers=[response valueForKey:@"otherusers"];
    
    
    for (int i=0; i<=arrotherusers.count-1; i++) {
        
        CSUserModel *chatlist=[[CSUserModel alloc]init];
        
        
        chatlist.userID=[[arrotherusers objectAtIndex:i]valueForKey:@"user_id"];
        chatlist.name=[[arrotherusers objectAtIndex:i]valueForKey:@"firstName"];
        chatlist.created=[NSNumber numberWithLong:[[[arrotherusers objectAtIndex:i]valueForKey:@"created"]integerValue]];
        
        
        [arrSelectedUserNew addObject:chatlist];
        

    }
    
    NSMutableSet *names = [NSMutableSet set];
    for (id obj in arrSelectedUserNew) {
        //        NSString *destinationName = [obj destinationname];
        if (![names containsObject:obj]) {
            [arrSelectedUser addObject:obj];
            [names addObject:obj];
        }
    }
    
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [_tblAddUser reloadData];
    
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return arrSelectedUser.count;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellId = @"cellid";
    UITableViewCell * cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    
    CSUserModel *model=[arrSelectedUser objectAtIndex:indexPath.row];
    
    cell.textLabel.text=model.name;
    
    if ([arrUserSelect containsObject:model]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;

    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;

    }
    
    return cell;
    
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell=[_tblAddUser cellForRowAtIndexPath:indexPath];
    
    CSUserModel *seluser=[arrSelectedUser objectAtIndex:indexPath.row];
    
    if (cell.accessoryType==UITableViewCellAccessoryCheckmark) {
        cell.accessoryType = UITableViewCellAccessoryNone;
        [arrUserSelect removeObject:seluser];
    }
    
    else{
        
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        [arrUserSelect addObject:seluser];
        
    }
}



-(void)btnCreateclicked{
    
    dispatch_async(dispatch_get_main_queue(), ^(void){
//        [_txtgroupname resignFirstResponder];
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    });
    
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus==NotReachable)
    {
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"No internet connection"
                                                            message:nil
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        
        [alertView show];
        
        
        
    }
    
    
    else{
        
        NSDate *dat = [NSDate date];
        NSDateFormatter *dateFormatter   = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC+05:30"]];
        
        [dateFormatter setDateFormat:@"MMddyyyyHH:mm:ss"];
        
        NSString * timestamp =[dateFormatter stringFromDate:dat];
        
        NSLog(@"%@",timestamp);
        NSString *trimmedString;
        
        NSString * groupName = _txtgroupname.text;
        
        
        NSString *trimmedString1 = [groupName stringByTrimmingCharactersInSet:
                                    [NSCharacterSet whitespaceCharacterSet]];
        
        NSCharacterSet *cset = [NSCharacterSet characterSetWithCharactersInString:@"!@#$%&*()+'\";:=,/?[]"];
        NSRange range = [groupName rangeOfCharacterFromSet:cset];
        
        
        if (range.location != NSNotFound){
            // ( or ) are present
            
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Special Characters are not allowed"
                                                                message:nil
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
            [alertView setTag:4];
            
            [alertView show];
            
        } else if ([trimmedString1 isEqualToString:@""]) {
            NSLog(@"no room id");
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Enter group name"
                                                                message:nil
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
            
            [alertView setTag:3];
            [alertView show];
            
            
        } else if (arrUserSelect.count==0) {
            
            
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                                message:@"Atleast one user must be selected"
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
            [alertView setTag:1];
            [alertView show];
            
            
        } else{
            
            NSRange whiteSpaceRange = [_txtgroupname.text rangeOfCharacterFromSet:[NSCharacterSet whitespaceCharacterSet]];
            if (whiteSpaceRange.location != NSNotFound) {
                NSLog(@"Found whitespace");
                trimmedString = [_txtgroupname.text stringByReplacingOccurrencesOfString:@" " withString:@""];
                
                groupname=[trimmedString stringByAppendingString:timestamp];
            }
            else{
                
                groupname=[_txtgroupname.text stringByAppendingString:timestamp];
            }
            
            
            
            
            
            NSString *string= [NSString stringWithFormat:@"http://148.251.133.82/spikachatws/CreateGroup/creator_id/%@/group_name/%@/room_id/%@",_strid,_txtgroupname.text,groupname];
            NSLog(@"%@",string);
            NSString* webStringURL = [string stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *url = [NSURL URLWithString:webStringURL];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setHTTPMethod:@"POST"];
            NSURLResponse *response;
            NSError *err;
            NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
            NSLog(@"responseData: %@", responseData);
            NSString *str = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
            NSLog(@"responseData: %@", str);
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData
                                                                 options:kNilOptions
                                                                   error:nil];
            grp_id=[json valueForKey:@"groupid"];
            
            if ([[json valueForKey:@"response"] isEqualToNumber:[NSNumber numberWithInt:1]]) {
                
                for (int i=0; i<=arrUserSelect.count-1; i++) {
                    
                    CSUserModel *user=[[CSUserModel alloc]init];
                    user=[arrUserSelect objectAtIndex:i];
                    int struserids=[user.userID intValue];
                    NSString *struserone=[NSString stringWithFormat:@"%i", struserids];
                    
                    [arrusernames addObject:user.name];
                    [arruserids addObject:struserone];
                    
                    NSString *string= [NSString stringWithFormat:@"http://148.251.133.82/spikachatws/AddMemberToGroup/member_id/%@/group_id/%@",user.userID,[json valueForKey:@"groupid"]];
                    NSLog(@"%@",string);
                    NSString* webStringURL2 = [string stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                    NSURL *url2 = [NSURL URLWithString:webStringURL2];
                    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url2];
                    [request setHTTPMethod:@"POST"];
                    NSURLResponse *response;
                    NSError *err;
                    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
                    NSLog(@"responseData: %@", responseData);
                    NSString *str2 = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
                    NSLog(@"responseData: %@", str);
                    NSDictionary* json2 = [NSJSONSerialization JSONObjectWithData:responseData
                                                                          options:kNilOptions
                                                                            error:nil];
                    
                    if (user==[arrUserSelect lastObject]) {
                        if ([[json valueForKey:@"response"] isEqualToNumber:[NSNumber numberWithInt:1]]) {
                            
                            // [arruserids addObject:_loggedinuser.userID];
                            
                            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:arruserids options:NSJSONWritingPrettyPrinted error:nil];
                            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                            
                            SQLite *sqlObj=[[SQLite alloc] initWithSQLFile:@"SpikaChatDB.sqlite"];
                            
                            [sqlObj openDb];
                            
                            
                            
                            NSString *query = [NSString stringWithFormat:@"insert into tblgroups (groupid,groupname,grouproomid,groupusers,groupcreatorid) values ('%@','%@','%@','%@','%@')",[json valueForKey:@"groupid"],_txtgroupname.text,groupname,jsonString,_strid];
                            
                            
                            
                            [sqlObj insertDb:query];
                            
                            
                            [sqlObj closeDb];
                            
                            
                            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Group Created Successfully"
                                                                                message:nil
                                                                               delegate:self
                                                                      cancelButtonTitle:nil
                                                                      otherButtonTitles:@"OK", nil];
                            [alertView setTag:2];
                            
                            [alertView show];
                            
                            
                        }
                    }
                    
                    
                }
            }
            
            
        }
    }
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag==2) {
        
        if(buttonIndex == 0)//OK button pressed
        {

            
            
            NSDictionary *parameters = @{
                                         paramUserID :grp_id,
                                         paramName : _txtgroupname.text,
                                         paramAvatarURL :@"http://ossdemo.spika.chat/spika/img/avatar.jpg",
                                         paramRoomID:groupname
                                         
                                         };
            CSGroupModel *modgrp=[[CSGroupModel alloc]init];
            modgrp.room_id=groupname;
            modgrp.group_name=_txtgroupname.text;
            modgrp.group_id=grp_id;
            modgrp.creator_id=_strid;
            modgrp.members=arruserids;
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:modgrp];
            [defaults setObject:data forKey:@"createdgroup"];
            [defaults synchronize];
            NSLog(@"%@",parameters);
            
            
            
            //            CSChatViewController *csvc=[[CSChatViewController alloc]initWithParameters:parameters];
            //            csvc.roomID=groupname;
            //            csvc.zhenda=@"1";
            //            _txtgroupname.text=@"";
            [arrUserSelect removeAllObjects];
            [self.navigationController popViewControllerAnimated:YES];
            
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];

            
        }
    } else if (alertView.tag==1) {
        
        if(buttonIndex == 0)
        {
            [MBProgressHUD hideHUDForView:self.view animated:YES];

        }
    } else if (alertView.tag==3) {
        
        if(buttonIndex == 0)
        {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
        }
    } else if (alertView.tag==4) {
        
        if(buttonIndex == 0)
        {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
        }
    }
}

- (NSString *) GetUTCDateTimeFromLocalTime:(NSString *)IN_strLocalTime
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate  *objDate    = [dateFormatter dateFromString:IN_strLocalTime];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    NSString *strDateTime   = [dateFormatter stringFromDate:objDate];
    return strDateTime;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
