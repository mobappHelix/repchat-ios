//
//  SelectUserTableViewCell.h
//  Spika
//
//  Created by HelixTech-Admin  on 12/10/2017.
//  Copyright © 2017 Clover Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectUserTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imageViewProfile;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewTick;
@property (strong, nonatomic) IBOutlet UILabel *labelName;
@property (strong, nonatomic) IBOutlet UILabel *labelLastMessage;
@property (strong, nonatomic) IBOutlet UILabel *labelLastMessageTime;

@end
