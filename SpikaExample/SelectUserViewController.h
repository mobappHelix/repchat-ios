//
//  SelectUserViewController.h
//  Spika
//
//  Created by HelixTech-Admin  on 04/07/2017.
//  Copyright © 2017 Clover Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSUserModel.h"
#import "CSConfig.h"
#import "CSChatViewController.h"
#import "CSCustomConfig.h"
#import "CSGroupModel.h"
#import "CreateGroupViewController.h"
#import "Reachability.h"
#import "Selectusercell.h"
#import "MBProgressHUD.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "FTPopOverMenu.h"


@interface SelectUserViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,NSURLConnectionDataDelegate>{

    NSMutableArray *arrUserSelect;
//    NSMutableArray *arrSelectedUser;
    NSMutableData *dataServer;
//    NSMutableData *dataServer1;

    NSURLConnection *connectionObj;
//    NSURLConnection *connectionObj1;
    NSString *revroomid;
    
    NSMutableArray *usersArray;

    
    NSMutableArray *arrmemb;
//    NSMutableArray *arrotherusers;
//    NSMutableArray *idarray;
    
    NSMutableArray * mainArray;
    
//    NSMutableArray * userArray;
//    NSMutableArray * groupArray;
    UIBarButtonItem *Item3;
    UIBarButtonItem *Item4;
    
    NSMutableArray * array1;
    
    NSString * refresh;


//    NSMutableArray * detailArray;
}

@property (weak, nonatomic) IBOutlet UIBarButtonItem *infoButtn;
@property(nonatomic,strong) IBOutlet UITableView *tblselectedUser;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sideBarButton;
@property(nonatomic,strong) NSString *strroomid;
@property(nonatomic,strong) NSString *strusename;
@property(nonatomic,strong) NSMutableArray *arrmemb;
@property(nonatomic,strong) CSUserModel *loggedinuser;
@property(nonatomic,strong) IBOutlet UINavigationItem *titlebar;
@property (strong, nonatomic) IBOutlet UILabel *userName;

- (IBAction)infoButtonPressed:(UIBarButtonItem *)sender;

-(void)parse;
-(void)serverChecking;
-(void)pushViewController:(UIViewController *)controller;

@end
