//
//  SelectUserViewController.m
//  Spika
//
//  Created by HelixTech-Admin  on 04/07/2017.
//  Copyright © 2017 Clover Studio. All rights reserved.
//

#import "SelectUserViewController.h"
#import "AppDelegate.h"
#import "SWRevealViewController.h"



@interface SelectUserViewController ()

@end

@implementation SelectUserViewController

static NSString *cellid=@"cellid";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.selectUserViewController = self;
    
    [CSCustomConfig sharedInstance].server_url = kAppBaseUrl;
    [CSCustomConfig sharedInstance].socket_url = kAppSocketURL;
    
    
//    SWRevealViewController *revealViewController = self.revealViewController;
//    if ( revealViewController )
//    {
//        [_sideBarButton setTarget: self.revealViewController];
//        [_sideBarButton setAction: @selector( revealToggle: )];
//        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
//    }
    
    
    
//    [self.tblselectedUser registerNib:[UINib nibWithNibName:@"Selectusercell" bundle:nil]
//          forCellReuseIdentifier:@"cellid"];
    
//    [_tblselectedUser registerNib:[UINib nibWithNibName:@"Selectusercell" bundle:nil] forCellReuseIdentifier:cellid];

    
//    [_tblselectedUser registerNib:[UINib nibWithNibName:@"Selectusercell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:cellid];

    
    [self.navigationController.navigationBar setTintColor:[UIColor blackColor]];
    
    self.navigationItem.title=@"Repchat";
  
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage: [UIImage imageNamed:@"group"] forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, 40, 35);
    [button addTarget:self action:@selector(creategroup) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *Item1 = [[UIBarButtonItem alloc] initWithCustomView:button];
    
    
    UIButton *item = [UIButton buttonWithType:UIButtonTypeCustom];
    [item setImage: [UIImage imageNamed:@"ic_room_chat"] forState:UIControlStateNormal];
    item.frame = CGRectMake(0, 0, 40, 40);
    [item addTarget:self action:@selector(roomchat) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *Item2 = [[UIBarButtonItem alloc] initWithCustomView:item];
    
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button2 setImage: [UIImage imageNamed:@"ic_more"] forState:UIControlStateNormal];
    button2.frame = CGRectMake(0, 0, 40, 40);
    [button2 addTarget:self action:@selector(moreButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    Item4 = [[UIBarButtonItem alloc] initWithCustomView:button2];
 
 
    self.navigationItem.rightBarButtonItems = @[Item4,Item2,Item1];
    
    UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button1 setImage: [UIImage imageNamed:@"info1"] forState:UIControlStateNormal];
    button1.frame = CGRectMake(0, 0, 40, 40);
    [button1 addTarget:self action:@selector(infoButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    Item3 = [[UIBarButtonItem alloc] initWithCustomView:button1];
    
    
    UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:self action:@selector(backButtonTapped)];
    self.navigationItem.hidesBackButton = YES;
//    self.navigationItem.leftBarButtonItem = back;
    
    self.navigationItem.leftItemsSupplementBackButton = YES;

//    self.navigationItem.leftBarButtonItems = @[back,Item3];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
   
    NSData *userdata = [defaults objectForKey:@"Loggeduser"];
    _loggedinuser = [NSKeyedUnarchiver unarchiveObjectWithData:userdata];
    
    _userName.text = [NSString stringWithFormat:@"%@",_loggedinuser.name];
    
    arrUserSelect=[[NSMutableArray alloc]init];
//    arrSelectedUser=[[NSMutableArray alloc]init];

    
    revroomid=_strroomid;
    
   // [self createusers];
    //[self checkuser];
    
     arrmemb=[[NSMutableArray alloc]init];
    

    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    
    

    
//    [CSCustomConfig sharedInstance].server_url = kAppBaseUrl;
//    [CSCustomConfig sharedInstance].socket_url = kAppSocketURL;
//    
//    [self.tblselectedUser registerNib:[UINib nibWithNibName:@"Selectusercell" bundle:nil]
//               forCellReuseIdentifier:@"cellid"];
//    
//    self.navigationItem.title=@"Repchat";
    
    
//    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
//    [button setImage: [UIImage imageNamed:@"group"] forState:UIControlStateNormal];
//    button.frame = CGRectMake(0, 0, 40, 35);
//    [button addTarget:self action:@selector(creategroup) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem *Item1 = [[UIBarButtonItem alloc] initWithCustomView:button];
//    
//    
//    UIButton *item = [UIButton buttonWithType:UIButtonTypeCustom];
//    [item setImage: [UIImage imageNamed:@"ic_room_chat"] forState:UIControlStateNormal];
//    item.frame = CGRectMake(0, 0, 40, 40);
//    [item addTarget:self action:@selector(roomchat) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem *Item2 = [[UIBarButtonItem alloc] initWithCustomView:item];
//    
//    
//    self.navigationItem.rightBarButtonItems = @[Item2,Item1];
    
//    [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"splashFlag"];

    [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"groupEdit"];
    [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"attachment"];


    
    refresh = @"0";

    
    [self parse];


  // [self serverChecking];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)createusers{
    
    CSUserModel *user1=[[CSUserModel alloc]init];
    user1.name=@"Jayesh";
    user1.roomID=@"123";
    [arrUserSelect addObject:user1];
    
    CSUserModel *user2=[[CSUserModel alloc]init];
    user2.name=@"Sailee";
    user2.roomID=@"123";
    [arrUserSelect addObject:user2];
    
    CSUserModel *user3=[[CSUserModel alloc]init];
    user3.name=@"Atmaram";
    user3.roomID=@"234";
    [arrUserSelect addObject:user3];
    
    CSUserModel *user4=[[CSUserModel alloc]init];
    user4.name=@"Gaurish";
    user4.roomID=@"234";
    [arrUserSelect addObject:user4];
    
    CSUserModel *user5=[[CSUserModel alloc]init];
    user5.name=@"Shubhala";
    user5.roomID=@"345";
    [arrUserSelect addObject:user5];
    
    
    CSUserModel *user6=[[CSUserModel alloc]init];
    user6.name=@"Mithilesh";
    user6.roomID=@"345";
    [arrUserSelect addObject:user6];
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return mainArray.count;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *cellid=@"cellid";
    
   // CSUserModel *model=[arrSelectedUser objectAtIndex:indexPath.row];
    
//        Selectusercell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
    
//    Selectusercell *cell=[_tblselectedUser dequeueReusableCellWithIdentifier:cellid forIndexPath:indexPath];
//    
//    if (cell==nil) {
//        cell=[[Selectusercell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellid];
//    }
    
    Selectusercell *cell = (Selectusercell *)[tableView dequeueReusableCellWithIdentifier:@"cellid"];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"Selectusercell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    
    
    
    
    
    CSUserModel * userModel = [[CSUserModel alloc]init];
    
    userModel = [mainArray objectAtIndex:indexPath.row];
    
    
      if ([userModel.userType isEqualToString:@"users"]) {
          
          
          cell.lblname.text= userModel.name;
          [cell.imguser sd_setImageWithURL:[NSURL URLWithString:userModel.imagePath]
                                   placeholderImage:[UIImage imageNamed:@"ic_user_list_default"]];
//          cell.lbltime.text= userModel.lastMessageTime;
//          cell.lblsubname.text= userModel.lastMessage;

          
          
          Reachability *reachability = [Reachability reachabilityForInternetConnection];
          NetworkStatus networkStatus = [reachability currentReachabilityStatus];
          if(networkStatus==NotReachable)
          {
              NSLog(@"Offline");
              
              
              if ([array1 containsObject:userModel.roomID]) {
                  NSLog(@"UserName :%@",userModel.name);
                  cell.lbltime.text= userModel.lastMessageTime;
                  cell.lblsubname.text= userModel.lastMessage;
                  
//                  NSLog(@"Last Message Time:%@",userModel.lastMessageTime);
                  NSLog(@"Last Message:%@:%@",userModel.name,userModel.lastMessage);

              } else {
                  cell.lbltime.text= @"";
                  cell.lblsubname.text= @"";

              }
              
              
          } else {
              cell.lbltime.text= userModel.lastMessageTime;
              cell.lblsubname.text= userModel.lastMessage;
              
              NSLog(@"Last Message Time:%@",userModel.lastMessageTime);
              NSLog(@"Last Message:%@",userModel.lastMessage);

              
          }
      
      
      } else if ([userModel.userType isEqualToString:@"groups"]) {
          
          
          cell.lblname.text=userModel.group_name;
          cell.imguser.image=[UIImage imageNamed:@"ic_user_list_default"];
          
//          cell.lbltime.text= userModel.lastMessageTime;
//          cell.lblsubname.text= userModel.lastMessage;


          
          
          Reachability *reachability = [Reachability reachabilityForInternetConnection];
          NetworkStatus networkStatus = [reachability currentReachabilityStatus];
          if(networkStatus==NotReachable)
          {
              
              if ([array1 containsObject:userModel.roomID]) {
                  NSLog(@"UserName :%@",userModel.name);
                  cell.lbltime.text= userModel.lastMessageTime;
                  cell.lblsubname.text= userModel.lastMessage;
                  
//                  NSLog(@"Last Message Time:%@",userModel.lastMessageTime);
                  NSLog(@"Last Message:%@:%@",userModel.group_name,userModel.lastMessage);


              } else {
                  cell.lbltime.text= @"";
                  cell.lblsubname.text= @"";

              }
              
              
          }else {
              cell.lbltime.text= userModel.lastMessageTime;
              cell.lblsubname.text= userModel.lastMessage;
              
              NSLog(@"Last Message Time:%@",userModel.lastMessageTime);
              NSLog(@"Last Message:%@",userModel.lastMessage);
              

          }

          
      }
    

    
  
    return cell;
    
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return 80;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    CSUserModel * userModel = [mainArray objectAtIndex:indexPath.row];


    if ([userModel.userType isEqualToString:@"users"]) {
        
//        NSString * userroomid = userModel.roomID;
//        
//        NSLog(@"Room Id : %@",userroomid);
        
        NSString *userroomid;
        
        if ([_loggedinuser.userID intValue]>[userModel.userID intValue]) {
            
            userroomid=[[[NSString stringWithFormat:@"%@",userModel.userID]stringByAppendingString:@"*"]stringByAppendingString:[NSString stringWithFormat:@"%@",_loggedinuser.userID]];
        } else {
            
            userroomid=[[[NSString stringWithFormat:@"%@",_loggedinuser.userID]stringByAppendingString:@"*"]stringByAppendingString:[NSString stringWithFormat:@"%@",userModel.userID]];
        }
        
        
        
        NSDictionary *parameters = @{
                                     paramUserID :_loggedinuser.userID,
                                     paramName : _loggedinuser.name,
                                     paramAvatarURL :@"http://ossdemo.spika.chat/spika/img/avatar.jpg",
                                     paramRoomID:userroomid
                                     };
        CSChatViewController *csvc=[[CSChatViewController alloc]initWithParameters:parameters];
        csvc.roomID=userroomid;
        csvc.zhenda=@"3";
        csvc.strtitlename=[NSString stringWithFormat:@"%@",userModel.name];
        [self.navigationController pushViewController:csvc animated:YES];

        
        
//        CSUserModel *model=[arrSelectedUser objectAtIndex:indexPath.row];
//        NSNumber *numb=[NSNumber numberWithInteger:[model.userID integerValue]];
//        NSString *struserid=[numb stringValue];
//        NSNumber *numb2=[NSNumber numberWithInteger:[_strroomid integerValue]];
//         NSString *struserid2=[numb2 stringValue];
//        
//        if ([struserid intValue]>[struserid2 intValue]) {
//            
//            NSString *userroomid=[[[NSString stringWithFormat:@"%@", struserid2]stringByAppendingString:@"*"]stringByAppendingString:struserid];
//            
//            NSLog(@"Room Id : %@",userroomid);
//            
//            NSDictionary *parameters = @{
//                                         paramUserID :_loggedinuser.userID,
//                                         paramName : _loggedinuser.name,
//                                         paramAvatarURL :@"http://ossdemo.spika.chat/spika/img/avatar.jpg",
//                                         paramRoomID:userroomid
//                                         };
//            CSChatViewController *csvc=[[CSChatViewController alloc]initWithParameters:parameters];
//            csvc.roomID=userroomid;
//            csvc.zhenda=@"3";
//            csvc.strtitlename=model.name;
//            [self.navigationController pushViewController:csvc animated:YES];
//
//        }
//        else{
//        
//            NSString *userroomid=[[[NSString stringWithFormat:@"%@", struserid]stringByAppendingString:@"*"]stringByAppendingString:struserid2];
//            
//            NSLog(@"Room Id : %@",userroomid);
//
//            
//            NSDictionary *parameters = @{
//                                         paramUserID :_loggedinuser.userID,
//                                         paramName : _loggedinuser.name,
//                                         paramAvatarURL :@"http://ossdemo.spika.chat/spika/img/avatar.jpg",
//                                         paramRoomID:userroomid
//                                         };
//            CSChatViewController *csvc=[[CSChatViewController alloc]initWithParameters:parameters];
//            csvc.zhenda=@"3";
//            csvc.strtitlename=model.name;
//            [self.navigationController pushViewController:csvc animated:YES];
//        
//        }
    } else if ([userModel.userType isEqualToString:@"groups"]) {
    

        
        NSString *grproomid = userModel.roomID;

        
        NSDictionary *parameters = @{
                                     paramUserID :_loggedinuser.userID,
                                     paramName : _loggedinuser.name,
                                     paramAvatarURL :@"http://ossdemo.spika.chat/spika/img/avatar.jpg",
                                     paramRoomID:grproomid
                                     };
        NSLog(@"%@",parameters);
        
        
        CSGroupModel *grp = [[CSGroupModel alloc]init];
        grp.group_id = userModel.group_id;
        grp.group_name = userModel.group_name;
        grp.creator_id = userModel.creator_id;
        grp.room_id = userModel.roomID;
        grp.lastMessageTime = userModel.lastMessageTime;
        grp.lastMessage = userModel.lastMessage;
        grp.members =userModel.members;
        
        
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:grp];
        [defaults setObject:data forKey:@"selectedgroup"];
        [defaults synchronize];
       
        CSChatViewController *csvc=[[CSChatViewController alloc]initWithParameters:parameters];
        
        csvc.roomID=grproomid;
        
      
        [self.navigationController pushViewController:csvc animated:YES];
   
    
    }
    
    else{
    NSLog(@"User selected");
    }
    
}

//-(void)checkuser{
//
//    for (int i=0; i<=arrUserSelect.count-1; i++) {
//        CSUserModel *user=[arrUserSelect objectAtIndex:i];
//        
//        if ([user.roomID isEqualToString:_strroomid]&&![user.name isEqualToString:_strusename]) {
//            [arrSelectedUser addObject:user];
//            
//            [_tblselectedUser reloadData];
//        }
//        
//        else{
//        
//            NSLog(@"no user");
//        
//        }
//    }
//}


-(void)serverChecking{
    
    NSString *fcmToken = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"fcmToken"];
    //Live
    NSURL *urlObj=[[NSURL alloc]initWithString:[NSString stringWithFormat:@"http://148.251.46.55:8080/get_users/%@/%@/2",_loggedinuser.userID,fcmToken]];
    //Local
//    NSURL *urlObj=[[NSURL alloc]initWithString:[NSString stringWithFormat:@"http://192.168.0.36:8080/get_users/%@/%@/2",_loggedinuser.userID,fcmToken]];
    
//        NSURL *urlObj=[[NSURL alloc]initWithString:[NSString stringWithFormat:@"http://192.168.0.141:8080/get_users/%@/%@/2",_loggedinuser.userID,fcmToken]];


    
    NSURLRequest *requestObj=[[NSURLRequest alloc]initWithURL:urlObj];
    connectionObj=[[NSURLConnection alloc]initWithRequest:requestObj delegate:self];
    if(connectionObj)
    {
        NSLog(@"Successful connection");
        dataServer=[[NSMutableData alloc]init];
    }
    
//    NSURL *urlObj1=[[NSURL alloc]initWithString:[NSString stringWithFormat:@"http://148.251.46.55:8080/lastMessages"]];
//    NSURLRequest *requestObj1=[[NSURLRequest alloc]initWithURL:urlObj1];
//    connectionObj1=[[NSURLConnection alloc]initWithRequest:requestObj1 delegate:self];
//    if(connectionObj1)
//    {
//        NSLog(@"Successful connection");
//        dataServer1=[[NSMutableData alloc]init];
//    }
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    
    if (connection == connectionObj) {
        [dataServer setLength:0];
    }
//    else if (connection == connectionObj1) {
//        [dataServer1 setLength:0];
//    }
    
    
}

-(void)viewDidAppear:(BOOL)animated{
    
    
    
    
}

//-(void) viewWillDisappear:(BOOL)animated {
//    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
//        // back button was pressed.  We know this is true because self is no longer
//        // in the navigation stack.
//        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"splashFlag"];
//    }
//    [super viewWillDisappear:animated];
//}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    
    
    if (connection == connectionObj) {
        [dataServer appendData:data];
    }
//    else if (connection == connectionObj1) {
//        [dataServer1 appendData:data];
//    }
    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
    //[MBProgressHUD hideHUDForView:self.view animated:YES];
    
    if (connection == connectionObj) {
//        userArray = [[NSMutableArray alloc]init];
//        groupArray = [[NSMutableArray alloc]init];
        
        NSString *strng=[[NSString alloc]initWithData:dataServer encoding:NSUTF8StringEncoding];
        
//        NSString *jsonString = strng;
//        
//        NSRange startRange = [jsonString rangeOfString:@"{"];
//        NSRange endRange = [jsonString rangeOfString:@"}]}"];
//        
//        NSRange searchRange = NSMakeRange(0 , endRange.location+3);
//        NSString *aString=[jsonString substringWithRange:searchRange];
        
        
        NSData *JSONdata = [[NSMutableData alloc]init];
        JSONdata=[strng dataUsingEncoding:NSUTF8StringEncoding];
        NSError * error = nil;
        
        NSMutableDictionary *response = [NSJSONSerialization JSONObjectWithData:JSONdata options:0 error:&error];
        
        
//        arrotherusers=[[NSMutableArray alloc]init];
//        idarray=[[NSMutableArray alloc]init];
        
        usersArray = [[NSMutableArray alloc]init];
        
        
        usersArray = [response valueForKey:@"data"];
        
        
        
        mainArray = [[NSMutableArray alloc]init];

        
        for (int i = 0; i< usersArray.count; i++) {
            
//            NSMutableDictionary * dict = [usersArray objectAtIndex:i];
            
            CSUserModel * userModel = [[CSUserModel alloc]init];
            
            if ([[[usersArray objectAtIndex:i] valueForKey:@"type"] isEqualToString:@"users"]) {
//                [arrotherusers addObject:[usersArray objectAtIndex:i]];
                
                userModel.userID = [[usersArray objectAtIndex:i]valueForKey:@"user_id"];
                userModel.name = [[usersArray objectAtIndex:i]valueForKey:@"firstName"];
                userModel.registereddate = [[usersArray objectAtIndex:i]valueForKey:@"registered_date"];
                userModel.lastMessage = [[usersArray objectAtIndex:i]valueForKey:@"last_message"];
                userModel.lastMessageTime = [[usersArray objectAtIndex:i]valueForKey:@"last_message_time"];
                userModel.roomID = [[usersArray objectAtIndex:i]valueForKey:@"roomID"];
                userModel.userType = [[usersArray objectAtIndex:i]valueForKey:@"type"];
                userModel.imagePath = [[usersArray objectAtIndex:i]valueForKey:@"profile_url"];
                
//                [[UIImageView new] sd_setImageWithURL:[[usersArray objectAtIndex:i]valueForKey:@"profile_url"]];

                
                userModel.lastMessageCreated = [[usersArray objectAtIndex:i]valueForKey:@"last_message_created"];
                
                
                NSString *userroomid;
                
                if ([_strroomid intValue]>[[[usersArray objectAtIndex:i]valueForKey:@"user_id"] intValue]) {
                    
                    userroomid=[[[NSString stringWithFormat:@"%@",[[usersArray objectAtIndex:i]valueForKey:@"user_id"]]stringByAppendingString:@"*"]stringByAppendingString:[NSString stringWithFormat:@"%@",_strroomid]];
                } else {
                    
                    userroomid=[[[NSString stringWithFormat:@"%@",_strroomid]stringByAppendingString:@"*"]stringByAppendingString:[NSString stringWithFormat:@"%@",[[usersArray objectAtIndex:i]valueForKey:@"user_id"]]];
                }
                
                
                
                SQLite *sqlObj=[[SQLite alloc] initWithSQLFile:@"SpikaChatDB.sqlite"];
                
                [sqlObj openDb];
                
                NSString *str = userModel.lastMessage;
                str = [str stringByReplacingOccurrencesOfString:@"'" withString:@""];
                
                NSString *query = [NSString stringWithFormat:@"insert into otherusers (otheruserid,firstname,registereddate,userid,lastmessage,lastmessagetime,userType,lastmessagecreated,profileimage) values ('%@','%@','%@','%@','%@','%@','%@','%@','%@')",userModel.userID,userModel.name,userModel.registereddate,userModel.roomID,str,userModel.lastMessageTime,userModel.userType,userModel.lastMessageCreated,userModel.imagePath];
                
                
                
                [sqlObj insertDb:query];
                
                
                [sqlObj closeDb];

                
                
                
            } else if ([[[usersArray objectAtIndex:i] valueForKey:@"type"] isEqualToString:@"groups"]) {
//                [idarray addObject:[usersArray objectAtIndex:i]];
                
                userModel.creator_id=[[usersArray objectAtIndex:i]valueForKey:@"creator_id"];
                userModel.group_id=[[usersArray objectAtIndex:i]valueForKey:@"group_id"];
                userModel.group_name=[[usersArray objectAtIndex:i]valueForKey:@"group_name"];
                userModel.roomID=[[usersArray objectAtIndex:i]valueForKey:@"room_id"];
                userModel.members=[[[usersArray objectAtIndex:i]valueForKey:@"group_info"]valueForKey:@"member"];
                userModel.lastMessage=[[usersArray objectAtIndex:i]valueForKey:@"last_message"];
                userModel.lastMessageTime=[[usersArray objectAtIndex:i]valueForKey:@"last_message_time"];
                userModel.userType = [[usersArray objectAtIndex:i] valueForKey:@"type"];
                userModel.lastMessageCreated = [[usersArray objectAtIndex:i]valueForKey:@"last_message_created"];

//                userModel.imagePath = [[usersArray objectAtIndex:i]valueForKey:@"profile_url"];
                
                NSData *jsonData = [NSJSONSerialization dataWithJSONObject:userModel.members options:NSJSONWritingPrettyPrinted error:nil];
                NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                
                
                SQLite *sqlObj=[[SQLite alloc] initWithSQLFile:@"SpikaChatDB.sqlite"];
                
                [sqlObj openDb];
                
                NSString *str = userModel.lastMessage;
                str = [str stringByReplacingOccurrencesOfString:@"'" withString:@""];
                
                NSString *query = [NSString stringWithFormat:@"insert into tblgroups (groupid,groupname,grouproomid,groupusers,groupcreatorid,userid,lastmessage,lastmessagetime,userType,lastmessagecreated,profileimage,creatorid) values ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",userModel.group_id,userModel.group_name,userModel.roomID,jsonString,userModel.creator_id,_strroomid,str,userModel.lastMessageTime,userModel.userType,userModel.lastMessageCreated,userModel.imagePath,userModel.creator_id];
                
                [sqlObj insertDb:query];
                
                
                [sqlObj closeDb];
                
                

            }
            
            [mainArray addObject:userModel];

        }
        
        
        
        [_tblselectedUser reloadData];

        
        if ([refresh isEqualToString:@"1"]) {
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];

            refresh = @"0";
            
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Refresh Complete" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];

        }
        
        
        
        
        
        
        
//        arrotherusers=[response valueForKey:@"otherusers"];
//        
//        idarray=[response valueForKey:@"groups"];

//        [self dataArrange];

        
    }
    
//    else if (connection == connectionObj1) {
//        
////        detailArray = [[NSMutableArray alloc]init];
//
//        NSString *strng1=[[NSString alloc]initWithData:dataServer1 encoding:NSUTF8StringEncoding];
//        
//        NSData *JSONdata1 = [[NSMutableData alloc]init];
//        JSONdata1=[strng1 dataUsingEncoding:NSUTF8StringEncoding];
//        NSError * error1 = nil;
//        
//        detailArray = [NSJSONSerialization JSONObjectWithData:JSONdata1
//                                                                     options:0 error:&error1];
//
//
//
//
//    }
   
    
}

//-(void)dataArrange {
//    
////    [userArray removeAllObjects];
////    [groupArray removeAllObjects];
//    
//    if (arrotherusers.count!=0) {
//        
//        
//        for (int i=0; i<=arrotherusers.count-1; i++) {
//            
//            CSUserModel *chatlist=[[CSUserModel alloc]init];
//            
//            
//            
//            chatlist.userID=[[arrotherusers objectAtIndex:i]valueForKey:@"user_id"];
//            chatlist.name=[[arrotherusers objectAtIndex:i]valueForKey:@"firstName"];
//            chatlist.registereddate=[[arrotherusers objectAtIndex:i]valueForKey:@"registered_date"];
//            chatlist.lastMessage=[[arrotherusers objectAtIndex:i]valueForKey:@"last_message"];
//            chatlist.lastMessageTime=[[arrotherusers objectAtIndex:i]valueForKey:@"last_message_time"];
//            
////            chatlist.profileImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[[arrotherusers objectAtIndex:i]valueForKey:@"profile_url"]]]];
//            
//            
//            
//
////            NSData * imageData = UIImageJPEGRepresentation([UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[[arrotherusers objectAtIndex:i]valueForKey:@"profile_url"]]]], 0.0f);
////            
////            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
////            NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
////            
////            NSArray *parts = [[[arrotherusers objectAtIndex:i]valueForKey:@"profile_url"] componentsSeparatedByString:@"/"];
////            NSString *filename = [parts lastObject];
////            
////            NSString *filePath = [documentsPath stringByAppendingPathComponent:filename]; //Add the file name
////            [imageData writeToFile:filePath atomically:YES];
////            
////            chatlist.imagePath = filePath;
//            
//            
//            
//            
//            
//            NSString *userroomid;
//            
//            if ([_strroomid intValue]>[[[arrotherusers objectAtIndex:i]valueForKey:@"user_id"] intValue]) {
//                
//                userroomid=[[[NSString stringWithFormat:@"%@",[[arrotherusers objectAtIndex:i]valueForKey:@"user_id"]]stringByAppendingString:@"*"]stringByAppendingString:[NSString stringWithFormat:@"%@",_strroomid]];
//            } else {
//                
//                userroomid=[[[NSString stringWithFormat:@"%@",_strroomid]stringByAppendingString:@"*"]stringByAppendingString:[NSString stringWithFormat:@"%@",[[arrotherusers objectAtIndex:i]valueForKey:@"user_id"]]];
//            }
//            
//            
////            NSString *userroomid2=[[[NSString stringWithFormat:@"%@", [[arrotherusers objectAtIndex:i]valueForKey:@"user_id"]]stringByAppendingString:@"*"]stringByAppendingString:[NSString stringWithFormat:@"%@",_strroomid]];
//
////            for (int m = 0; m<=detailArray.count-1; m++) {
////                if ([userroomid isEqualToString:[[detailArray objectAtIndex:m]valueForKey:@"roomID"]]) {
////                    chatlist.lastMessage = [[detailArray objectAtIndex:m]valueForKey:@"message"];
////                    chatlist.lastMessageTime = [NSString stringWithFormat:@"%@",[[detailArray objectAtIndex:m]valueForKey:@"createdTime"]];
////                }
////            }
//            
////            [userArray addObject:chatlist];
//            
//            
//            
//            SQLite *sqlObj=[[SQLite alloc] initWithSQLFile:@"SpikaChatDB.sqlite"];
//            
//            [sqlObj openDb];
//            
//            NSString *str = chatlist.lastMessage;
//            str = [str stringByReplacingOccurrencesOfString:@"'" withString:@""];
//            
//            NSString *query = [NSString stringWithFormat:@"insert into otherusers (otheruserid,firstname,registereddate,userid,lastmessage,lastmessagetime) values ('%@','%@','%@','%@','%@','%@')",chatlist.userID,chatlist.name,chatlist.registereddate,_strroomid,str,chatlist.lastMessageTime];
//            
//            
//            
//            [sqlObj insertDb:query];
//            
//            
//            [sqlObj closeDb];
//            
//        }
//    }
//    
//    else{
//        
//        NSLog(@"no otherusers");
//    }
//    
//    
//    if (idarray.count!=0) {
//        
//        for (int j=0; j<=idarray.count-1; j++) {
//            CSGroupModel *grps=[[CSGroupModel alloc]init];
//            
//            grps.creator_id=[[idarray objectAtIndex:j]valueForKey:@"creator_id"];
//            grps.group_id=[[idarray objectAtIndex:j]valueForKey:@"group_id"];
//            grps.group_name=[[idarray objectAtIndex:j]valueForKey:@"group_name"];
//            grps.room_id=[[idarray objectAtIndex:j]valueForKey:@"room_id"];
//            grps.members=[[[idarray objectAtIndex:j]valueForKey:@"group_info"]valueForKey:@"member"];
//            grps.lastMessage=[[idarray objectAtIndex:j]valueForKey:@"last_message"];
//            grps.lastMessageTime=[[idarray objectAtIndex:j]valueForKey:@"last_message_time"];
//            
//            
//            
//            
//            
//
//            NSData * imageData = UIImageJPEGRepresentation([UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[[idarray objectAtIndex:j]valueForKey:@"profile_url"]]]], 0.0f);
//            
//            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//            NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
//            
//            NSArray *parts = [[[arrotherusers objectAtIndex:j]valueForKey:@"profile_url"] componentsSeparatedByString:@"/"];
//            NSString *filename = [parts lastObject];
//            
//            NSString *filePath = [documentsPath stringByAppendingPathComponent:filename]; //Add the file name
//            [imageData writeToFile:filePath atomically:YES];
//            
//            grps.imagePath = filePath;
//            
//            
//            
//            
//            
////            for (int m = 0; m<=detailArray.count-1; m++) {
////                if ([grps.room_id isEqualToString:[[detailArray objectAtIndex:m]valueForKey:@"roomID"]]) {
////                    grps.lastMessage = [[detailArray objectAtIndex:m]valueForKey:@"message"];
////                    grps.lastMessageTime = [NSString stringWithFormat:@"%@",[[detailArray objectAtIndex:m]valueForKey:@"createdTime"]];
////                }
////            }
//           
//            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:grps.members options:NSJSONWritingPrettyPrinted error:nil];
//            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
//            
//            
////            [groupArray addObject:grps];
//            
//            
//            
//            SQLite *sqlObj=[[SQLite alloc] initWithSQLFile:@"SpikaChatDB.sqlite"];
//            
//            [sqlObj openDb];
//            
//            NSString *query = [NSString stringWithFormat:@"insert into tblgroups (groupid,groupname,grouproomid,groupusers,groupcreatorid,userid,lastmessage,lastmessagetime) values ('%@','%@','%@','%@','%@','%@','%@','%@')",grps.group_id,grps.group_name,grps.room_id,jsonString,grps.creator_id,_strroomid,grps.lastMessage,grps.lastMessageTime];
//            
//            [sqlObj insertDb:query];
//            
//            
//            [sqlObj closeDb];
//            
//        }
//    }
//    else{
//        
//        NSLog(@"no groups");
//        
//        
//    }
//    
////    [arrSelectedUser addObjectsFromArray:groupArray];
////    [arrSelectedUser addObjectsFromArray:userArray];
//    
//    
//    
//    
////    [MBProgressHUD hideHUDForView:self.view animated:YES];
//    
//}

-(void)creategroup{

    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus!=NotReachable)
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        CreateGroupViewController *sfvc = [storyboard instantiateViewControllerWithIdentifier:@"create"];
        sfvc.strid=_strroomid;
        // [sfvc setModalPresentationStyle:UIModalPresentationFullScreen];
        [self.navigationController pushViewController:sfvc animated:YES];
        
    }
    else
    {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"No internet connectivity" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        
    }


}

//-(void)viewDidDisappear:(BOOL)animated{
//
//   
//
//}


//-(void) viewWillDisappear:(BOOL)animated {
//    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
//        // back button was pressed.  We know this is true because self is no longer
//        // in the navigation stack.
//        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"splashFlag"];
//    }
//    [super viewWillDisappear:animated];
//}

-(void)deletegroups{

    SQLite *sqlObj=[[SQLite alloc] initWithSQLFile:@"SpikaChatDB.sqlite"];
    
    [sqlObj openDb];
    
    NSString *query = [NSString stringWithFormat:@"delete from tblgroups"];
//    NSString *query = [NSString stringWithFormat:@"delete from tblgroups where userid='%@'",_strroomid];

    
    [sqlObj updateDb:query];
    
    
    [sqlObj closeDb];

}

-(void)deleteotherusers{

    SQLite *sqlObj=[[SQLite alloc] initWithSQLFile:@"SpikaChatDB.sqlite"];
    
    [sqlObj openDb];
    
//    NSString *query = [NSString stringWithFormat:@"delete from otherusers where otheruserid='%@'",_strroomid];
    NSString *query = [NSString stringWithFormat:@"delete from otherusers"];

    
    [sqlObj updateDb:query];
    
    
    [sqlObj closeDb];


}

-(void)getofflineusersandgroups{


    mainArray = [[NSMutableArray alloc]init];
    
    NSMutableArray * userArray = [[NSMutableArray alloc]init];
    NSMutableArray * groupArray = [[NSMutableArray alloc]init];
    
    
    //User
    

    SQLite *sqlObj=[[SQLite alloc] initWithSQLFile:@"SpikaChatDB.sqlite"];
    
    [sqlObj openDb];
    
    
//    NSString *query = [NSString stringWithFormat:@"Select firstname,registereddate,otheruserid,lastmessage,lastmessagetime from otherusers WHERE otheruserid='%@'",_loggedinuser.userID];

        NSString *query = [NSString stringWithFormat:@"Select firstname,registereddate,otheruserid,lastmessage,lastmessagetime,userType,lastmessagecreated,profileimage,userid from otherusers"];
    
    
    [sqlObj readDb:query];
    
    
    
    
    while ([sqlObj hasNextRow])
    {
        CSUserModel * userModel=[[CSUserModel alloc]init];
        
        
        userModel.name=[sqlObj getColumn:0 type:@"text"];
        userModel.registereddate=[sqlObj getColumn:1 type:@"text"];
        userModel.userID=[sqlObj getColumn:2 type:@"text"];

        userModel.lastMessage=[sqlObj getColumn:3 type:@"text"];
        userModel.lastMessageTime=[sqlObj getColumn:4 type:@"text"];
        userModel.userType=[sqlObj getColumn:5 type:@"text"];
        userModel.lastMessageCreated = [sqlObj getColumn:6 type:@"text"];
        userModel.imagePath = [sqlObj getColumn:7 type:@"text"];
        userModel.roomID = [sqlObj getColumn:8 type:@"text"];


        
        [userArray addObject:userModel];
        
       //  [_tblselectedUser reloadData];
    }
    
    [sqlObj closeDb];
    

    //Group
    
    SQLite *sqlObj2=[[SQLite alloc] initWithSQLFile:@"SpikaChatDB.sqlite"];
    
    [sqlObj2 openDb];
    
    
    NSString *query2 = [NSString stringWithFormat:@"Select groupid,groupname,grouproomid,groupusers,groupcreatorid,userid,lastmessage,lastmessagetime,userType,lastmessagecreated,profileimage,creatorid from tblgroups"];
    
//    NSString *query2 = [NSString stringWithFormat:@"Select groupid,groupname,grouproomid,groupusers,groupcreatorid,userid,lastmessage,lastmessagetime from tblgroups WHERE userid='%@'",_loggedinuser.userID];

    
    [sqlObj2 readDb:query2];
    
    
    
    
    while ([sqlObj2 hasNextRow])
    {
        CSUserModel * userModel=[[CSUserModel alloc]init];
        
        
        userModel.group_id=[sqlObj2 getColumn:0 type:@"text"];
        userModel.group_name=[sqlObj2 getColumn:1 type:@"text"];
        userModel.roomID=[sqlObj2 getColumn:2 type:@"text"];
        NSString *jsonString=[sqlObj2 getColumn:3 type:@"text"];
        userModel.creator_id=[sqlObj2 getColumn:4 type:@"text"];
        userModel.lastMessage=[sqlObj2 getColumn:6 type:@"text"];
        userModel.lastMessageTime=[sqlObj2 getColumn:7 type:@"text"];
        userModel.userType=[sqlObj2 getColumn:8 type:@"text"];
        userModel.lastMessageCreated = [sqlObj2 getColumn:9 type:@"text"];
        userModel.imagePath = [sqlObj2 getColumn:10 type:@"text"];
        userModel.creator_id = [sqlObj2 getColumn:11 type:@"text"];
        
        NSLog(@"UserType: %@",[sqlObj2 getColumn:8 type:@"text"]);

        
        NSData *data = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
         NSMutableArray *arrayObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
        userModel.members=arrayObject;
        
        
        [groupArray addObject:userModel];
        
        
    }
    
    [sqlObj2 closeDb];
    
    
    [mainArray addObjectsFromArray:groupArray];
    [mainArray addObjectsFromArray:userArray];
    
    NSArray * array = [[NSMutableArray alloc]initWithArray:mainArray];
    
    
    NSSortDescriptor *sd = [[NSSortDescriptor alloc] initWithKey:@"lastMessageCreated" ascending:NO];
    array = [array sortedArrayUsingDescriptors:@[sd]];
    
    mainArray = [[NSMutableArray alloc]initWithArray:array];
    
    
    array1 = [[NSMutableArray alloc]init];
    
    
    SQLite *sqlObj3=[[SQLite alloc] initWithSQLFile:@"SpikaChatDB.sqlite"];
    
    [sqlObj3 openDb];
    
    
    NSString *query3 = [NSString stringWithFormat:@"Select distinct roomID from tblmessage"];
    
    
    [sqlObj3 readDb:query3];
    
    
    while ([sqlObj3 hasNextRow])
    {
        
        [array1 addObject:[sqlObj3 getColumn:0 type:@"text"]];
        
    }
    
    [sqlObj3 closeDb];

    
    
    
    
    [self.tblselectedUser reloadData];
   

}

-(void)roomchat{

    NSDictionary *parameters = @{
                                 paramUserID :_loggedinuser.userID,
                                 paramName :_loggedinuser.name,
                                 paramAvatarURL :@"http://ossdemo.spika.chat/spika/img/avatar.jpg",
                                 paramRoomID:@"new_demo"
                                 };
    CSChatViewController *csvc=[[CSChatViewController alloc]initWithParameters:parameters];
    csvc.zhenda=@"4";
    
    [self.navigationController pushViewController:csvc animated:YES];


}

-(void)infoButtonTapped:(UIButton *)sender {
    
//    FTPopOverMenuConfiguration *configuration = [FTPopOverMenuConfiguration defaultConfiguration];
//    configuration.menuRowHeight = 45;
//    configuration.menuWidth = 250;
//    configuration.textColor = [UIColor blackColor];
//    configuration.textFont = [UIFont boldSystemFontOfSize:16];
//    configuration.tintColor = [UIColor whiteColor];
//    configuration.borderColor = [UIColor blackColor];
//    configuration.borderWidth = 0.1;
//    configuration.textAlignment = NSTextAlignmentCenter;
//    
//    
//    
//    
//    UIView *view = [Item3 valueForKey:@"view"];
//    
//    [FTPopOverMenu showFromSenderFrame:view.frame withMenuArray:[[NSArray alloc] initWithObjects:[NSString stringWithFormat:@"%@",_loggedinuser.name], nil] doneBlock:nil dismissBlock:nil];
//
////    [FTPopOverMenu shi];
    
}


-(void)moreButtonTapped:(UIButton *)sender {
    
    
    FTPopOverMenuConfiguration *configuration = [FTPopOverMenuConfiguration defaultConfiguration];
    configuration.menuRowHeight = 45;
    configuration.menuWidth = 150;
    configuration.textColor = [UIColor blackColor];
    configuration.textFont = [UIFont boldSystemFontOfSize:16];
    configuration.tintColor = [UIColor whiteColor];
    configuration.borderColor = [UIColor blackColor];
    configuration.borderWidth = 0.1;
    configuration.textAlignment = NSTextAlignmentCenter;
    
    
    UIView *view = [Item4 valueForKey:@"view"];
    
    [FTPopOverMenu showFromSenderFrame:view.frame withMenuArray:@[@"Refresh"] doneBlock:^(NSInteger selectedIndex) {
        if (selectedIndex == 0) {
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];

            
            Reachability *reachability = [Reachability reachabilityForInternetConnection];
            NetworkStatus networkStatus = [reachability currentReachabilityStatus];
            if(networkStatus!=NotReachable)
            {
                refresh = @"1";
                
                [self parse];

                
            }
            else
            {
                refresh = @"0";

                [MBProgressHUD hideHUDForView:self.view animated:YES];
                
                UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"No internet connectivity" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];

            }
        }
        
    } dismissBlock:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)infoButtonPressed:(UIBarButtonItem *)sender {
    
    FTPopOverMenuConfiguration *configuration = [FTPopOverMenuConfiguration defaultConfiguration];
    configuration.menuRowHeight = 45;
    configuration.menuWidth = 250;
    configuration.textColor = [UIColor blackColor];
    configuration.textFont = [UIFont boldSystemFontOfSize:16];
    configuration.tintColor = [UIColor whiteColor];
    configuration.borderColor = [UIColor blackColor];
    configuration.borderWidth = 0.1;
    configuration.textAlignment = NSTextAlignmentCenter;
    
    
    
    
    UIView *view = [_infoButtn valueForKey:@"view"];
    
    [FTPopOverMenu showFromSenderFrame:view.frame withMenuArray:[[NSArray alloc] initWithObjects:[NSString stringWithFormat:@"%@",_loggedinuser.name], nil] doneBlock:nil dismissBlock:nil];
    
    //    [FTPopOverMenu shi];

}

-(void)parse{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSData *userdata = [defaults objectForKey:@"Loggeduser"];
    _loggedinuser = [NSKeyedUnarchiver unarchiveObjectWithData:userdata];
    
    _userName.text = [NSString stringWithFormat:@"%@",_loggedinuser.name];
    
    
    arrUserSelect=[[NSMutableArray alloc]init];
//    arrSelectedUser=[[NSMutableArray alloc]init];
    
    
    revroomid=_strroomid;
    
    // [self createusers];
    //[self checkuser];
    
    arrmemb=[[NSMutableArray alloc]init];
    
    
    
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus!=NotReachable)
    {
        [self deletegroups];
        [self deleteotherusers];
        
        
        
        [self serverChecking];

        //  isoffline=YES;
        

        
        
    }
    else
    {
        // isoffline=NO;
        //[self login:self.parameters];
        
//        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        NSLog(@"Offline");
        
        [self getofflineusersandgroups];
        
        
    }
}

-(void)pushViewController:(UIViewController *)controller {
    
    [self.navigationController pushViewController:controller animated:YES];
    

}

-(void)backButtonTapped {
    
    [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"splashFlag"];

    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
