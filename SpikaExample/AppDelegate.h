//
//  AppDelegate.h
//  Spika
//
//  Created by mislav on 08/12/15.
//  Copyright © 2015 Clover Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UserNotifications/UserNotifications.h>
@import Firebase;
@import FirebaseInstanceID;
@import FirebaseMessaging;
#import "Reachability.h"
@class SelectUserViewController;
#import "UsersViewController.h"
#import "LoginViewController.h"
#import <AudioToolbox/AudioServices.h>
#import "CSUserModel.h"



@interface AppDelegate : UIResponder <UIApplicationDelegate,UNUserNotificationCenterDelegate, FIRMessagingDelegate>{
    
    
    Reachability * internetReachable;
    
    NSURLConnection *connectionObj;
    NSMutableData *responseData;
    NSMutableArray *responseArray;



}

@property (strong, nonatomic) UIWindow *window;
@property (weak, nonatomic) SelectUserViewController * selectUserViewController;
@property (weak, nonatomic) UsersViewController * usersViewController;
@property (weak, nonatomic) LoginViewController * loginViewController;
@property(nonatomic,strong) CSUserModel *loggedinuser;
@property (nonatomic) UIBackgroundTaskIdentifier backgroundTask;




@end

