//
//  UsersViewController.m
//  Spika
//
//  Created by HelixTech-Admin  on 04/07/2017.
//  Copyright © 2017 Clover Studio. All rights reserved.
//

#import "UsersViewController.h"
#import "AppDelegate.h"


@interface UsersViewController ()

@end

@implementation UsersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.usersViewController = self;
    
     arrUserlist=[[NSMutableArray alloc]init];
    
    // arrUserimages=[[NSMutableArray alloc]initWithObjects:[UIImage imageNamed:@"user.png"],[UIImage imageNamed:@"user.png"],[UIImage imageNamed:@"user.png"],[UIImage imageNamed:@"user.png"],[UIImage imageNamed:@"user.png"],[UIImage imageNamed:@"user.png"], nil];
    
    //    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    //    [button setTitle:@"Exit" forState:UIControlStateNormal];
    //    [button setTintColor:[UIColor blackColor]];
    //    button.frame = CGRectMake(0, 0, 60, 40);
    //    [button addTarget:self action:@selector(doExit) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *Item1 = [[UIBarButtonItem alloc] initWithTitle:@"Exit" style:UIBarButtonItemStylePlain target:self action:@selector(doExit)];
    
    self.navigationItem.leftBarButtonItem = Item1;
    
 
        // [self createusers];
    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{

//    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    hud.mode = MBProgressHUDModeAnnularDeterminate;
////    hud.label.text = @"Loading";
    
    NSString *fcmToken = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"fcmToken"];
    NSLog(@"FCM :%@",fcmToken);
    
    if (fcmToken == (id)[NSNull null] || fcmToken.length == 0 )
    {
        NSLog(@"NULL");
        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    }
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus==NotReachable)
    {
        
        //  isoffline=YES;
        
        [self getOfflineusers];
        
        
    }
    else
    {
        // isoffline=NO;
        //[self login:self.parameters];
        
        hud1 = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [self clearusers];
        [self serverChecking];
        
    }

//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];

   
}

-(void)doExit
{
    //show confirmation message to user
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Confirmation"
                                                    message:@"Do you want to exit?"
                                                   delegate:self
                                          cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:@"OK", nil];
    [alert show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex != 0)  // 0 == the cancel button
    {
        //home button press programmatically
        UIApplication *app = [UIApplication sharedApplication];
        [app performSelector:@selector(suspend)];
        
        //wait 2 seconds while app is going background
        [NSThread sleepForTimeInterval:0.0];
        
        //exit app when app is in background
        exit(0);
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return arrUserlist.count;

}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

     NSString *cellid=@"cellid";
    
    CSUserModel *model=[arrUserlist objectAtIndex:indexPath.row];
    
    UITableViewCell *cell=[_tblUsers dequeueReusableCellWithIdentifier:nil];
    
    if (cell==nil) {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        
        CSUserModel *model=[arrUserlist objectAtIndex:indexPath.row];
        
        cell.textLabel.text=model.name;
        cell.imageView.image=[UIImage imageNamed:@"ic_user_list_default"];
    }

    return cell;


}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    CSUserModel *model=[arrUserlist objectAtIndex:indexPath.row];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SelectUserViewController *sfvc = [storyboard instantiateViewControllerWithIdentifier:@"selected"];
    sfvc.strroomid=model.userID;
   // [sfvc setModalPresentationStyle:UIModalPresentationFullScreen];
    [self.navigationController pushViewController:sfvc animated:YES];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:model];
    [defaults setObject:data forKey:@"Loggeduser"];
    [defaults synchronize];



}

-(void)viewDidDisappear:(BOOL)animated{


    [arrUserlist removeAllObjects];


}

-(void)createusers{

    CSUserModel *user1=[[CSUserModel alloc]init];
    user1.name=@"Jayesh";
    user1.roomID=@"123";
    [arrUserlist addObject:user1];
    
    CSUserModel *user2=[[CSUserModel alloc]init];
    user2.name=@"Sailee";
    user2.roomID=@"123";
    [arrUserlist addObject:user2];
    
    CSUserModel *user3=[[CSUserModel alloc]init];
    user3.name=@"Atmaram";
    user3.roomID=@"234";
    [arrUserlist addObject:user3];
    
    CSUserModel *user4=[[CSUserModel alloc]init];
    user4.name=@"Gaurish";
    user4.roomID=@"234";
    [arrUserlist addObject:user4];
    
    CSUserModel *user5=[[CSUserModel alloc]init];
    user5.name=@"Shubhala";
    user5.roomID=@"345";
    [arrUserlist addObject:user5];
    
    
    CSUserModel *user6=[[CSUserModel alloc]init];
    user6.name=@"Mithilesh";
    user6.roomID=@"345";
    [arrUserlist addObject:user6];


}

-(void)serverChecking{
    
    NSURL *urlObj=[[NSURL alloc]initWithString:@"http://148.251.133.82/spikachatws/GetLIstOfUsers"];
    NSURLRequest *requestObj=[[NSURLRequest alloc]initWithURL:urlObj];
    NSURLConnection *connectionObj=[[NSURLConnection alloc]initWithRequest:requestObj delegate:self];
    if(connectionObj)
    {
        NSLog(@"Successful connection");
        dataServer=[[NSMutableData alloc]init];
    }
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    
    
    [dataServer setLength:0];
    
    
    
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    
    [dataServer appendData:data];
    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
    //[MBProgressHUD hideHUDForView:self.view animated:YES];
    NSMutableDictionary *response=[[NSMutableDictionary alloc]initWithDictionary:[NSJSONSerialization JSONObjectWithData:dataServer options:0 error:nil]];
    
    
    idarray=[response valueForKey:@"data"];
    
    
    for (int i=0; i<=idarray.count-1; i++) {
        
        CSUserModel *chatlist=[[CSUserModel alloc]init];
        
        
        chatlist.userID=[[idarray objectAtIndex:i]valueForKey:@"user_id"];
        chatlist.name=[[idarray objectAtIndex:i]valueForKey:@"firstName"];
        chatlist.created=[[idarray objectAtIndex:i]valueForKey:@"registered_date"] ;

        [arrUserlist addObject:chatlist];
        
        SQLite *sqlObj=[[SQLite alloc] initWithSQLFile:@"SpikaChatDB.sqlite"];
        
        [sqlObj openDb];
        
        
        
        NSString *query = [NSString stringWithFormat:@"insert into tblusers (userID,username,created) values ('%@','%@','%@')",chatlist.userID,chatlist.name,chatlist.created];
        
        
        
        [sqlObj insertDb:query];
        
        
        [sqlObj closeDb];
        

//        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [hud1 hideAnimated:YES];
        
        [_tblUsers reloadData];
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:arrUserlist];
    [defaults setObject:data forKey:@"totalusers"];
    [defaults synchronize];
}


-(void)clearusers{

    SQLite *sqlObj=[[SQLite alloc] initWithSQLFile:@"SpikaChatDB.sqlite"];
    
    [sqlObj openDb];
    
    
    
    NSString *query = [NSString stringWithFormat:@"delete from tblusers"];
    
    
    
    [sqlObj updateDb:query];
    
    
    [sqlObj closeDb];



}

-(void)getOfflineusers{


    SQLite *sqlObj=[[SQLite alloc] initWithSQLFile:@"SpikaChatDB.sqlite"];
    
    [sqlObj openDb];
    
    
    NSString *query = [NSString stringWithFormat:@"SELECT  userID,username,created from tblusers"];
    
    
    
    [sqlObj readDb:query];
    
    
    
    
    while ([sqlObj hasNextRow])
    {
        CSUserModel *chatlist=[[CSUserModel alloc]init];
        
        
        chatlist.userID=[sqlObj getColumn:0 type:@"text"];
        chatlist.name=[sqlObj getColumn:1 type:@"text"];
        chatlist.registereddate=[sqlObj getColumn:2 type:@"text"];
        
         [arrUserlist addObject:chatlist];



    }

    [sqlObj closeDb];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:arrUserlist];
    [defaults setObject:data forKey:@"totalusers"];
    [defaults synchronize];


}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return 80;

}

-(void)hideHud {
    
    [hud hideAnimated:YES];
}

-(void)pushViewController:(UIViewController *)controller {
    
    [self.navigationController pushViewController:controller animated:YES];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
