//
//  Selectusercell.m
//  Spika
//
//  Created by HelixTech-Admin  on 08/08/2017.
//  Copyright © 2017 Clover Studio. All rights reserved.
//

#import "Selectusercell.h"

@implementation Selectusercell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
