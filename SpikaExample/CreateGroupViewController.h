//
//  CreateGroupViewController.h
//  Spika
//
//  Created by HelixTech-Admin  on 25/07/2017.
//  Copyright © 2017 Clover Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSUserModel.h"
#import "SelectUserViewController.h"
#import "CSChatViewController.h"
#import "SQLite.h"
#import "MBProgressHUD.h"
#import "Reachability.h"

@interface CreateGroupViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,NSURLConnectionDataDelegate,UIAlertViewDelegate,UITextFieldDelegate>{
    
    NSMutableArray *arrUserSelect;
    NSMutableArray *arrSelectedUser;
    NSMutableArray *arrSelectedUserNew;

    NSMutableData *dataServer;
    NSMutableArray *idarray;
    NSMutableArray *arrotherusers;
    NSString *revroomid;
    NSMutableData *dataone;
    
    
}

@property(nonatomic,strong) IBOutlet UITableView *tblAddUser;
@property(nonatomic,strong) NSString *strid;
@property(nonatomic,strong) NSString *strusename;
@property(nonatomic,strong) IBOutlet UITextField *txtgroupname;
@property(nonatomic,strong) IBOutlet UIButton *btncreate;
@property(nonatomic,strong) CSUserModel *loggedinuser;
@end
