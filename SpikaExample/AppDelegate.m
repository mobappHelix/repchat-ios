//
//  AppDelegate.m
//  Spika
//
//  Created by mislav on 08/12/15.
//  Copyright © 2015 Clover Studio. All rights reserved.
//

#import "AppDelegate.h"
#import "CSChatViewController.h"
#import "CSConfig.h"
#import "CSAvatarLoader.h"
#import "Util.h"
#import "SQLite.h"

#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
@import UserNotifications;
#endif

// Implement UNUserNotificationCenterDelegate to receive display notification via APNS for devices
// running iOS 10 and above.
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
@interface AppDelegate () <UNUserNotificationCenterDelegate>
@end
#endif

// Copied from Apple's header in case it is missing in some cases (e.g. pre-Xcode 8 builds).
#ifndef NSFoundationVersionNumber_iOS_9_x_Max
#define NSFoundationVersionNumber_iOS_9_x_Max 1299
#endif

@implementation AppDelegate

NSString *const kGCMMessageIDKey = @"gcm.message_id";

- (BOOL)application:(UIApplication *)application
didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
 
    // [START configure_firebase]
    [FIRApp configure];
    // [END configure_firebase]
    
    // [START set_messaging_delegate]
    [FIRMessaging messaging].delegate = self;
    // [END set_messaging_delegate]
    
    // Register for remote notifications. This shows a permission dialog on first run, to
    // show the dialog at a more appropriate time move this registration accordingly.
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_7_1) {
        // iOS 7.1 or earlier. Disable the deprecation warnings.
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        UIRemoteNotificationType allNotificationTypes =
        (UIRemoteNotificationTypeSound |
         UIRemoteNotificationTypeAlert |
         UIRemoteNotificationTypeBadge);
        [application registerForRemoteNotificationTypes:allNotificationTypes];
#pragma clang diagnostic pop
    } else {
        // iOS 8 or later
        // [START register_for_notifications]
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
            UIUserNotificationType allNotificationTypes =
            (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
            UIUserNotificationSettings *settings =
            [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
            [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        } else {
            // iOS 10 or later
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
            // For iOS 10 display notification (sent via APNS)
            [UNUserNotificationCenter currentNotificationCenter].delegate = self;
            UNAuthorizationOptions authOptions =
            UNAuthorizationOptionAlert
            | UNAuthorizationOptionSound
            | UNAuthorizationOptionBadge;
            [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions completionHandler:^(BOOL granted, NSError * _Nullable error) {
            }];
#endif
        }
        
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        // [END register_for_notifications]
    }
    
//    //     check for internet connection
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(checkNetworkStatus:)
//                                                 name:kReachabilityChangedNotification object:nil];
//    
//    // Set up Reachability
//    internetReachable = [Reachability reachabilityForInternetConnection];
//    [internetReachable startNotifier];
    

        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"splashFlag"];


    NSString* filepath =  [[NSUserDefaults standardUserDefaults] objectForKey:@"filepath"];
    NSLog(@"Filepath:%@",filepath);
    
    if ( ( ![filepath isEqual:[NSNull null]] ) && ( [filepath length] != 0 ) ) {
        
        NSLog(@"NOT NULL");
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *cacheDirectory = [paths objectAtIndex:0];
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        NSError *error;
        BOOL success1 = [fileManager removeItemAtPath:[cacheDirectory stringByAppendingPathComponent:filepath] error:&error];
        if (success1) {
            NSLog(@"Deleted");
            //                            UIAlertView *removedSuccessFullyAlert = [[UIAlertView alloc] initWithTitle:@"Congratulations:" message:@"Successfully removed" delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
            //                            [removedSuccessFullyAlert show];
            
               NSLog(@"Filepath:%@",[cacheDirectory stringByAppendingPathComponent:filepath]);
            
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"filepath"];

        }
        else
        {
            NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
        }
        
        
    } else {
        NSLog(@"NULL");
    }
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    
    NSLog(@"applicationWillResignActive");
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    
////     check for internet connection
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(checkNetworkStatus:)
//                                                 name:kReachabilityChangedNotification object:nil];
//    
//    // Set up Reachability
//    internetReachable = [Reachability reachabilityForInternetConnection];
//    [internetReachable startNotifier];
    
    
//    UIViewController *vc = [self visibleViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
//    
//    if ([vc isKindOfClass:[UsersViewController class]]) {
//        NSLog(@"UsersViewController");
//    } else if ([vc isKindOfClass:[SelectUserViewController class]]) {
//        NSLog(@"SelectUserViewController");
//    }
//
//    
//    if (vc == nil)
//    {
//        
//        NSLog(@"Root");
//    }
//    if ([vc isKindOfClass:[UINavigationController class]])
//    {
//        UINavigationController *navigationController = (UINavigationController *)[UIApplication sharedApplication].keyWindow.rootViewController.presentedViewController;
//        
//        NSLog(@"Any Other");
//        
//        //        UIViewController *lastViewController = [[navigationController viewControllers] lastObject];
//        //
//        //        return [self visibleViewController:lastViewController];
//    }
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    
    NSLog(@"applicationDidEnterBackground");

    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    
    
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    
//    [internetReachable stopNotifier];

}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    NSLog(@"applicationWillTerminate");

//    [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"splashFlag"];


    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}




// [START receive_message]
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    // TODO: Handle data of notification
    
    // With swizzling disabled you must let Messaging know about the message, for Analytics
    // [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
    
    // Print message ID.
    if (userInfo[kGCMMessageIDKey]) {
        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }
    
    // Print full message.
    NSLog(@"%@", userInfo);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    // TODO: Handle data of notification
    
//     With swizzling disabled you must let Messaging know about the message, for Analytics
    //     [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
    
    
    // Print message ID.
    if (userInfo[kGCMMessageIDKey]) {
        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }
    
    // Print full message.
    NSLog(@"%@", userInfo);
    
    [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"splashFlag"];

    
    completionHandler(UIBackgroundFetchResultNewData);
    
    
    if(application.applicationState == UIApplicationStateActive) {
        
        //app is currently active, can update badges count here
        
    }else if(application.applicationState == UIApplicationStateBackground){
        
        //app is in background, if content-available key of your notification is set to 1, poll to your backend to retrieve data and update your interface here
        
    }else if(application.applicationState == UIApplicationStateInactive){
        
        //app is transitioning from background to foreground (user taps notification), do what you need when user taps here
        
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // Your code to run on the main queue/thread
            
            // Print full message.
            NSLog(@"%@", userInfo);
            
            
            [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"splashFlag"];

            
            
            AppDelegate * appDelegate = [[UIApplication sharedApplication] delegate];
            UIWindow *mainWindow = appDelegate.window;
            //        UsersViewController * uVc = [UsersViewController new];
            UIStoryboard * mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                     bundle: nil];
            
            UINavigationController *navigationObject = (UINavigationController *)[UIApplication sharedApplication].keyWindow.rootViewController;
            
            //        UINavigationController *navigationObject1 = [mainStoryboard instantiateViewControllerWithIdentifier:@"navigation"];
            
            //        [navigationObject initWithRootViewController:self.usersViewController]
            //
            //        navigationObject = [[UINavigationController alloc]initWithRootViewController:self.usersViewController];
            
            [navigationObject popToRootViewControllerAnimated:YES];
            
            //        [mainWindow setRootViewController:[navigationObject initWithRootViewController:self.usersViewController]];
            
            if ([[userInfo valueForKey:@"msg_type"] isEqualToString:@"user_msg"]) {
                
                
                //CSChatViewController
                NSDictionary *parameters = @{
                                             paramUserID :[NSString stringWithFormat:@"%@",[userInfo valueForKey:@"userId"]],
                                             paramName : [NSString stringWithFormat:@"%@",[userInfo valueForKey:@"receiver_name"]],
                                             paramAvatarURL :@"http://ossdemo.spika.chat/spika/img/avatar.jpg",
                                             paramRoomID:[NSString stringWithFormat:@"%@",[userInfo valueForKey:@"roomID"]]
                                             };
                CSChatViewController *csvc=[[CSChatViewController alloc]initWithParameters:parameters];
                csvc.roomID=[userInfo valueForKey:@"roomID"];
                csvc.zhenda=@"3";
                csvc.strtitlename=[userInfo valueForKey:@"username"];
                //    [self.navigationController pushViewController:csvc animated:YES];
                
                CSUserModel *model=[[CSUserModel alloc]init];
                model.userID = [userInfo valueForKey:@"userId"];
                model.name = [userInfo valueForKey:@"receiver_name"];
                
                
                
                //SelectUserViewController
                
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                SelectUserViewController *sfvc = [storyboard instantiateViewControllerWithIdentifier:@"selected"];
                sfvc.strroomid=model.userID;
                // [sfvc setModalPresentationStyle:UIModalPresentationFullScreen];
                //            [self.navigationController pushViewController:sfvc animated:YES];
                
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                NSData *data = [NSKeyedArchiver archivedDataWithRootObject:model];
                [defaults setObject:data forKey:@"Loggeduser"];
                [defaults synchronize];
                
//                [self.usersViewController pushViewController:sfvc];
                [self.loginViewController pushViewController:sfvc];

                
                
                
                
                //            [self.selectUserViewController pushViewController:csvc];
                
                
                
                
            } else {
                
                CSUserModel *model=[[CSUserModel alloc]init];
                model.userID = [userInfo valueForKey:@"userId"];
                model.name = [userInfo valueForKey:@"receiver_name"];
                
                
                
                //SelectUserViewController
                
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                SelectUserViewController *sfvc = [storyboard instantiateViewControllerWithIdentifier:@"selected"];
                sfvc.strroomid=model.userID;
                // [sfvc setModalPresentationStyle:UIModalPresentationFullScreen];
                //            [self.navigationController pushViewController:sfvc animated:YES];
                
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                NSData *data = [NSKeyedArchiver archivedDataWithRootObject:model];
                [defaults setObject:data forKey:@"Loggeduser"];
                [defaults synchronize];
                
//                [self.usersViewController pushViewController:sfvc];
                
                [self.loginViewController pushViewController:sfvc];

                
                NSLog(@"%@", userInfo);
                //
                //            NSDictionary *parameters = @{
                //             paramUserID :[NSString stringWithFormat:@"%@",[userInfo valueForKey:@"userId"]],
                //             paramName : [NSString stringWithFormat:@"%@",[userInfo valueForKey:@"receiver_name"]],
                //             paramAvatarURL :@"http://ossdemo.spika.chat/spika/img/avatar.jpg",
                //             paramRoomID:[NSString stringWithFormat:@"%@",[userInfo valueForKey:@"roomID"]]
                //             };
                //             NSLog(@"%@",parameters);
                //
                //
                //             CSGroupModel *grp = [[CSGroupModel alloc]init];
                //             grp.group_id = [[usersArray objectAtIndex:indexPath.row] valueForKey:@"group_id"];
                //             grp.group_name = [[usersArray objectAtIndex:indexPath.row] valueForKey:@"group_name"];
                //             grp.creator_id = [[usersArray objectAtIndex:indexPath.row] valueForKey:@"creator_id"];
                //             grp.room_id = [[usersArray objectAtIndex:indexPath.row] valueForKey:@"room_id"];
                //             grp.lastMessageTime = [[usersArray objectAtIndex:indexPath.row] valueForKey:@"last_message_time"];
                //             grp.lastMessage = [[usersArray objectAtIndex:indexPath.row] valueForKey:@"last_message"];
                //             
                //             
                //             
                //             
                //             NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                //             NSData *data = [NSKeyedArchiver archivedDataWithRootObject:grp];
                //             [defaults setObject:data forKey:@"selectedgroup"];
                //             [defaults synchronize];
                //             
                //             CSChatViewController *csvc=[[CSChatViewController alloc]initWithParameters:parameters];
                //             
                //             csvc.roomID=grproomid;
                //             
                //             [self.selectUserViewController pushViewController:csvc];
                
                
            }
            
            
            
            
            
            
            
            
        });
        
    }
    
    
    [self.selectUserViewController parse];
}
// [END receive_message]

// [START ios_10_message_handling]
// Receive displayed notifications for iOS 10 devices.
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
// Handle incoming notification messages while app is in the foreground.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
    NSDictionary *userInfo = notification.request.content.userInfo;
    
    
    // Print message ID.
    if (userInfo[kGCMMessageIDKey]) {
        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }
    
    // Print full message.
    NSLog(@"%@", userInfo);
    
    // Change this to your preferred presentation option
    completionHandler(UNNotificationPresentationOptionNone);
    
    [self.selectUserViewController parse];

}

// Handle notification messages after display notification is tapped by the user.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void(^)(void))completionHandler {
    NSDictionary *userInfo = response.notification.request.content.userInfo;
    if (userInfo[kGCMMessageIDKey]) {
        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }
    NSLog(@"%@", userInfo);
    
    [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"splashFlag"];

    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        // Your code to run on the main queue/thread
        
        // Print full message.
        NSLog(@"%@", userInfo);
        
        
        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"splashFlag"];


        
        AppDelegate * appDelegate = [[UIApplication sharedApplication] delegate];
        UIWindow *mainWindow = appDelegate.window;
//        UsersViewController * uVc = [UsersViewController new];
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                 bundle: nil];
        
        UINavigationController *navigationObject = (UINavigationController *)[UIApplication sharedApplication].keyWindow.rootViewController;
        
//        UINavigationController *navigationObject1 = [mainStoryboard instantiateViewControllerWithIdentifier:@"navigation"];
        
//        [navigationObject initWithRootViewController:self.usersViewController]
//        
//        navigationObject = [[UINavigationController alloc]initWithRootViewController:self.usersViewController];
        
        [navigationObject popToRootViewControllerAnimated:YES];
    
//        [mainWindow setRootViewController:[navigationObject initWithRootViewController:self.usersViewController]];
        
        if ([[userInfo valueForKey:@"msg_type"] isEqualToString:@"user_msg"]) {
            
            
            //CSChatViewController
            NSDictionary *parameters = @{
                                         paramUserID :[NSString stringWithFormat:@"%@",[userInfo valueForKey:@"userId"]],
                                         paramName : [NSString stringWithFormat:@"%@",[userInfo valueForKey:@"receiver_name"]],
                                         paramAvatarURL :@"http://ossdemo.spika.chat/spika/img/avatar.jpg",
                                         paramRoomID:[NSString stringWithFormat:@"%@",[userInfo valueForKey:@"roomID"]]
                                         };
            CSChatViewController *csvc=[[CSChatViewController alloc]initWithParameters:parameters];
            csvc.roomID=[userInfo valueForKey:@"roomID"];
            csvc.zhenda=@"3";
            csvc.strtitlename=[userInfo valueForKey:@"username"];
            //    [self.navigationController pushViewController:csvc animated:YES];
            
            CSUserModel *model=[[CSUserModel alloc]init];
            model.userID = [userInfo valueForKey:@"userId"];
            model.name = [userInfo valueForKey:@"receiver_name"];

            
            
            //SelectUserViewController
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            SelectUserViewController *sfvc = [storyboard instantiateViewControllerWithIdentifier:@"selected"];
            sfvc.strroomid=model.userID;
            // [sfvc setModalPresentationStyle:UIModalPresentationFullScreen];
//            [self.navigationController pushViewController:sfvc animated:YES];
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:model];
            [defaults setObject:data forKey:@"Loggeduser"];
            [defaults synchronize];
            
            [self.loginViewController pushViewController:sfvc];
            
            
            
            
//            [self.selectUserViewController pushViewController:csvc];

            
            
            
        } else {
            
            CSUserModel *model=[[CSUserModel alloc]init];
            model.userID = [userInfo valueForKey:@"userId"];
            model.name = [userInfo valueForKey:@"receiver_name"];
            
            
            
            //SelectUserViewController
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            SelectUserViewController *sfvc = [storyboard instantiateViewControllerWithIdentifier:@"selected"];
            sfvc.strroomid=model.userID;
            // [sfvc setModalPresentationStyle:UIModalPresentationFullScreen];
            //            [self.navigationController pushViewController:sfvc animated:YES];
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:model];
            [defaults setObject:data forKey:@"Loggeduser"];
            [defaults synchronize];
            
            [self.loginViewController pushViewController:sfvc];
            
            NSLog(@"%@", userInfo);
//            
//            NSDictionary *parameters = @{
//             paramUserID :[NSString stringWithFormat:@"%@",[userInfo valueForKey:@"userId"]],
//             paramName : [NSString stringWithFormat:@"%@",[userInfo valueForKey:@"receiver_name"]],
//             paramAvatarURL :@"http://ossdemo.spika.chat/spika/img/avatar.jpg",
//             paramRoomID:[NSString stringWithFormat:@"%@",[userInfo valueForKey:@"roomID"]]
//             };
//             NSLog(@"%@",parameters);
//
//             
//             CSGroupModel *grp = [[CSGroupModel alloc]init];
//             grp.group_id = [[usersArray objectAtIndex:indexPath.row] valueForKey:@"group_id"];
//             grp.group_name = [[usersArray objectAtIndex:indexPath.row] valueForKey:@"group_name"];
//             grp.creator_id = [[usersArray objectAtIndex:indexPath.row] valueForKey:@"creator_id"];
//             grp.room_id = [[usersArray objectAtIndex:indexPath.row] valueForKey:@"room_id"];
//             grp.lastMessageTime = [[usersArray objectAtIndex:indexPath.row] valueForKey:@"last_message_time"];
//             grp.lastMessage = [[usersArray objectAtIndex:indexPath.row] valueForKey:@"last_message"];
//             
//             
//             
//             
//             NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//             NSData *data = [NSKeyedArchiver archivedDataWithRootObject:grp];
//             [defaults setObject:data forKey:@"selectedgroup"];
//             [defaults synchronize];
//             
//             CSChatViewController *csvc=[[CSChatViewController alloc]initWithParameters:parameters];
//             
//             csvc.roomID=grproomid;
//             
//             [self.selectUserViewController pushViewController:csvc];
            
            
        }
        
        
        
        
 
        

        
    });
    
    completionHandler();
    
    [self.selectUserViewController parse];
    
    
    }
#endif
// [END ios_10_message_handling]

// [START refresh_token]
- (void)messaging:(nonnull FIRMessaging *)messaging didRefreshRegistrationToken:(nonnull NSString *)fcmToken {
    // Note that this callback will be fired everytime a new token is generated, including the first
    // time. So if you need to retrieve the token as soon as it is available this is where that
    // should be done.
    NSLog(@"FCM registration token: %@", fcmToken);
    

    [[NSUserDefaults standardUserDefaults] setObject:fcmToken forKey:@"fcmToken"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self.loginViewController hideHud];

    
    // TODO: If necessary send token to application server.
}
// [END refresh_token]

// [START ios_10_data_message]
// Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
// To enable direct data messages, you can set [Messaging messaging].shouldEstablishDirectChannel to YES.
- (void)messaging:(FIRMessaging *)messaging didReceiveMessage:(FIRMessagingRemoteMessage *)remoteMessage {
    NSLog(@"Received data message: %@", remoteMessage.appData);
}
// [END ios_10_data_message]

//- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
//    NSLog(@"Unable to register for remote notifications: %@", error);
//}

// This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
// If swizzling is disabled then this function must be implemented so that the APNs device token can be paired to
// the FCM registration token.
//- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
//    NSLog(@"APNs device token retrieved: %@", deviceToken);
//    
//    // With swizzling disabled you must set the APNs device token here.
//    // [FIRMessaging messaging].APNSToken = deviceToken;
//}
//@end


//- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings // NS_AVAILABLE_IOS(8_0);
//{
//    [application registerForRemoteNotifications];
//}

-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    
    NSLog(@"deviceToken: %@", deviceToken);
    NSString * token = [NSString stringWithFormat:@"%@", deviceToken];
    //Format token as you need:
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@">" withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@"<" withString:@""];
    
    NSLog(@"token:%@",token);
    
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"Error : %@",error.localizedDescription);
    
}
//- (void)createEditableCopyOfDatabaseIfNeeded {
//    
//    // First, test for existence.
//    
//    BOOL success, success2;
//    
//    
//    NSFileManager *fileManager = [NSFileManager defaultManager];
//    
//    NSError *error, *error2;
//    
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    
//    NSString *documentsDirectory = [paths objectAtIndex:0];
//    
//    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:@"SpikaChatDB.sqlite"];
//    
//    NSString *writableDBPath2 = [documentsDirectory stringByAppendingPathComponent:@"oncushtml.zip"];
//    
//    success = [fileManager fileExistsAtPath:writableDBPath];
//    success2 = [fileManager fileExistsAtPath:writableDBPath2];
//    NSLog(@"Writable path%@",writableDBPath);
//    NSLog(@"Writable path%@",writableDBPath2);
//    
//    NSString  *appVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
//    
//    if (success)
//    {
//        if(![appVersion isEqualToString:@"1.0"])
//        {
//            [fileManager removeItemAtPath:writableDBPath error:&error];
//        }
//    }
//    else
//    {
//        NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"SpikaChatDB.sqlite"];
//        
//        NSLog(@"Default DB : %@",defaultDBPath);
//        
//        success = [fileManager copyItemAtPath:defaultDBPath toPath:writableDBPath error:&error];
//        
//        
//        if (!success) {
//            
//            NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
//            
//        }
//        else
//        {
//            NSLog(@"DB copy success");
//        }
//        
//        
//        
//    }
//    
//}

- (void)createEditableCopyOfDatabaseIfNeeded
{
    // First, test for existence.
    BOOL success;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:@"SpikaChatDB.sqlite"];
    success = [fileManager fileExistsAtPath:writableDBPath];
    if (success) return;
    
    // The writable database does not exist, so copy the default to the appropriate location.
    
    NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"SpikaChatDB.sqlite"];
    success = [fileManager copyItemAtPath:defaultDBPath toPath:writableDBPath error:&error];
    if (!success) {
        NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
    }
    
    
}

- (void)popToRoot
{
    AppDelegate * appDelegate = [[UIApplication sharedApplication] delegate];
    UIWindow *mainWindow = appDelegate.window;
    UsersViewController * uVc = [UsersViewController new];
    UINavigationController *navigationObject = [[UINavigationController alloc] initWithRootViewController:uVc];
    [mainWindow setRootViewController:navigationObject];
}

- (UIViewController *)visibleViewController:(UIViewController *)rootViewController
{
    if (rootViewController.presentedViewController == nil)
    {
        return rootViewController;
    }
    if ([rootViewController.presentedViewController isKindOfClass:[UINavigationController class]])
    {
        UINavigationController *navigationController = (UINavigationController *)rootViewController.presentedViewController;
        UIViewController *lastViewController = [[navigationController viewControllers] lastObject];
        
        return [self visibleViewController:lastViewController];
    }
    if ([rootViewController.presentedViewController isKindOfClass:[UITabBarController class]])
    {
        UITabBarController *tabBarController = (UITabBarController *)rootViewController.presentedViewController;
        UIViewController *selectedViewController = tabBarController.selectedViewController;
        
        return [self visibleViewController:selectedViewController];
    }
    
    UIViewController *presentedViewController = (UIViewController *)rootViewController.presentedViewController;
    
    return [self visibleViewController:presentedViewController];
}

@end
