//
//  ChatViewController.m
//  Prototype
//
//  Created by Ivo Peric on 28/08/15.
//  Copyright (c) 2015 Clover Studio. All rights reserved.
//

#import "CSChatViewController.h"
#import "CSModel.h"
#import "CSUserModel.h"
#import "CSMessageModel.h"
#import "CSResponseModel.h"
#import "UIActivityIndicatorView+AFNetworking.h"
#import "CSLoginResult.h"
#import "CSGetMessages.h"
#import "CSConfig.h"
#import "CSTypingModel.h"
#import "CSMyTextMessageTableViewCell.h"
#import "CSUserInfoTableViewCell.h"
#import "CSYourTextMessageTableViewCell.h"
#import "CSUsersViewController.h"
#import "CSYourImageMessageTableViewCell.h"
#import "CSMyImageMessageTableViewCell.h"
#import "CSYourStickerMessageCell.h"
#import "CSMyStickerMessageCell.h"
#import "CSYourMediaMessageTableViewCell.h"
#import "CSMyMediaMessageTableViewCell.h"
#import "CSMessageInfoViewController.h"
#import "CSImagePreviewView.h"
#import "CSVideoPreviewView.h"
#import "CSAudioPreviewView.h"
#import "CSDownloadManager.h"
#import "CSUploadImagePreviewViewController.h"
#import "CSEmitJsonCreator.h"
#import "CSUploadVideoPreviewViewController.h"
#import "CSRecordAudioViewController.h"
#import "CSMapViewController.h"
#import "CSUploadManager.h"
#import "CSSocketController.h"
#import "CSCustomConfig.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import "UIAlertView+Blocks.h"
#import "CSTitleView.h"
#import "CSNotificationOverlayView.h"
#import "CSChatErrorCodes.h"
#import "CSStickerListModel.h"
#import "CSSeenByModel.h"

@interface CSChatViewController () <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, CSChatSettingsViewDelegate, CSCellClickedDelegate, UIDocumentInteractionControllerDelegate, CSMenuViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CSChatSocketControllerDelegate, ABPeoplePickerNavigationControllerDelegate, CSStickerDelegate>{

    NSString *strid;
    NSString *struserid;
    NSString *strroomID;
    NSString *struser_name;
    NSNumber *strtype;
    NSString *strroomid;
    NSString *strcreated;
    NSString *strfile;
    NSString *strlocalid;
    NSString *strlat;
    NSString *strlon;
    NSMutableArray  *strseenBy;
    NSString *strstatus;
    NSString *strattributes;
    NSString *stravatarURL;
    NSString *strToken;
    NSString *message;
    BOOL isoffline;
    NSNumber *deleted;
    NSString *user_id;
    NSString *user_created;
    NSString *user_userID;
    NSString *file_id;
    NSString *mimetype;
    NSString *file_name;
    NSNumber *file_size;
    NSString *thumb_id;
    NSString *thumb_name;
    NSNumber *thumb_size;
    NSString *strsub;
    NSString *tile;
    

}

@property (nonatomic, strong) NSDictionary *parameters;
@property (nonatomic) BOOL isLoading;
@property (nonatomic, strong) CSTitleView *titleView;
//@property (nonatomic, strong) UIActivityIndicatorView *indicator;

@property (nonatomic, strong) CSStickerListModel *stickerList;

@end

@implementation CSChatViewController


-(instancetype)initWithParameters:(NSDictionary *)parameters {
    if (self = [super init]) {
        self.parameters = parameters;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    EditinfoViewController *edit=[[EditinfoViewController alloc]init];
    //edit.delegate=self;
    _arrgroupusers=[[NSMutableArray alloc]init];
    
    tile=_strtitlename;
    
    zhenndareceive=_zhenda;
    
//    self.navigationController.navigationBar.translucent = NO;
//    self.navigationController.navigationBar.barTintColor = kAppDefaultColor(1);
    self.navigationController.navigationBar.tintColor = kAppBubbleRightColor;
//    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSData *data = [defaults objectForKey:@"totalusers"];
//    totalusers = [NSKeyedUnarchiver unarchiveObjectWithData:data];
//    
    NSData *userdata = [defaults objectForKey:@"Loggeduser"];
    _loggedinuser = [NSKeyedUnarchiver unarchiveObjectWithData:userdata];
    
    if ([zhenndareceive isEqual:@"1"]) {
        
        NSData *data2 = [defaults objectForKey:@"createdgroup"];
        self.groupreceived=[NSKeyedUnarchiver unarchiveObjectWithData:data2];
        
    }
    else{
        NSData *data3 = [defaults objectForKey:@"selectedgroup"];
        self.groupreceived=[NSKeyedUnarchiver unarchiveObjectWithData:data3];
    }
    [defaults synchronize];
    
 
    
    self.tableView.estimatedRowHeight = 44;
    [self.tableView setTransform:CGAffineTransformMakeRotation(-M_PI)];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([CSMyTextMessageTableViewCell class]) bundle:nil]
         forCellReuseIdentifier:kAppMyTextMessageCell];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([CSUserInfoTableViewCell class]) bundle:nil]
         forCellReuseIdentifier:kAppUserInfoMessageCell];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([CSYourTextMessageTableViewCell class]) bundle:nil]
         forCellReuseIdentifier:kAppYourTextMessageCell];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([CSYourImageMessageTableViewCell class]) bundle:nil]
         forCellReuseIdentifier:kAppYourImageMessageCell];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([CSMyImageMessageTableViewCell class]) bundle:nil]
         forCellReuseIdentifier:kAppMyImageMessageCell];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([CSYourMediaMessageTableViewCell class]) bundle:nil]
         forCellReuseIdentifier:kAppYourMediaMessageCell];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([CSMyMediaMessageTableViewCell class]) bundle:nil]
         forCellReuseIdentifier:kAppMyMediaMessageCell];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([CSYourStickerMessageCell class]) bundle:nil]
         forCellReuseIdentifier:kAppYourStickerMessageCell];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([CSMyStickerMessageCell class]) bundle:nil]
         forCellReuseIdentifier:kAppMyStickerMessageCell];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(deleteMessageInChat:)
                                                 name:kAppDeleteMessageNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(sendFileMessage:)
                                                 name:kAppFileUploadedNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(sendLocationMessage:)
                                                 name:kAppLocationSelectedNotification
                                               object:nil];
    
//    self.indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
//    [self.indicator setFrame:CGRectMake(0, 0, 44, 44)];
//    [self.indicator setHidesWhenStopped:YES];
//    self.tableView.tableFooterView = self.indicator;

    
    self.tableView.backgroundColor = [UIColor whiteColor];
    
    self.messages = [[NSMutableArray alloc]init];
    self.tempSentMessagesLocalId = [[NSMutableArray alloc] init];
    self.typingUsers = [[NSMutableArray alloc] init];
    self.lastDataLoadedFromNet = [[NSMutableArray alloc] init];
    
    self.stickerList = [CSStickerListModel new];
    isoffline=NO;
    
    NSError *error;
    self.activeUser = [[CSUserModel alloc] initWithDictionary:self.parameters error:&error];
    
    [self.loggedinuser setRoomID:self.activeUser.roomID];
    
    _seeby=[[NSMutableArray alloc]init];
    strseenBy=[[NSMutableArray alloc]init];
    _etMessage.delegate=self;
    
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus==NotReachable)
    {
        
        isoffline=YES;
        online=@"NO";
       
        [self getOfflineMessages];
            
    }
    else
    {
        isoffline=NO;
        online=@"YES";
        
        
//    [self.titleView setSubtitle:@"Online"];

//    [self login:self.parameters];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self login:self.parameters];
            
        });
        
    
    }
    
//    dispatch_async(dispatch_get_main_queue(), ^{
//              [self isonlinetitle];
//        
//    });
    
//
    
}



-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    


    
    //[self isonlinetitle];
    if ([zhenndareceive isEqual:@"3"]) {
    
        NSLog(@"its user");
        
        self.titleView = [CSTitleView new];
        [self.titleView setTitle:tile];
        [self.titleView setSubtitle:@""];
         self.navigationItem.titleView = self.titleView;
        
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus networkStatus = [reachability currentReachabilityStatus];
        if(networkStatus!=NotReachable)
        {
            
            isoffline=NO;
            online=@"YES";
            
//                        [self.titleView setSubtitle:@"Online"];

        }
        else
        {
            isoffline=YES;
            online=@"NO";
            [MBProgressHUD hideHUDForView:self.view animated:YES];

            
        }
    
    }
    
    else if ([zhenndareceive isEqual:@"4"]){
    
        self.titleView = [CSTitleView new];
        [self.titleView setTitle:@"Room chat"];
        [self.titleView setSubtitle:@""];
        self.navigationItem.titleView = self.titleView;
    
   
    }
    else{
        
                    [self.titleView setSubtitle:@""];

        
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus networkStatus = [reachability currentReachabilityStatus];
        if(networkStatus!=NotReachable)
        {
//            isoffline=NO;
//            online=@"YES";
            
//            [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"groupEdit"];
            
            
   
            NSString * groupEdit = [[NSUserDefaults standardUserDefaults] objectForKey:@"groupEdit"];

            if ([groupEdit isEqualToString:@"1"]) {
                
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                            [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"groupEdit"];

            }
            

            
        } else {
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];

        }
        
        self.titleView = [CSTitleView new];
        [self.titleView setTitle:_groupreceived.group_name];
        [self.titleView setSubtitle:@""];

        self.navigationItem.titleView = self.titleView;
        
        UIImage *image1 = [[UIImage imageNamed:@"ic_grp_info"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        UIBarButtonItem *item1 = [[UIBarButtonItem alloc] initWithImage:image1 style:UIBarButtonItemStylePlain target:self action:@selector(editclicked)];
        
    self.navigationItem.rightBarButtonItem = item1;
    }
    NSArray* array = kAppMenuSettingsArray;
    [self.chatsettingsView setItems:array];
    self.chatsettingsView.hidden = YES;
    self.chatsettingsView.delegate = self;
    
    
    NSString * attachment = [[NSUserDefaults standardUserDefaults] objectForKey:@"attachment"];
    
    if ([attachment isEqualToString:@"1"]) {
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"attachment"];
        
        
    }
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardIsMoving:)
                                                 name:UIKeyboardWillChangeFrameNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationDidBecomeActive:)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationDidEnterBackground:)
                                                 name:UIApplicationDidEnterBackgroundNotification
                                               object:nil];
    
    [self isonlinetitle];

}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillChangeFrameNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidEnterBackgroundNotification
                                                  object:nil];
}


-(void)editclicked{
  
    
    [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"groupEdit"];

    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    EditinfoViewController *sfvc = [storyboard instantiateViewControllerWithIdentifier:@"Editinfo"];
    sfvc.roomid=self.activeUser.roomID;
//    sfvc.roomid = _loggedinuser.roomID;
    sfvc.strgrpflag=zhenndareceive;
    
    //[sfvc setModalPresentationStyle:UIModalPresentationFormSheet];
   // [self presentViewController:sfvc animated:YES completion:nil];
    sfvc.grprecv=_grouptopasss;
    
    sfvc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    sfvc.modalPresentationStyle =  UIModalPresentationFormSheet;
    
    //CGPoint frameSize = CGPointMake([[UIScreen mainScreen] bounds].size.width*0.95f, [[UIScreen mainScreen] bounds].size.height*0.95f);
    
    //sfvc.preferredContentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height);
    sfvc.preferredContentSize= CGSizeMake([[UIScreen mainScreen] bounds].size.width*0.95f, [[UIScreen mainScreen] bounds].size.height*0.95f );
    //[self presentViewController:sfvc animated:YES completion:nil];
    [self.navigationController pushViewController:sfvc animated:YES];



}

-(void) updateLabelWithString:(NSString*)string {
   [self.titleView setTitle:self.activeUser.name];
}


-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

-(void) keyboardIsMoving: (NSNotification*) aNotification{
    
    NSDictionary* info = [aNotification userInfo];
    CGRect kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    if(kbSize.origin.y == kAppHeight){
        [UIView animateWithDuration:0.3 animations:^{
            self.bottomMargin.constant = 0;
        }];
    }else{
        [UIView animateWithDuration:0.3 animations:^{
            self.bottomMargin.constant = kbSize.size.height;
        }];
    }
}

-(void)applicationDidBecomeActive: (NSNotification*) notification {
    if (self.token.length) {
        [self connectToSocket];
        
        CSMessageModel* lastMessage = [self.messages firstObject];
        [self getLatestMessages:lastMessage._id];
    }
}

-(void)applicationDidEnterBackground: (NSNotification*) notification {
    [[CSSocketController sharedController] close];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - socket method
-(void) connectToSocket {
    
    
    NSDictionary *parametersLogin = [self.loggedinuser toDictionary];
    [[CSSocketController sharedController] registerForChat:parametersLogin withChatSocketDelegate:self];
    //[self isonlinetitle];
}

-(void) didReceiveNewMessage: (CSMessageModel *) message {
    
    if (self.tableView.contentOffset.y) {
        float verticalPosition = self.navigationController.navigationBar.frame.size.height
        + [UIApplication sharedApplication].statusBarFrame.size.height
        + self.footerView.frame.origin.y;
        
        [CSNotificationOverlayView notificationOverlayFromChatWithMessage:@"new message"
                                                         verticalPosition:verticalPosition
                                                                 callback:^(BOOL wasSelected) {
                                                                     if (wasSelected) {
                                                                         [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
                                                                     }
                                                                 }];
    }
    
    if ([self.tempSentMessagesLocalId containsObject:message.localID]){
        
        [self.tempSentMessagesLocalId removeObject:message.localID];
        
        for(CSMessageModel* item in self.messages){
            
            if(item.localID != nil && [item.localID isEqualToString:message.localID]){
                
                item.status = kAppMessageStatusDelivered;
                item._id = message._id;
                item.created = message.created;
                
                
                SQLite *sqlObj1=[[SQLite alloc] initWithSQLFile:@"SpikaChatDB.sqlite"];
                [sqlObj1 openDb];
                NSString *query1 = [NSString stringWithFormat:@"update tblmessage set id = '%@', created = '%@' , status = '%ld'  where localID = '%@'", item._id, item.created,(long)item.status,message.localID];
                [sqlObj1 updateDb:query1];
                [sqlObj1 closeDb];
                break;
            }
            
        }
        
        
        [self.tableView reloadData];
        [MBProgressHUD hideHUDForView:self.view animated:YES];

        
    }
    else {
        
        
        if (self.tableView.contentOffset.y) {
            [self.tableView beginUpdates];
            [self.messages insertObject:message atIndex:0];
            [self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
            [self.tableView endUpdates];
        }
        else {
            [self.messages insertObject:message atIndex:0];
            [self.tableView reloadData];
            [MBProgressHUD hideHUDForView:self.view animated:YES];

        }
        
        if(![message.user.userID isEqualToString:self.activeUser.userID]){
            NSMutableArray* openMessage = [NSMutableArray new];
            [openMessage addObject:message._id];
            [self sendOpenMessages:openMessage];
        }
    
    }
}

-(void) didReceiveTyping: (CSTypingModel*) typing{
    
    if([typing.user.userID isEqualToString:self.activeUser.userID]){
        return;
    }
    
    if([typing.type intValue] == kAppTypingStatusOFF){
        [self isUserAlreadyTyping:typing.user];
        [self generateTypingLabel];
    }else{
        [self.typingUsers addObject:typing.user];
        [self generateTypingLabel];
    }
    
}

-(void) didUserLeft: (CSUserModel*) user{
    
    [self isUserAlreadyTyping:user];
    [self generateTypingLabel];
    
}

-(void) didSentMessage: (CSMessageModel*) sentMessage{
    
    [self.tableView beginUpdates];
    [self.messages insertObject:sentMessage atIndex:0];
    [self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
    [self.tableView endUpdates];
    
    SQLite *sqlObj=[[SQLite alloc] initWithSQLFile:@"SpikaChatDB.sqlite"];
    
    [sqlObj openDb];
    
    
    
    NSString *query = [NSString stringWithFormat:@"insert into tblmessage (id,userID,roomID,user_name,type,message,created,localID,seenBy,status,attributes,avatarURL,pushToken,deleted,user_userID,file_id,mimetype,file_name,file_size,thumb_id,thumb_name,thumb_size,latitude,longitude,logedInUser,logedInUserName) values ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%d','%@','%@','%@','%@','%@','%@','%@','%@','%f','%f','%@','%@')",sentMessage._id,sentMessage.userID,sentMessage.roomID,sentMessage.user.name,sentMessage.type,sentMessage.message,sentMessage.created,sentMessage.localID,sentMessage.seenBy,sentMessage.deleted,@"",sentMessage.user.avatarURL,sentMessage.user.pushToken,0,sentMessage.user.userID,sentMessage.file.file.id,sentMessage.file.file.mimeType,sentMessage.file.file.name,sentMessage.file.file.size,sentMessage.file.thumb.id,sentMessage.file.thumb.name,sentMessage.file.thumb.size,sentMessage.location.lat,sentMessage.location.lng,_loggedinuser.userID,_loggedinuser.name];
    
    
    
    [sqlObj insertDb:query];
    
    
    [sqlObj closeDb];
    
    
}

-(void) didMessageUpdated:(NSArray*) updatedMessages{

    for(CSMessageModel *item in self.messages){
        for(CSMessageModel *itemNew in updatedMessages){
            if([item._id isEqualToString:itemNew._id]){
                [item updateMessageWithData:itemNew];
                continue;
            }
        }
    }
    [self.tableView reloadData];
    [MBProgressHUD hideHUDForView:self.view animated:YES];


}

- (IBAction)sendMessage:(id)sender {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus==NotReachable)
    {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"No internet connection"
                                                            message:nil
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles: nil];
      
        
        [alertView show];

        
        
    }
    else
    {

    
    if(!self.isTyping){
        [self openMenu];
        return;
    }
    
    NSString *message = [NSString alloc];
    message = self.etMessage.text;
    
        NSString *rawString = message;
        NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
        NSString *trimmed = [rawString stringByTrimmingCharactersInSet:whitespace];
        if ([trimmed length] == 0) {
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Empty string"
                                                                message:nil
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles: nil];
            
            
            [alertView show];
            
        }
        else{
            
            Reachability *reachability = [Reachability reachabilityForInternetConnection];
            NetworkStatus networkStatus = [reachability currentReachabilityStatus];
            if(networkStatus!=NotReachable)
            {
                CSMessageModel* messageModel = [CSMessageModel createMessageWithUser:self.loggedinuser andMessage:message andType:[NSNumber numberWithInt:kAppTextMessageType] andFile:nil andLocation:nil];
                [self.tempSentMessagesLocalId addObject:messageModel.localID];
                
                [self didSentMessage:messageModel];
                [self.etMessage setText:@""];
                [self changeEditing:self.etMessage];
                self.isTyping = NO;
                
                NSDictionary *parameters = [CSEmitJsonCreator createEmitSendMessage:messageModel andUser:self.loggedinuser andMessage:message andFile:nil andLocation:nil];
                
                NSArray *array = [NSArray arrayWithObject:parameters];
                [[CSSocketController sharedController] emit:kAppSocketSendMessage args:array];
            } else {
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"No internet connection"
                                                                    message:nil
                                                                   delegate:self
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles: nil];
                
                
                [alertView show];
            }
            
           

            
        
        }
        }
    }


-(void)getOfflineMessages{
    
    NSString *mesgid;
    NSMutableArray *tempmsg=[[NSMutableArray alloc]init];
    
    SQLite *sqlObj=[[SQLite alloc] initWithSQLFile:@"SpikaChatDB.sqlite"];
    
    [sqlObj openDb];
    
    
    NSString *query = [NSString stringWithFormat:@"SELECT  id,userID,roomID,user_name,type,message,created,localID,status,attributes,avatarURL,pushToken,deleted,user_userID,file_id,mimetype,file_name,file_size,thumb_id,thumb_name,thumb_size,latitude,longitude,seenby FROM tblmessage where roomID = '%@'",self.activeUser.roomID];
    

    
     [sqlObj readDb:query];
    
    
    
    
    while ([sqlObj hasNextRow])
    {
       
        

        CSUserModel *moduser=[[CSUserModel alloc]init];
        moduser.userID=[sqlObj getColumn:13 type:@"text"];
        moduser.roomID=[sqlObj getColumn:2 type:@"text"];
        moduser.avatarURL=[sqlObj getColumn:10 type:@"text"];
        moduser.name=[sqlObj getColumn:3 type:@"text"];
        moduser.pushToken=[sqlObj getColumn:11 type:@"text"];
        
        CSFileDetailsModel *filedetail=[[CSFileDetailsModel alloc]init];
        filedetail.id=[sqlObj getColumn:14 type:@"text"];
        filedetail.mimeType=[sqlObj getColumn:15 type:@"text"];
        filedetail.name=[sqlObj getColumn:16 type:@"text"];
        filedetail.size=[sqlObj getColumn:17 type:@"text"];
        
        CSFileDetailsModel *thumbdetail=[[CSFileDetailsModel alloc]init];
        thumbdetail.id=[sqlObj getColumn:18 type:@"text"];
        thumbdetail.mimeType=[sqlObj getColumn:15 type:@"text"];
        thumbdetail.name=[sqlObj getColumn:19 type:@"text"];
        thumbdetail.size=[sqlObj getColumn:20 type:@"text"];
        
 
        CSFileModel *msgfile=[[CSFileModel alloc]init];
        msgfile.file=filedetail;
        msgfile.thumb=thumbdetail;
        
        CSLocationModel *loc=[[CSLocationModel alloc]init];
        loc.lat=[[sqlObj getColumn:21 type:@"text"] doubleValue];
        loc.lng=[[sqlObj getColumn:22 type:@"text"] doubleValue];
        
        NSString *jsonString=[sqlObj getColumn:23 type:@"text"];
        
        NSData *data = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        NSArray *arrayObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
        
        
        
        CSMessageModel *messg=[[CSMessageModel alloc]init];
        
        messg._id =[sqlObj getColumn:0 type:@"text"];
        
        messg.userID =[sqlObj getColumn:1 type:@"text"];
        messg.roomID =[sqlObj getColumn:2 type:@"text"];
        messg.user =moduser;
        messg.type = [NSNumber numberWithInteger:[[sqlObj getColumn:4 type:@"text"] integerValue]];
        messg.message =[sqlObj getColumn:5 type:@"text"] ;
        messg.created =[sqlObj getColumn:6 type:@"text"];
        messg.file = msgfile;
        messg.localID =[sqlObj getColumn:7 type:@"text"];
        messg.location=loc;
        messg.status=[[sqlObj getColumn:8 type:@"text"] intValue];
        messg.deleted=[sqlObj getColumn:12 type:@"text"];
        messg.seenBy=arrayObject;
        
        
       
        [self.messages addObject:messg];
        
        [self sortMessages];
        
        NSArray* unSeenMessages = [CSUtils generateUnSeenMessageIdsFrom:_messages andActiveUser:self.loggedinuser];
        [self sendOpenMessages:unSeenMessages];
        
    }
    
 
    [sqlObj closeDb];
    
   
    
}


- (IBAction)onStickerButton:(id)sender {
    
    [self.etMessage resignFirstResponder];
    
    if(!self.stickerView){
        
        self.stickerView = [[CSStickerView alloc] init];
        self.stickerView.stickerList = self.stickerList;
        
        [self.stickerView initializeInView:self.view dismiss:^(void){
            [self.stickerView removeFromSuperview];
            self.stickerView = nil;
        }];
        
        self.stickerView.delegate = self;
    }
}

#pragma mark - Sticker Delegate

-(void)onSticker:(NSString *)stickerUrl {
    [self sendSticker:stickerUrl];
}

#pragma mark - send messages socket api

- (void)sendSticker:(NSString *)stickerUrl {
    CSMessageModel* messageModel = [CSMessageModel createMessageWithUser:self.loggedinuser andMessage:stickerUrl andType:[NSNumber numberWithInt:kAppStickerMessageType] andFile:nil andLocation:nil];
    [self.tempSentMessagesLocalId addObject:messageModel.localID];
    [self didSentMessage:messageModel];
    
    NSDictionary *parameters = [CSEmitJsonCreator createEmitSendMessage:messageModel andUser:self.loggedinuser andMessage:stickerUrl andFile:nil andLocation:nil];
    NSArray *array = [NSArray arrayWithObject:parameters];
    [[CSSocketController sharedController] emit:kAppSocketSendMessage args:array];
}

- (void)sendContact:(NSString *)vCard {
    CSMessageModel* messageModel = [CSMessageModel createMessageWithUser:self.loggedinuser andMessage:vCard andType:[NSNumber numberWithInt:kAppContactMessageType] andFile:nil andLocation:nil];
    [self.tempSentMessagesLocalId addObject:messageModel.localID];
    [self didSentMessage:messageModel];
    
    NSDictionary *parameters = [CSEmitJsonCreator createEmitSendMessage:messageModel andUser:self.loggedinuser andMessage:vCard andFile:nil andLocation:nil];
    NSArray *array = [NSArray arrayWithObject:parameters];
    [[CSSocketController sharedController] emit:kAppSocketSendMessage args:array];
}

-(void)sendFileMessage:(NSNotification*) notificationData{
    
    [self.menuView animateHide];
    
    NSDictionary *dict = notificationData.userInfo;
    
    NSError* error;
    
    NSDictionary* response = [dict objectForKey:paramResponseObject];
    CSResponseModel *responseModel = [[CSResponseModel alloc] initWithDictionary:response error:&error];
    CSFileModel* resultModel = [[CSFileModel alloc] initWithDictionary:responseModel.data error:&error];
    
    CSMessageModel* messageModel = [CSMessageModel createMessageWithUser:self.loggedinuser andMessage:@"" andType:[NSNumber numberWithInt:kAppFileMessageType] andFile:resultModel andLocation:nil];
    [self.tempSentMessagesLocalId addObject:messageModel.localID];
    
    [self didSentMessage:messageModel];
    
    NSDictionary *parameters = [CSEmitJsonCreator createEmitSendMessage:messageModel andUser:self.loggedinuser andMessage:@"" andFile:resultModel andLocation:nil];
    
    NSArray *array = [NSArray arrayWithObject:parameters];
    [[CSSocketController sharedController] emit:kAppSocketSendMessage args:array];
    
}

-(void)sendLocationMessage:(NSNotification*) notificationData{
    
    [self.menuView animateHide];
    
    NSDictionary *dict = notificationData.userInfo;
    
    CSLocationModel* response = [dict objectForKey:paramLocation];
    NSString* address = [dict objectForKey:paramAddress];
    
    CSMessageModel* messageModel = [CSMessageModel createMessageWithUser:self.loggedinuser andMessage:address andType:[NSNumber numberWithInt:kAppLocationMessageType] andFile:nil andLocation:response];
    [self.tempSentMessagesLocalId addObject:messageModel.localID];
    
    [self didSentMessage:messageModel];
    
    NSDictionary *parameters = [CSEmitJsonCreator createEmitSendMessage:messageModel andUser:self.loggedinuser andMessage:address andFile:nil andLocation:response];
    
    NSArray *array = [NSArray arrayWithObject:parameters];
    [[CSSocketController sharedController] emit:kAppSocketSendMessage args:array];
    
   
}




-(void)deleteMessageInChat:(NSNotification*)notificationData{
    
    NSDictionary *dict = notificationData.userInfo;
    CSMessageModel* messageToDelete = [dict objectForKey:paramMessage];

    if(messageToDelete._id != nil){
        
        NSDictionary *parameters = @{paramUserID : self.activeUser.userID,
                                     paramMessageID : messageToDelete._id};
        
        NSArray *array = [NSArray arrayWithObject:parameters];
        [[CSSocketController sharedController] emit:kAppSocketDeleteMessage args:array];
        
        SQLite *sqlObj=[[SQLite alloc] initWithSQLFile:@"SpikaChatDB.sqlite"];
        
        [sqlObj openDb];
        
        
        NSString *query = [NSString stringWithFormat:@"DELETE FROM tblmessage where id = '%@'",messageToDelete._id];
        
        
        [sqlObj updateDb:query];
        
        
        [sqlObj closeDb];
        
 
    }else{
        
        [self.tableView reloadData];

    }
    
   
}

-(void) sendTypingWithType:(NSString*) type{
    
    NSDictionary *parameters = @{paramRoomID : self.activeUser.roomID,
                                 paramUserID : self.activeUser.userID,
                                 paramType : type};
    
    NSArray *array = [NSArray arrayWithObject:parameters];
    [[CSSocketController sharedController] emit:kAppSocketSendTyping args:array];
}

-(void) sendOpenMessages: (NSArray*) messageIds{
    NSDictionary *parameters = @{paramMessageIDs : messageIds,
                                 paramUserID : self.activeUser.userID};
    
    NSArray *array = [NSArray arrayWithObject:parameters];
    [[CSSocketController sharedController] emit:kAppSocketOpenMessage args:array];
}

#pragma mark - api data
-(void) login: (NSDictionary*) parameters{

    NSString* urlLogin = [NSString stringWithFormat:@"%@%@", [CSCustomConfig sharedInstance].server_url, kAppLogin];
    
    [[CSApiManager sharedManager] apiPOSTCallWithUrl:urlLogin
                                          parameters:parameters
                                         indicatorVC:nil
                                     toShowIndicator:NO
                                     toHideIndicator:YES
                                        completition:^(CSResponseModel *responseModel) {
    
                                            CSLoginResult *login = [[CSLoginResult alloc] initWithDictionary:responseModel.data error:nil];
        
                                            self.token = [NSString alloc];
                                            self.token = login.token;
                                            
                                            [CSApiManager sharedManager].accessToken = self.token;
        
                                            [self connectToSocket];
                                            [self clearDatabase];
                                            [self getMessages];
                                            [self getStickers];
                                        }];
}

-(void) getMessages {
    
    NSString* url = [NSString stringWithFormat:@"%@%@/%@/0", [CSCustomConfig sharedInstance].server_url, kAppGetMessages, self.activeUser.roomID];
    
    [[CSApiManager sharedManager] apiGETCallWithURL:url
                                        indicatorVC:nil
                                    toShowIndicator:NO
                                    toHideIndicator:YES
                                       completition:^(CSResponseModel *responseModel){
        
                                           CSGetMessages *messages = [[CSGetMessages alloc] initWithDictionary:responseModel.data error:nil];
                                           
                                        
                                           
                                           for (NSArray *arrmessage in messages.messages) {
                                
                                strid=[arrmessage valueForKey:@"id"];
                                struserid=[arrmessage valueForKey:@"userID"];
                                strroomID=[arrmessage valueForKey:@"roomID"];
                                struser_name=[[arrmessage valueForKey:@"user"] valueForKey:@"name"];
                                strtype=[arrmessage valueForKey:@"type"];
                                strroomid=[[arrmessage valueForKey:@"user"] valueForKey:@"roomID"];
                                strcreated=[arrmessage valueForKey:@"created"];
                                
                                strlocalid=[arrmessage valueForKey:@"localID"];
                                strlat=[[[arrmessage valueForKey:@"location"]valueForKey:@"lat"] stringValue];
                                strlon=[[[arrmessage valueForKey:@"location"]valueForKey:@"lng"] stringValue];
                                strseenBy=[arrmessage valueForKey:@"seenBy"];
                                NSData *jsonData = [NSJSONSerialization dataWithJSONObject:strseenBy options:NSJSONWritingPrettyPrinted error:nil];
                                NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];

                                               
                                strstatus=[arrmessage valueForKey:@"status"];
                                strattributes=@"";
                                stravatarURL=[[arrmessage valueForKey:@"user"] valueForKey:@"avatarURL"];
                                strToken=[[arrmessage valueForKey:@"user"] valueForKey:@"pushToken"];
                                message=[arrmessage valueForKey:@"message"];
                                deleted=[arrmessage valueForKey:@"deleted"];
                                user_userID=[[arrmessage valueForKey:@"user"] valueForKey:@"userID"];
                                file_id=[[[arrmessage valueForKey:@"file"] valueForKey:@"file"]valueForKey:@"id"];
                                mimetype=[[[arrmessage valueForKey:@"file"] valueForKey:@"file"] valueForKey:@"mimeType"];
                                file_name=[[[arrmessage valueForKey:@"file"] valueForKey:@"file"] valueForKey:@"name"];
                                file_size=[[[arrmessage valueForKey:@"file"] valueForKey:@"file"] valueForKey:@"size"];
                                thumb_id=[[[arrmessage valueForKey:@"file"] valueForKey:@"thumb"]valueForKey:@"id"];
                                
                                thumb_name=[[[arrmessage valueForKey:@"file"] valueForKey:@"thumb"] valueForKey:@"name"];
                                thumb_size=[[[arrmessage valueForKey:@"file"] valueForKey:@"thumb"] valueForKey:@"size"];

    SQLite *sqlObj=[[SQLite alloc] initWithSQLFile:@"SpikaChatDB.sqlite"];
                                               
                                      [sqlObj openDb];
                                               
                                               NSString *str = message;
                                               str = [str stringByReplacingOccurrencesOfString:@"'" withString:@""];
                                               
                                               
                                               
     NSString *query = [NSString stringWithFormat:@"insert into tblmessage (id,userID,roomID,user_name,type,message,created,localID,seenBy,status,attributes,avatarURL,pushToken,deleted,user_userID,file_id,mimetype,file_name,file_size,thumb_id,thumb_name,thumb_size,latitude,longitude,logedInUser,logedInUserName) values ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",strid,struserid,strroomID,struser_name,strtype,str,strcreated,strlocalid,jsonString,strstatus,strattributes,stravatarURL,strToken,deleted,user_userID,file_id,mimetype,file_name,file_size,thumb_id,thumb_name,thumb_size,strlat,strlon,_loggedinuser.userID,_loggedinuser.name];
                                               
   
                                [sqlObj insertDb:query];
                                               
                                               
                                [sqlObj closeDb];

                                           }
                                           
                                           
                                           
                                           [self.lastDataLoadedFromNet removeAllObjects];
                                           [self.lastDataLoadedFromNet addObjectsFromArray:messages.messages];
                                           
                                           [self.messages addObjectsFromArray:messages.messages];
                                           
                                           [self sortMessages];
                                           [self.tableView reloadData];
                                           [MBProgressHUD hideHUDForView:self.view animated:YES];

                                           
                                           //send unseen messages
                                           NSArray* unSeenMessages = [CSUtils generateUnSeenMessageIdsFrom:messages.messages andActiveUser:self.activeUser];
                                           [self sendOpenMessages:unSeenMessages];
                                     

                                       }];
    
}

-(void) getMessagesWithLastMessageId:(NSString *) lastMessageId {
    
    NSString* url = [NSString stringWithFormat:@"%@%@/%@/%@", [CSCustomConfig sharedInstance].server_url, kAppGetMessages, self.activeUser.roomID, lastMessageId];
    
    [[CSApiManager sharedManager] apiGETCallWithURL:url
                                        indicatorVC:nil
                                       completition:^(CSResponseModel *responseModel) {
        
                                           CSGetMessages *messages = [[CSGetMessages alloc] initWithDictionary:responseModel.data error:nil];
                                           
                                           for (NSArray *arrmessage in messages.messages) {
                                               
                                               strid=[arrmessage valueForKey:@"id"];
                                               struserid=[arrmessage valueForKey:@"userID"];
                                               strroomID=[arrmessage valueForKey:@"roomID"];
                                               struser_name=[[arrmessage valueForKey:@"user"] valueForKey:@"name"];
                                               strtype=[arrmessage valueForKey:@"type"];
                                               strroomid=[[arrmessage valueForKey:@"user"] valueForKey:@"roomID"];
                                               strcreated=[arrmessage valueForKey:@"created"];
                                              
                                               strlocalid=[arrmessage valueForKey:@"localID"];
                                               strlat=[[[arrmessage valueForKey:@"location"]valueForKey:@"lat"] stringValue];
                                               strlon=[[[arrmessage valueForKey:@"location"]valueForKey:@"lng"] stringValue];
                                               strseenBy=[arrmessage valueForKey:@"seenBy"];
                                               NSData *jsonData = [NSJSONSerialization dataWithJSONObject:strseenBy options:NSJSONWritingPrettyPrinted error:nil];
                                               NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];

                                               strstatus=[arrmessage valueForKey:@"status"];
                                               strattributes=@"";
                                               stravatarURL=[[arrmessage valueForKey:@"user"] valueForKey:@"avatarURL"];
                                               strToken=[[arrmessage valueForKey:@"user"] valueForKey:@"pushToken"];
                                               message=[arrmessage valueForKey:@"message"];
                                               deleted=[arrmessage valueForKey:@"deleted"];

                                               user_userID=[[arrmessage valueForKey:@"user"] valueForKey:@"userID"];
                                               file_id=[[[arrmessage valueForKey:@"file"] valueForKey:@"file"]valueForKey:@"id"];
                                               mimetype=[[[arrmessage valueForKey:@"file"] valueForKey:@"file"] valueForKey:@"mimeType"];
                                               file_name=[[[arrmessage valueForKey:@"file"] valueForKey:@"file"] valueForKey:@"name"];
                                               file_size=[[[arrmessage valueForKey:@"file"] valueForKey:@"file"] valueForKey:@"size"];
                                               thumb_id=[[[arrmessage valueForKey:@"file"] valueForKey:@"thumb"]valueForKey:@"id"];
                                               
                                               thumb_name=[[[arrmessage valueForKey:@"file"] valueForKey:@"thumb"] valueForKey:@"name"];
                                               thumb_size=[[[arrmessage valueForKey:@"file"] valueForKey:@"thumb"] valueForKey:@"size"];

                                               SQLite *sqlObj=[[SQLite alloc] initWithSQLFile:@"SpikaChatDB.sqlite"];
                                               
                                               [sqlObj openDb];
                                               
                                               NSString *str = message;
                                               str = [str stringByReplacingOccurrencesOfString:@"'" withString:@""];
                                               
                                               
                                               
                                               NSString *query = [NSString stringWithFormat:@"insert into tblmessage (id,userID,roomID,user_name,type,message,created,localID,seenBy,status,attributes,avatarURL,pushToken,deleted,user_userID,file_id,mimetype,file_name,file_size,thumb_id,thumb_name,thumb_size,latitude,longitude,logedInUser,logedInUserName) values ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",strid,struserid,strroomID,struser_name,strtype,str,strcreated,strlocalid,jsonString,strstatus,strattributes,stravatarURL,strToken,deleted,user_userID,file_id,mimetype,file_name,file_size,thumb_id,thumb_name,thumb_size,strlat,strlon,_loggedinuser.userID,_loggedinuser.name];

                                               
                                               
                                               [sqlObj insertDb:query];
                                               
                                               
                                               [sqlObj closeDb];
                                               
                                           }
                                               
                                           
                                           
                                           [self.lastDataLoadedFromNet removeAllObjects];
                                           [self.lastDataLoadedFromNet addObjectsFromArray:messages.messages];
                                           [self.messages addObjectsFromArray:messages.messages];
                                           
                                           
                                           
                                           [self sortMessages];
                                           [self.tableView reloadData];
                                           [MBProgressHUD hideHUDForView:self.view animated:YES];

                                           
                                           self.isLoading = NO;
//                                           [self.indicator stopAnimating];
                                       }];
    
}

-(void) getLatestMessages:(NSString *) lastMessageId {
    
    NSString* url = [NSString stringWithFormat:@"%@%@/%@/%@", [CSCustomConfig sharedInstance].server_url, kAppGetLatestMessages, self.activeUser.roomID, lastMessageId];
    
    [[CSApiManager sharedManager] apiGETCallWithURL:url
                                        indicatorVC:nil
                                    toShowIndicator:NO
                                    toHideIndicator:YES
                                       completition:^(CSResponseModel *responseModel) {
        
                                           CSGetMessages *messages = [[CSGetMessages alloc] initWithDictionary:responseModel.data error:nil];
                                           
                                           for (NSArray *arrmessage in messages.messages) {
                                               
                                               strid=[arrmessage valueForKey:@"id"];
                                               struserid=[arrmessage valueForKey:@"userID"];
                                               strroomID=[arrmessage valueForKey:@"roomID"];
                                               struser_name=[[arrmessage valueForKey:@"user"] valueForKey:@"name"];
                                               strtype=[arrmessage valueForKey:@"type"];
                                               strroomid=[[arrmessage valueForKey:@"user"] valueForKey:@"roomID"];
                                               strcreated=[arrmessage valueForKey:@"created"];
                                              
                                               strlocalid=[arrmessage valueForKey:@"localID"];
                                               strlat=[[[arrmessage valueForKey:@"location"]valueForKey:@"lat"] stringValue];
                                               strlon=[[[arrmessage valueForKey:@"location"]valueForKey:@"lng"] stringValue];
                                               strseenBy=[arrmessage valueForKey:@"seenBy"];
                                               NSData *jsonData = [NSJSONSerialization dataWithJSONObject:strseenBy options:NSJSONWritingPrettyPrinted error:nil];
                                               NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];

                                               strstatus=[arrmessage valueForKey:@"status"];
                                               strattributes=@"";
                                               stravatarURL=[[arrmessage valueForKey:@"user"] valueForKey:@"avatarURL"];
                                               strToken=[[arrmessage valueForKey:@"user"] valueForKey:@"pushToken"];
                                               message=[arrmessage valueForKey:@"message"];
                                               deleted=[arrmessage valueForKey:@"deleted"];

                                               user_userID=[[arrmessage valueForKey:@"user"] valueForKey:@"userID"];
                                               file_id=[[[arrmessage valueForKey:@"file"] valueForKey:@"file"]valueForKey:@"id"];
                                               mimetype=[[[arrmessage valueForKey:@"file"] valueForKey:@"file"] valueForKey:@"mimeType"];
                                               file_name=[[[arrmessage valueForKey:@"file"] valueForKey:@"file"] valueForKey:@"name"];
                                               file_size=[[[arrmessage valueForKey:@"file"] valueForKey:@"file"] valueForKey:@"size"];
                                               thumb_id=[[[arrmessage valueForKey:@"file"] valueForKey:@"thumb"]valueForKey:@"id"];
                                               
                                               thumb_name=[[[arrmessage valueForKey:@"file"] valueForKey:@"thumb"] valueForKey:@"name"];
                                               thumb_size=[[[arrmessage valueForKey:@"file"] valueForKey:@"thumb"] valueForKey:@"size"];
                                               
                                               NSString *str = message;
                                               str = [str stringByReplacingOccurrencesOfString:@"'" withString:@""];

                                               SQLite *sqlObj=[[SQLite alloc] initWithSQLFile:@"SpikaChatDB.sqlite"];
                                               
                                               [sqlObj openDb];
                                               
                                               
                                               
                                               NSString *query = [NSString stringWithFormat:@"insert into tblmessage (id,userID,roomID,user_name,type,message,created,localID,seenBy,status,attributes,avatarURL,pushToken,deleted,user_userID,file_id,mimetype,file_name,file_size,thumb_id,thumb_name,thumb_size,latitude,longitude,logedInUser,logedInUserName) values ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",strid,struserid,strroomID,struser_name,strtype,str,strcreated,strlocalid,jsonString,strstatus,strattributes,stravatarURL,strToken,deleted,user_userID,file_id,mimetype,file_name,file_size,thumb_id,thumb_name,thumb_size,strlat,strlon,_loggedinuser.userID,_loggedinuser.name];
                                               
                                               [sqlObj insertDb:query];
                                               
                                               
                                               [sqlObj closeDb];
                                               
                                           }
        
                                           
                                           [self.lastDataLoadedFromNet addObjectsFromArray:messages.messages];
                                           [self.messages addObjectsFromArray:messages.messages];
                                           
                                           
                                           
                                           [self sortMessages];
                                           [self.tableView reloadData];
                                           [MBProgressHUD hideHUDForView:self.view animated:YES];

                                           
                                           //send unseen messages
                                           NSArray* unSeenMessages = [CSUtils generateUnSeenMessageIdsFrom:messages.messages andActiveUser:self.activeUser];
                                           [self sendOpenMessages:unSeenMessages];
                                           
                                       }];
    
}


-(void)clearDatabase{

    SQLite *sqlObj=[[SQLite alloc] initWithSQLFile:@"SpikaChatDB.sqlite"];
    
    [sqlObj openDb];
    
    
    
    NSString *query = [NSString stringWithFormat:@"delete from tblmessage where roomID = '%@'",self.activeUser.roomID];
    
    [sqlObj updateDb:query];
    
    
    [sqlObj closeDb];
    


}

-(void) sortMessages{
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:paramCreated ascending:NO];
    [self.messages sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    [self.lastDataLoadedFromNet sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
}

-(void) getStickers {
    NSString* url = [NSString stringWithFormat:@"%@%@", [CSCustomConfig sharedInstance].server_url, kAppGetStickers];
    
    [[CSApiManager sharedManager] apiGETCallWithURL:url
                                       completition:^(CSResponseModel *responseModel) {
                                           self.stickerList = [[CSStickerListModel alloc] initWithDictionary:responseModel.data error:nil];
                                           if (self.stickerView != nil) {
                                               self.stickerView.stickerList = self.stickerList;
                                           }
                                       }];
}

#pragma mark - UI HELPERS

-(void) generateTypingLabel{
    
    if (self.typingUsers.count < 1) {
        [self.titleView setSubtitle:@"Online"];
//        [self isonlinetitle];
      
    }
    else if (self.typingUsers.count == 1) {
        CSUserModel* user = [self.typingUsers objectAtIndex:0];
        [self.titleView setSubtitle:[NSString stringWithFormat:NSLocalizedStringFromTable(@"typing...", @"CSChatLocalization", nil)]];
    }
    else {
        NSString* text = @"";
        for(CSUserModel* item in self.typingUsers){
            text = [text stringByAppendingFormat:@"%@, ", item.name];
        }
        text = [text substringToIndex:text.length - 2];
        [self.titleView setSubtitle:[NSString stringWithFormat:NSLocalizedStringFromTable(@"typing...", @"CSChatLocalization", nil)]];
    }
}

-(BOOL) isUserAlreadyTyping: (CSUserModel*) user{
    for(CSUserModel* item in self.typingUsers){
        if([item.userID isEqualToString:user.userID]){
            [self.typingUsers removeObject:item];
            return YES;
        }
    }
    return NO;
}

- (IBAction)changedTextInTextField:(id)sender {
//    if(self.etMessage.text.length > 0 && self.isTyping == NO){
//        self.isTyping = YES;
//        [self sendTypingWithType:[NSString stringWithFormat:@"%d", kAppTypingStatusON]];
//    }else if(self.etMessage.text.length == 0 && self.isTyping == YES){
//        self.isTyping = NO;
//        [self sendTypingWithType:[NSString stringWithFormat:@"%d", kAppTypingStatusOFF]];
//    }
}

- (IBAction)changeEditing:(id)sender {
    if(self.etMessage.text.length > 0 && self.isTyping == NO){
        self.isTyping = YES;
        [self sendTypingWithType:[NSString stringWithFormat:@"%d", kAppTypingStatusON]];
        [self.sendButton setImage:[UIImage imageNamed:@"send_btn"] forState:UIControlStateNormal];
    }else if(self.etMessage.text.length == 0 && self.isTyping == YES){
        self.isTyping = NO;
        [self sendTypingWithType:[NSString stringWithFormat:@"%d", kAppTypingStatusOFF]];
        [self.sendButton setImage:[UIImage imageNamed:@"attach_btn"] forState:UIControlStateNormal];
    }

}

#pragma mark - table view methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.messages.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CSMessageModel *message = [self.messages objectAtIndex:indexPath.row];
    CSBaseTableViewCell *cell;


    NSLog(@"%@""%@""%d",message.user.userID,self.activeUser.userID,[message.type intValue]);
    
    if ([message.deleted longLongValue] > 0) {
        if ([message.user.userID isEqualToString:self.loggedinuser.userID]) {
            cell = [self.tableView dequeueReusableCellWithIdentifier:kAppMyTextMessageCell forIndexPath:indexPath];
        }
        else {
            cell = [self.tableView dequeueReusableCellWithIdentifier:kAppYourTextMessageCell forIndexPath:indexPath];
        }
    }
    else if ([message.user.userID isEqualToString:self.loggedinuser.userID] && ([message.type intValue] == kAppTextMessageType)) {
        cell = [self.tableView dequeueReusableCellWithIdentifier:kAppMyTextMessageCell forIndexPath:indexPath];
    }
    else if ([message.user.userID isEqualToString:self.loggedinuser.userID] && ([message.type intValue] == kAppFileMessageType || [message.type intValue] == kAppLocationMessageType || [message.type intValue] == kAppContactMessageType)){
        if([CSUtils isMessageAnImage:message]) {
            cell = [self.tableView dequeueReusableCellWithIdentifier:kAppMyImageMessageCell forIndexPath:indexPath];
        }
        else {
            cell = [self.tableView dequeueReusableCellWithIdentifier:kAppMyMediaMessageCell forIndexPath:indexPath];
        }
    }
    else if ([message.type intValue] == kAppNewUserMessageType || [message.type intValue] == kAppLeaveUserMessageType) {
        cell = [self.tableView dequeueReusableCellWithIdentifier:kAppUserInfoMessageCell forIndexPath:indexPath];
    }
    else if ([message.type intValue] == kAppTextMessageType) {
        cell = [self.tableView dequeueReusableCellWithIdentifier:kAppYourTextMessageCell forIndexPath:indexPath];
    }
    else if ([message.type intValue] == kAppFileMessageType || [message.type intValue] == kAppLocationMessageType || [message.type intValue] == kAppContactMessageType) {
        if ([CSUtils isMessageAnImage:message]) {
            cell = [self.tableView dequeueReusableCellWithIdentifier:kAppYourImageMessageCell forIndexPath:indexPath];
        }
        else {
            cell = [self.tableView dequeueReusableCellWithIdentifier:kAppYourMediaMessageCell forIndexPath:indexPath];
        }
    }
    else if ([message.type intValue] == kAppStickerMessageType) {
        if ([message.user.userID isEqualToString:self.loggedinuser.userID]) {
            cell = [self.tableView dequeueReusableCellWithIdentifier:kAppMyStickerMessageCell forIndexPath:indexPath];
        }
        else {
            cell = [self.tableView dequeueReusableCellWithIdentifier:kAppYourStickerMessageCell forIndexPath:indexPath];
        }
    }
    
    CSMessageModel *messageAfter = [self.messages objectAtIndex:MAX(indexPath.row - 1, 0)];
    cell.shouldShowAvatar = ![message.userID isEqualToString:messageAfter.userID] || indexPath.row == 0 || [messageAfter.type intValue] == kAppNewUserMessageType || [messageAfter.type intValue] == kAppLeaveUserMessageType ? YES : NO;
    
    CSMessageModel *messageBefore = [self.messages objectAtIndex:MAX(MIN(indexPath.row + 1, self.messages.count - 1), 0)];
    cell.shouldShowName = ![message.userID isEqualToString:messageBefore.userID] || indexPath.row == self.messages.count - 1 || [messageBefore.type intValue] == kAppNewUserMessageType || [messageBefore.type intValue] == kAppLeaveUserMessageType ? YES : NO;
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:message.created.longLongValue / 1000];
    NSDate *dateBefore = [NSDate dateWithTimeIntervalSince1970:messageBefore.created.longLongValue / 1000];
    cell.shouldShowDate =  (![[NSCalendar currentCalendar] isDate:date inSameDayAsDate:dateBefore] || indexPath.row == self.messages.count - 1) && message.created.intValue ? YES : NO;
    
    if (cell.shouldShowDate==YES) {
        NSLog(@"yes");
    }
    
    cell.message = message;
    cell.delegate = self;
    [cell setTransform:CGAffineTransformMakeRotation(M_PI)];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CSMessageModel* message = [self.messages objectAtIndex:indexPath.row];
    
    if([message.deleted longLongValue] > 0) {
        return;
    }
    
    if ([message.type intValue] == kAppFileMessageType) {
        
//        self.navigationController.navigationBar.userInteractionEnabled = NO;
        
        float navigationHeight = self.navigationController.navigationBar.frame.size.height;
        float statusHeight = [UIApplication sharedApplication].statusBarFrame.size.height;
        
        
        NSString* filePath = [CSUtils getFileFromFileModel:message.file];
        if(![CSUtils isFileExistsWithFileName:filePath]){
            
            Reachability *reachability = [Reachability reachabilityForInternetConnection];
            NetworkStatus networkStatus = [reachability currentReachabilityStatus];
            if(networkStatus != NotReachable)
            {
                CSDownloadManager* downloadManager = [CSDownloadManager new];
                [downloadManager downloadFileWithUrl:[NSURL URLWithString:[CSUtils generateDownloadURLFormFileId:message.file.file.id]] destination:[NSURL URLWithString:filePath] viewForLoading:self.view completition:^(BOOL succes){
                    
                    if([CSUtils isMessageAnImage:message]){
                        CSImagePreviewView* previewView = [[CSImagePreviewView alloc] init];
                        [self.view addSubview:previewView];
                        
                        
                        [previewView initializeViewWithMessage:message navigationAndStatusBarHeight:(statusHeight + navigationHeight) dismiss:^(void){
                            
                            [previewView removeFromSuperview];
                            self.navigationController.navigationBar.userInteractionEnabled = YES;
                            
                        }];
                    }else if([CSUtils isMessageAVideo:message]){
                        
                        CSVideoPreviewView* previewView = [[CSVideoPreviewView alloc] init];
                        [self.view addSubview:previewView];
                        
                        [previewView initializeViewWithMessage:message navigationAndStatusBarHeight:(statusHeight + navigationHeight) dismiss:^(void){
                            
                            [previewView removeFromSuperview];
                            self.navigationController.navigationBar.userInteractionEnabled = YES;
                            
                        }];
                    }else if([CSUtils isMessageAAudio:message]){
                        
                        CSAudioPreviewView* previewView = [[CSAudioPreviewView alloc] init];
                        [self.view addSubview:previewView];
                        
                        [previewView initializeViewWithMessage:message navigationAndStatusBarHeight:(statusHeight + navigationHeight) dismiss:^(void){
                            
                            [previewView removeFromSuperview];
                            self.navigationController.navigationBar.userInteractionEnabled = YES;
                            
                        }];
                    }else{
                        
                        NSString * extension = [filePath pathExtension];
                        
                        NSLog(@"PDF :%@",extension);
                        
                        if([extension isEqualToString:@"jpeg"]||[extension isEqualToString:@"png"]||[extension isEqualToString:@"gif"]||[extension isEqualToString:@"jpg"])
                        {
                            //Image
                            CSImagePreviewView* previewView = [[CSImagePreviewView alloc] init];
                            [self.view addSubview:previewView];
                            
                            
                            [previewView initializeViewWithMessage:message navigationAndStatusBarHeight:(statusHeight + navigationHeight) dismiss:^(void){
                                
                                [previewView removeFromSuperview];
                                self.navigationController.navigationBar.userInteractionEnabled = YES;
                                
                            }];
                        }else if([extension isEqualToString:@"mp4"]){
                            
                            //Video
                            
                            CSVideoPreviewView* previewView = [[CSVideoPreviewView alloc] init];
                            [self.view addSubview:previewView];
                            
                            [previewView initializeViewWithMessage:message navigationAndStatusBarHeight:(statusHeight + navigationHeight) dismiss:^(void){
                                
                                [previewView removeFromSuperview];
                                self.navigationController.navigationBar.userInteractionEnabled = YES;
                                
                            }];
                        } else if([extension isEqualToString:@"wav"]||[extension isEqualToString:@"mp3"]){
                            
                            //Audio
                            
                            CSAudioPreviewView* previewView = [[CSAudioPreviewView alloc] init];
                            [self.view addSubview:previewView];
                            
                            [previewView initializeViewWithMessage:message navigationAndStatusBarHeight:(statusHeight + navigationHeight) dismiss:^(void){
                                
                                [previewView removeFromSuperview];
                                self.navigationController.navigationBar.userInteractionEnabled = YES;
                                
                            }];
                        }else {
                            [self openDocument:filePath];
                            
                        }
                        
                    }
                    
                }];
            } else {
                
//                            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"File not downloaded" message:nil delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil ];
//                
//                            [alert show];
                
                
                [UIAlertView showWithTitle:@"File not downloaded"
                                   message:nil
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil
                                  tapBlock:nil
                 ];
            }
            

            
        }else{
            
            if([CSUtils isMessageAnImage:message]){
                CSImagePreviewView* previewView = [[CSImagePreviewView alloc] init];
                [self.view addSubview:previewView];
                
                
                [previewView initializeViewWithMessage:message navigationAndStatusBarHeight:(statusHeight + navigationHeight) dismiss:^(void){
                    
                    [previewView removeFromSuperview];
                    self.navigationController.navigationBar.userInteractionEnabled = YES;
                    
                }];
            }else if([CSUtils isMessageAVideo:message]){
                
                CSVideoPreviewView* previewView = [[CSVideoPreviewView alloc] init];
                [self.view addSubview:previewView];
                
                [previewView initializeViewWithMessage:message navigationAndStatusBarHeight:(statusHeight + navigationHeight) dismiss:^(void){
                    
                    [previewView removeFromSuperview];
                    self.navigationController.navigationBar.userInteractionEnabled = YES;
                    
                }];
            }else if([CSUtils isMessageAAudio:message]){
                
                CSAudioPreviewView* previewView = [[CSAudioPreviewView alloc] init];
                [self.view addSubview:previewView];
                
                [previewView initializeViewWithMessage:message navigationAndStatusBarHeight:(statusHeight + navigationHeight) dismiss:^(void){
                    
                    [previewView removeFromSuperview];
                    self.navigationController.navigationBar.userInteractionEnabled = YES;
                    
                }];
            }else{
                
                NSString * extension = [filePath pathExtension];
                
                NSLog(@"PDF :%@",extension);
                
                if([extension isEqualToString:@"jpeg"]||[extension isEqualToString:@"png"]||[extension isEqualToString:@"gif"]||[extension isEqualToString:@"jpg"])
                {
                    //Image
                    CSImagePreviewView* previewView = [[CSImagePreviewView alloc] init];
                    [self.view addSubview:previewView];
                    
                    
                    [previewView initializeViewWithMessage:message navigationAndStatusBarHeight:(statusHeight + navigationHeight) dismiss:^(void){
                        
                        [previewView removeFromSuperview];
                        self.navigationController.navigationBar.userInteractionEnabled = YES;
                        
                    }];
                }else if([extension isEqualToString:@"mp4"]){
                    
                    //Video
                    
                    CSVideoPreviewView* previewView = [[CSVideoPreviewView alloc] init];
                    [self.view addSubview:previewView];
                    
                    [previewView initializeViewWithMessage:message navigationAndStatusBarHeight:(statusHeight + navigationHeight) dismiss:^(void){
                        
                        [previewView removeFromSuperview];
                        self.navigationController.navigationBar.userInteractionEnabled = YES;
                        
                    }];
                } else if([extension isEqualToString:@"wav"]||[extension isEqualToString:@"mp3"]){
                    
                    //Audio
                    
                    CSAudioPreviewView* previewView = [[CSAudioPreviewView alloc] init];
                    [self.view addSubview:previewView];
                    
                    [previewView initializeViewWithMessage:message navigationAndStatusBarHeight:(statusHeight + navigationHeight) dismiss:^(void){
                        
                        [previewView removeFromSuperview];
                        self.navigationController.navigationBar.userInteractionEnabled = YES;
                        
                    }];
                }else {
                    [self openDocument:filePath];
                    
                }
                
            }
        }
        
        
    }
    else if ([message.type intValue] == kAppLocationMessageType) {
        CSMapViewController* controller = [[CSMapViewController alloc] initWithMessage:message];
        [self.navigationController pushViewController:controller animated:YES];
    }
    else if ([message.type intValue] == kAppContactMessageType) {
        ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, NULL);
        
        if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined) {
            ABAddressBookRequestAccessWithCompletion(addressBookRef, ^(bool granted, CFErrorRef error) {
                if (granted) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self addContactToAddressBook:message.message];
                    });
                }
            });
        }
        else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized) {
            [self addContactToAddressBook:message.message];
        }
        else {
            [UIAlertView showWithTitle:NSLocalizedStringFromTable(@"Privacy Error", @"CSChatLocalization", nil)
                               message:NSLocalizedStringFromTable(@"Go to\nSettings > Privacy > Contacts\nand enable access", @"CSChatLocalization", nil)
                     cancelButtonTitle:@"OK"
                     otherButtonTitles:nil
                              tapBlock:nil
             ];
        }
    }
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height) {
        if (!self.isLoading) {
            [self onRefresh];
        }
    }
}

@synthesize documentController;
-(void) openDocument:(NSString*) filePath{
    
    NSString * extension = [filePath pathExtension];
    
    NSLog(@"PDF :%@",extension);
    
    if ([extension isEqualToString:@"pdf"]||[extension isEqualToString:@"Pdf"]||[extension isEqualToString:@"PDF"]) {
        NSLog(@"PDF :%@",extension);
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        PdfViewController * pdfViewController = [storyboard instantiateViewControllerWithIdentifier:@"PdfViewController"];
        pdfViewController.pathString = filePath;
        
        [self.navigationController pushViewController:pdfViewController animated:YES];
        
        
        
    } else {
        
        self.documentController = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:filePath]];
        self.documentController.delegate = self;
        [self.documentController presentOpenInMenuFromRect:CGRectZero inView:self.view animated:YES];
        
    }
    

    
    
}

-(void) onRefresh {
    
    NSString* lastMessageId = @"0";
    if(self.lastDataLoadedFromNet.count > 0){
        
        self.isLoading = YES;
//        [self.indicator startAnimating];
//        self.tableView.tableFooterView.hidden = NO;
        
        CSMessageModel* lastMessage = [self.lastDataLoadedFromNet lastObject];
        lastMessageId = lastMessage._id;
        
        [self getMessagesWithLastMessageId:lastMessageId];
    }
}

-(void) onSettingsClicked{
    [_chatsettingsView animateTableViewToOpen:self.chatsettingsView.hidden withMenu:self.chatsettingsView];
}

-(void) onSettingsClickedPosition:(NSInteger)position{
    
    if(position == 0){
        CSUsersViewController* viewController = [[CSUsersViewController alloc] initWithRoomID:self.activeUser.roomID andToken:self.token];
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

-(void)onDismissClicked{
    
    [self onSettingsClicked];
    
}



//delegates from cells
-(void)onInfoClicked:(CSMessageModel *)message{
    CSMessageInfoViewController *messageInfo = [[CSMessageInfoViewController alloc] initWithMessage:message andUser:self.activeUser];
    messageInfo.stronlinechk=online;
    
//    SQLite *sqlObj2=[[SQLite alloc] initWithSQLFile:@"SpikaChatDB.sqlite"];
//    
//                [sqlObj2 openDb];
//    
//                NSString *query2=[NSString stringWithFormat:@"SELECT  at,seenid,seenavatarurl,seencreated,seenname,seentoken,seenuserid,messageid from seenby where messageid = '%@'",message._id];
//    
//                [sqlObj2 readDb:query2];
//    
//                while ([sqlObj2 hasNextRow])
//                {
//    
//    
//                    CSUserModel *userseen=[[CSUserModel alloc]init];
//    
//                    userseen._id=[sqlObj2 getColumn:1 type:@"text"];
//                    userseen.avatarURL=[sqlObj2 getColumn:2 type:@"text"];
//                    userseen.created=[NSNumber numberWithInt:[[sqlObj2 getColumn:3 type:@"text"] intValue]];
//                    userseen.name=[sqlObj2 getColumn:4 type:@"text"];
//                    userseen.pushToken=[sqlObj2 getColumn:5 type:@"text"];
//                    userseen.userID=[sqlObj2 getColumn:6 type:@"text"];
//                    userseen.strmessgeid=[sqlObj2 getColumn:7 type:@"text"];
//    
//                    CSSeenByModel *byseen=[[CSSeenByModel alloc]init];
//                    byseen.at=[NSNumber numberWithInt:[[sqlObj2 getColumn:0 type:@"text"] intValue]];
//                    byseen.user=userseen;
//    
//                    [_seeby addObject:byseen];
//                }
//                
//                
//                [sqlObj2 closeDb];
//    
//    message.seenBy=_seeby;
    

    [self.navigationController pushViewController:messageInfo animated:YES];
}

#pragma mark - menu
-(void) openMenu{

    [self.etMessage resignFirstResponder];
//    [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"attachment"];
    
    if(!self.menuView){
        
        self.menuView = [[CSMenuView alloc] init];
        
        [self.menuView initializeInView:self.view dismiss:^(void){
            [self.menuView removeFromSuperview];
            self.menuView = nil;
        }];
        
        self.menuView.delegate = self;
    }
}

-(void)onCamera{
    
    [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"attachment"];

    CSUploadImagePreviewViewController* controller = [CSUploadImagePreviewViewController new];
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)onGallery{
    
    [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"attachment"];

    CSUploadImagePreviewViewController* controller = [[CSUploadImagePreviewViewController alloc] initWithType:kAppGalleryType];
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)onLocation{
    
    [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"attachment"];

    CSMapViewController* controller = [[CSMapViewController alloc] initToPostLocation];
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)onFile{
    
    [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"attachment"];

    UIImagePickerController *picker= [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    picker.mediaTypes = [NSArray arrayWithObjects:(NSString *)kUTTypeMovie, (NSString *) kUTTypeAudio, (NSString*) kUTTypeImage, (NSString*) kUTTypeMP3, nil];
    
    [self presentViewController:picker animated:YES completion:nil];
}

-(void)onVideo{
    
    [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"attachment"];
    
    CSUploadVideoPreviewViewController* controller = [CSUploadVideoPreviewViewController new];
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)onContact{
    
    [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"attachment"];
    
    ABPeoplePickerNavigationController *picker = [[ABPeoplePickerNavigationController alloc] init];
    picker.peoplePickerDelegate = self;
    [self presentViewController:picker animated:YES completion:nil];
}

-(void)onAudio {
    
    [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"attachment"];
    
    CSRecordAudioViewController* controller = [CSRecordAudioViewController new];
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - file picker
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    NSString* mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    
    if(CFStringCompare ((__bridge CFStringRef) mediaType, kUTTypeImage, 0) == kCFCompareEqualTo){
        [self uploadImage:info];
    }else if (CFStringCompare ((__bridge CFStringRef) mediaType, kUTTypeMovie, 0) == kCFCompareEqualTo){
        [self uploadVideo:info];
    }else if (CFStringCompare ((__bridge CFStringRef) mediaType, kUTTypeVideo, 0) == kCFCompareEqualTo){
        [self uploadVideo:info];
    }else{
        [self uploadOtherFile:info];
    }
    
    [picker dismissViewControllerAnimated:YES completion:^{
    }];
    
}

-(void) uploadImage:(NSDictionary*) data{
    NSURL *imageUrl = (NSURL *)[data valueForKey:UIImagePickerControllerReferenceURL];
    NSString* extensions = [imageUrl pathExtension];
    NSString* mimeType;
    
    if([extensions isEqualToString:@"JPG"] || [extensions isEqualToString:@"jpg"]){
        mimeType = kAppImageJPG;
    }else if([extensions isEqualToString:@"PNG"] || [extensions isEqualToString:@"png"]){
        mimeType = kAppImagePNG;
    }else  if([extensions isEqualToString:@"GIF"] || [extensions isEqualToString:@"gif"]){
        mimeType = kAppImageGIF;
    }else{
        mimeType = kAppImageJPG;
    }
    
    NSString* fileName = [NSString stringWithFormat:@"%@_%ld.%@", @"image", (long)[[NSDate date] timeIntervalSince1970], extensions];
    
    UIImage * image = [data valueForKey:UIImagePickerControllerOriginalImage];
    
    CSUploadManager* uploadManager = [CSUploadManager new];
    [uploadManager uploadImage:image fileName:fileName mimeType:mimeType viewForLoading:self.view completition:^(id responseObject){
        
        NSNotification* note = [[NSNotification alloc] initWithName:kAppFileUploadedNotification object:nil userInfo:[NSDictionary dictionaryWithObject:responseObject forKey:paramResponseObject]];

        [self sendFileMessage:note];
        
    }];
}

-(void) uploadVideo:(NSDictionary*) data{
    NSURL *videoUrl = (NSURL *)[data valueForKey:UIImagePickerControllerMediaURL];
    NSString *path = [videoUrl path];
    NSData *videoData = [[NSFileManager defaultManager] contentsAtPath:path];
    
    NSString *mimeType = @"video/mp4";
    //    _fileName = [path lastPathComponent];
    NSString *fileName = [NSString stringWithFormat:@"%@_%ld.%@", @"video", (long)[[NSDate date] timeIntervalSince1970], @"mp4"];
    
    CSUploadManager* manager = [CSUploadManager new];
    [manager uploadFile:videoData fileName:fileName mimeType:mimeType viewForLoading:self.view completition:^(id responseObject){
        
        NSNotification* note = [[NSNotification alloc] initWithName:kAppFileUploadedNotification object:nil userInfo:[NSDictionary dictionaryWithObject:responseObject forKey:paramResponseObject]];
        
        [self sendFileMessage:note];
        
        
    }];
}

-(void) uploadOtherFile:(NSDictionary*) data{
    NSURL *videoUrl = (NSURL *)[data valueForKey:UIImagePickerControllerMediaURL];
    NSString *path = [videoUrl path];
    NSData *videoData = [[NSFileManager defaultManager] contentsAtPath:path];
    
    NSString *mimeType = @"application/octet-stream";
    //    _fileName = [path lastPathComponent];
    NSString *fileName = [NSString stringWithFormat:@"%ld_%@", (long)[[NSDate date] timeIntervalSince1970], [path lastPathComponent]];
    
    CSUploadManager* manager = [CSUploadManager new];
    [manager uploadFile:videoData fileName:fileName mimeType:mimeType viewForLoading:self.view completition:^(id responseObject){
        
        NSNotification* note = [[NSNotification alloc] initWithName:kAppFileUploadedNotification object:nil userInfo:[NSDictionary dictionaryWithObject:responseObject forKey:paramResponseObject]];
        
        [self sendFileMessage:note];
        
        
    }];
}

#pragma mark - socket delegate

-(void)socketDidReceiveNewMessage:(CSMessageModel *)message {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self didReceiveNewMessage:message];
        [self getMessageModel:message];
        
    });
}

-(void)socketDidReceiveTyping:(CSTypingModel *)typing{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self didReceiveTyping:typing];
    });
}

-(void)socketDidReceiveUserLeft:(CSUserModel *)userLeft{
    dispatch_async(dispatch_get_main_queue(), ^{
         [self didUserLeft:userLeft];
        [self.titleView setSubtitle:@""];
    });
}

-(void)socketDidReceiveMessageUpdated:(NSArray *)updatedMessages{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self didMessageUpdated:updatedMessages];
    });
}

-(void)socketDidReceiveError:(NSNumber *)errorCode {
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIAlertView showWithTitle:@"Socket Error"
                           message:[CSChatErrorCodes errorForCode:errorCode]
                 cancelButtonTitle:@"OK"
                 otherButtonTitles:nil
                          tapBlock:nil
         ];
    });
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    NSLog(@"dealloc: ChatViewController");
}

#pragma mark - adress book delegate

-(void)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker didSelectPerson:(ABRecordRef)person {
    CFArrayRef peopleArray = CFArrayCreate(NULL, (void *)&person, 1, &kCFTypeArrayCallBacks);
    NSData *vCardData = (__bridge NSData*) ABPersonCreateVCardRepresentationWithPeople(peopleArray);
    NSString *vCard = [[NSString alloc] initWithData:vCardData encoding:NSUTF8StringEncoding];
    NSLog(@"vCard > %@", vCard );
    
    [self sendContact:vCard];
    
    [self.menuView animateHide];
}


-(void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker {
    [self.menuView animateHide];
}

-(void)addContactToAddressBook:(NSString *)vCardString {
    
    [UIAlertView showWithTitle:NSLocalizedStringFromTable(@"Add to contacts", @"CSChatLocalization", nil)
                       message:NSLocalizedStringFromTable(@"Do you want add this contact to address book?", @"CSChatLocalization", nil)
             cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
             otherButtonTitles:@[NSLocalizedString(@"OK", nil)]
                      tapBlock:^(UIAlertView * _Nonnull alertView, NSInteger buttonIndex) {
                          if ([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:NSLocalizedString(@"OK", nil)]) {
                              CFDataRef vCardData = (__bridge CFDataRef)[vCardString dataUsingEncoding:NSUTF8StringEncoding];
                              ABAddressBookRef book = ABAddressBookCreate();
                              ABRecordRef defaultSource = ABAddressBookCopyDefaultSource(book);
                              CFArrayRef vCardPeople = ABPersonCreatePeopleInSourceWithVCardRepresentation(defaultSource, vCardData);
                              for (CFIndex index = 0; index < CFArrayGetCount(vCardPeople); index++) {
                                  ABRecordRef person = CFArrayGetValueAtIndex(vCardPeople, index);
                                  ABAddressBookAddRecord(book, person, NULL);
                              }
                              
                              CFRelease(vCardPeople);
                              CFRelease(defaultSource);
                              ABAddressBookSave(book, NULL);
                              CFRelease(book);
                          }
                      }
     ];
}

-(void) getMessageModel:(CSMessageModel *) message{

    if ([message.type intValue]==1000) {
        CSUserModel *userinmsg=[[CSUserModel alloc]init];
        userinmsg=message.user;
        
        //[self.users addObject:userinmsg];
        
       
            if ([userinmsg.userID isEqualToString:self.activeUser.userID]) {
                NSLog(@"user self");
            }
            else{
            
                [self.titleView setSubtitle:[NSString stringWithFormat:@"Online"]];
            }
        
        
    }
   else if  ([message.type intValue] ==1001){
    
        CSUserModel *userinmsg2=[[CSUserModel alloc]init];
        userinmsg2=message.user;
        
      // [self.users removeAllObjects];
       
      
            if ([userinmsg2.userID isEqualToString:self.activeUser.userID]) {
                NSLog(@"user self");
            }
            else{
                
                [self.titleView setSubtitle:@"Online"];
            }

    }
    
}

//-(void)isOnline {
//    NSString* url = [NSString stringWithFormat:@"%@%@/%@", [CSCustomConfig sharedInstance].server_url, kAppGetUsersInRoom, self.roomID];
//    
//    [[CSApiManager sharedManager] apiGETCallWithURL:url
//                                        indicatorVC:self
//                                       completition:^(CSResponseModel *responseModel) {
//                                           
//                                           self.users = [NSMutableArray new];
//                                           
//                                           for(NSDictionary* item in (NSArray*)responseModel.data) {
//                                               CSUserModel *itemUser = [[CSUserModel alloc] initWithDictionary:item error:nil];
//                                               [self.users addObject:itemUser];
//                                           }
//                                       }];
//
//}

-(void)isonlinetitle{
    
        NSString* url = [NSString stringWithFormat:@"%@%@/%@", [CSCustomConfig sharedInstance].server_url, kAppGetUsersInRoom, self.roomID];
    
//    NSString* url = [NSString stringWithFormat:@"%@%@/%@/0", [CSCustomConfig sharedInstance].server_url, kAppGetMessages, self.activeUser.roomID];

    
        [[CSApiManager sharedManager] apiGETCallWithURL:url
                                            indicatorVC:nil
                                           completition:^(CSResponseModel *responseModel) {
    
                                               self.users = [NSMutableArray new];
    
                                               for(NSDictionary* item in (NSArray*)responseModel.data) {
                                                   CSUserModel *itemUser = [[CSUserModel alloc] initWithDictionary:item error:nil];
                                                   [self.users addObject:itemUser];
                                                   NSLog(@"%@",_users);

                                                   
                                                   }
                                               if (_users.count==0) {
                                                   NSLog(@"No users found");
                                               }
                                               else{
                                               
                                               for (int i=0; i<=_users.count-1; i++) {
                                                   CSUserModel *modUser=[[CSUserModel alloc]init];
                                                   
                                                   modUser=[_users objectAtIndex:i];
                                                   
                                                   if (![modUser.userID isEqualToString:self.activeUser.userID]) {
                                                       
                                                       if ([zhenndareceive isEqual:@"3"]) {
                                                           
                                                           NSLog(@"its user");
                                                           
                                                           strsub=@"Online";
                                                           
                                                           
                                                           [self.titleView setSubtitle:strsub];
                                                           
                                                       } else {
                                                           
                                                           NSLog(@"Group/Room Chat");

                                                           
                                                       }
                                                       

                                                       
                                                       
                                                       
                                                   }
                                               }
                                               }

                                               

    
     }];


    
}


@end
