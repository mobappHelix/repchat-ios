//
//  ChatViewController.h
//  Prototype
//
//  Created by Ivo Peric on 28/08/15.
//  Copyright (c) 2015 Clover Studio. All rights reserved.
//


#import <UIKit/UIKit.h>

#import "CSUserModel.h"
#import "CSChatSettingsView.h"
#import "CSBaseViewController.h"
#import "CSMenuView.h"
#import "CSStickerView.h"
//#import "FMDatabase.h"
#import "Util.h"
#import "SQLite.h"
#import "Reachability.h"
#import "CSGroupModel.h"
#import "EditinfoViewController.h"
#import "PdfViewController.h"



@interface CSChatViewController : CSBaseViewController<UIActionSheetDelegate>{
    NSString *zhenndareceive;
    NSString *online;
}

-(instancetype)initWithParameters:(NSDictionary *)parameters;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (weak, nonatomic) IBOutlet UITextField *etMessage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomMargin;
@property (weak, nonatomic) IBOutlet CSChatSettingsView *chatsettingsView;
@property (weak, nonatomic) IBOutlet UIView *footerView;


- (IBAction)changedTextInTextField:(id)sender;
- (IBAction)sendMessage:(id)sender;
- (IBAction)onStickerButton:(id)sender;
@property (nonatomic,strong) NSMutableArray *arrgroupusers;

@property (nonatomic, strong) NSString* token;
@property (nonatomic, strong) NSMutableArray* messages;
@property (nonatomic, strong) NSMutableArray* tempSentMessagesLocalId;
@property (nonatomic, strong) NSMutableArray* typingUsers;
@property (nonatomic, strong) NSMutableArray* lastDataLoadedFromNet;
@property (nonatomic) BOOL isTyping;
@property (nonatomic, strong) NSString* roomID;
@property (nonatomic, strong) NSString* zhenda;
@property (nonatomic,strong)  NSString *strtitlename;


@property (nonatomic, strong) NSMutableArray* users;
//@property (nonatomic,strong) FMDatabase *database;
@property (nonatomic, strong) NSMutableArray *seeby;

- (IBAction)changeEditing:(id)sender;

@property (retain)UIDocumentInteractionController *documentController;
@property (nonatomic, strong) CSMenuView *menuView;
@property (nonatomic, strong) CSStickerView *stickerView;
@property (nonatomic,strong) CSGroupModel *grouptopasss;
@property (nonatomic,strong) CSGroupModel *groupreceived;
@property (nonatomic,strong) CSUserModel *loggedinuser;

@end
