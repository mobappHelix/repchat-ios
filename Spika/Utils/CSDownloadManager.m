//
//  DownloadManager.m
//  Prototype
//
//  Created by Ivo Peric on 01/09/15.
//  Copyright (c) 2015 Clover Studio. All rights reserved.
//

#import "CSDownloadManager.h"
#import <AFNetworking/AFNetworking.h>
#import "UIKit+AFNetworking.h"
#import "CSProgressLoadingView.h"
#import "CSConfig.h"
#import "CSCustomConfig.h"
#import "UIAlertView+Blocks.h"

@interface CSDownloadManager()


@property CSProgressLoadingView* progressView;


@end

@implementation CSDownloadManager

//
//-(void) downloadFileWithUrl:(NSURL*) url destination: (NSURL*) destination viewForLoading:(UIView*) parentView completition:(fileDownloadFinished)finished{
//    
//    if(parentView){
//        _progressView = [[CSProgressLoadingView alloc] init];
//        [parentView addSubview:_progressView];
//    }
//    
//    NSURLRequest *request = [NSURLRequest requestWithURL:url];
//    AFURLConnectionOperation *operation =   [[AFHTTPRequestOperation alloc] initWithRequest:request];
//    
//    operation.outputStream = [NSOutputStream outputStreamToFileAtPath:destination.path append:NO];
//    
//    [operation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
//        
//        if(parentView){
//            [_progressView changeProgressLabelOnMainThread:[NSString stringWithFormat:@"%lld", totalBytesRead] max:[NSString stringWithFormat:@"%lld", totalBytesExpectedToRead]];
//            [_progressView changeProgressViewOnMainThread:[NSString stringWithFormat:@"%f", (float)((float)totalBytesRead / (float)totalBytesExpectedToRead)]];
//        }
//        
//    }];
//    
//    [operation setCompletionBlock:^{
//        if(parentView){
//            [_progressView removeFromSuperview];
//        }
//        finished(YES);
//        
//    }];
//    [operation start];
//    
//}

-(void) downloadFileWithUrl:(NSURL*) url destination: (NSURL*) destination viewForLoading:(UIView*) parentView completition:(fileDownloadFinished)finished{
    
    boleanValue1 = YES;
    
    
    
    
    [[NSUserDefaults standardUserDefaults] setObject:[destination.absoluteString lastPathComponent] forKey:@"filepath"];

    
    if(parentView){
        _progressView = [[CSProgressLoadingView alloc] init];
        [parentView addSubview:_progressView];
    }
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFURLConnectionOperation *operation =   [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.outputStream = [NSOutputStream outputStreamToFileAtPath:destination.path append:NO];
    
    [operation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
        
        if(parentView){
            [_progressView changeProgressLabelOnMainThread:[NSString stringWithFormat:@"%lld", totalBytesRead] max:[NSString stringWithFormat:@"%lld", totalBytesExpectedToRead]];
            [_progressView changeProgressViewOnMainThread:[NSString stringWithFormat:@"%f", (float)((float)totalBytesRead / (float)totalBytesExpectedToRead)]];
            
            NSLog(@"Number:%@",[NSString stringWithFormat:@"%f", (float)((float)totalBytesRead / (float)totalBytesExpectedToRead)]);
            
            if (![[NSString stringWithFormat:@"%f", (float)((float)totalBytesRead / (float)totalBytesExpectedToRead)] isEqualToString:@"1.000000"]) {
                
                boleanValue1 = NO;
                
//                finished(boleanValue1);

            } else {
                boleanValue1 = YES;

            }
        }
        NSLog(@"Bool 1:%d",(BOOL)boleanValue1);

    }];
    
    [operation setCompletionBlock:^{
        if(parentView){
            [_progressView removeFromSuperview];
        }
        
//        if (!succes) {
//            NSLog(@"Fail");
//            
//            
//            NSFileManager *fileManager = [NSFileManager defaultManager];
//            
//            NSError *error;
//            BOOL success1 = [fileManager removeItemAtPath:filePath error:&error];
//            if (success1) {
//                NSLog(@"Deleted");
//                //                            UIAlertView *removedSuccessFullyAlert = [[UIAlertView alloc] initWithTitle:@"Congratulations:" message:@"Successfully removed" delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
//                //                            [removedSuccessFullyAlert show];
//            }
//            else
//            {
//                NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
//            }
//            
//        }
        NSLog(@"Bool 2:%d",(BOOL)boleanValue1);
        if (boleanValue1) {
            finished(boleanValue1);
            
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"filepath"];


        }else {
            
            NSLog(@"Url :%@",destination.absoluteString);
            
            
            NSString* filepath =  [[NSUserDefaults standardUserDefaults] objectForKey:@"filepath"];
            NSLog(@"Filepath:%@",filepath);
            NSLog(@"Fail");

            NSFileManager *fileManager = [NSFileManager defaultManager];
            
            NSError *error;
            BOOL success1 = [fileManager removeItemAtPath:destination.absoluteString error:&error];
            if (success1) {
                NSLog(@"Deleted");
                //                            UIAlertView *removedSuccessFullyAlert = [[UIAlertView alloc] initWithTitle:@"Congratulations:" message:@"Successfully removed" delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
                //                            [removedSuccessFullyAlert show];
            }
            else
            {
                NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
            }
            
        }
        
    }];
    [operation start];
    
}


@end
