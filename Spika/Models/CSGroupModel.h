//
//  CSGroupModel.h
//  Spika
//
//  Created by HelixTech-Admin  on 24/07/2017.
//  Copyright © 2017 Clover Studio. All rights reserved.
//

#import "CSModel.h"

@interface CSGroupModel : CSModel
@property (nonatomic, strong) NSString *creator_id;
@property (nonatomic, strong) NSString *group_id;
@property (nonatomic, strong) NSString *group_name;
@property (nonatomic, strong) NSString *room_id;
@property  (nonatomic,strong) NSMutableArray *members;
@property (nonatomic, strong) NSString *lastMessage;
@property (nonatomic, strong) NSString *lastMessageTime;
@property (nonatomic, strong) NSString *imagePath;



@end
