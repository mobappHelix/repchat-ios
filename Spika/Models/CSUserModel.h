//
//  UserModel.h
//  Prototype
//
//  Created by mislav on 14/07/15.
//  Copyright (c) 2015 Clover Studio. All rights reserved.
//

#import "CSModel.h"

@interface CSUserModel : CSModel

@property (nonatomic, strong) NSString *userID;
@property (nonatomic, strong) NSString *roomID;
@property (nonatomic, strong) NSString *avatarURL;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *pushToken;
@property (nonatomic,strong)  NSString *_id;
@property (nonatomic, strong) NSNumber *created;
@property (nonatomic,strong)  NSString *strmessgeid;
@property (nonatomic,strong)  NSString *registereddate;
@property (nonatomic,strong)  NSString *lastMessage;
@property (nonatomic,strong)  NSString *lastMessageTime;
@property (nonatomic,strong)  NSString *lastMessageCreated;
@property (nonatomic, strong) NSString *imagePath;

@property (nonatomic, strong) NSString *userType;


@property (nonatomic, strong) NSString *creator_id;
@property (nonatomic, strong) NSString *group_id;
@property (nonatomic, strong) NSString *group_name;
//@property (nonatomic, strong) NSString *room_id;
@property  (nonatomic,strong) NSMutableArray *members;

//@property (nonatomic,strong)  UIImage *profileImage;




@end
